package com.evqpoint.evqpointbatteryswappingstationapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.google.android.material.snackbar.Snackbar;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.control.UserControlActivityPre;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.mapactivity.MultiStationsMapActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.AddTransactionAsync;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.TransactionEntity;
import com.evqpoint.evqpointbatteryswappingstationapp.network.login.LoginApi;
import com.evqpoint.evqpointbatteryswappingstationapp.network.login.LoginResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.login.LoginUserEmail;
import com.evqpoint.evqpointbatteryswappingstationapp.network.login.LoginUserPhone;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionResponse;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_SCREEN_TIME_OUT=2000;

    //RETROFIT
    public static final String BASE_URL = "https://client.evqpoint.com/api/user/";//"https://client.evqpoint.com/user/";//"https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String userID;
    String userPhone;
    String userpass;

    ConstraintLayout clayout;
    LoginResponse loginResponse;
    ProgressBar progressbar;
    String loginToken;
    TextView tvNotice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        pref = this.getSharedPreferences("LoginPref", 0);
        editor = pref.edit();

        //TODO REMOVE TEMP
        //editor.clear();
        //editor.apply();
        //

        tvNotice = findViewById(R.id.tvNotice);
        clayout = findViewById(R.id.container);
        progressbar = findViewById(R.id.progressBar);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ImageView imageView = findViewById(R.id.imageView);
        //Glide.with(this).asGif().load(R.drawable.gif_logo).into(imageView);
        Glide.with(this).load(R.drawable.evqpointlogo_nocap).into(imageView);

        //As per previously Stored Password
        loginStart();
        clayout.setOnClickListener(v -> loginStart());
    }

    private void loginStart(){
        if(checkNetwork()){
            tvNotice.setVisibility(View.INVISIBLE);
            if(pref.getString("loginPassword", null) != null) {
                userpass = pref.getString("loginPassword", null);
                //Log.e("SplashActivity: Saved Pass",userpass);
            }
            if(pref.getString("loginEmail", null) != null){
                userID = pref.getString("loginEmail", null);
                //Log.e("SplashActivity: Saved ID",userID);
            }
            if(pref.getString("loginPhone", null) != null){
                userPhone = pref.getString("loginPhone", null);
                //Log.e("SplashActivity: Saved ID",userPhone);
            }

            new Handler().postDelayed(() -> {

                /*
                Intent i=new Intent(SplashActivity.this,
                        LoginActivity.class);
                finish();
                startActivity(i);
                */

                if((userID!=null)&&(userpass!=null)){
                    initLogin(userID, userpass);
                }else if((userPhone!=null)&&(userpass!=null)){
                    initLogin(userPhone, userpass);
                }else{
                    Log.e("SplashActivity: Status1","Invoking Splash >> Login Activity");
                    Intent i=new Intent(SplashActivity.this,
                            LoginActivity.class);
                    finish();
                    startActivity(i);
                }
            }, SPLASH_SCREEN_TIME_OUT);
        }
        else{
            tvNotice.setVisibility(View.VISIBLE);
        }
    }

    private void initLogin(String username, String userpass)
    {
        Log.e("SplashActivity: Status","initLogin");
        Log.e("SplashActivity: User Name is",username);
        Log.e("SplashActivity: User Pass is",userpass);

        if(username!=null||username!=""||username!=" ")
        {
            Log.e("SplashActivity: Status","Username check passed.");

            if(username.matches("\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}")) {

                Log.e("SplashActivity: User Name is","Phone Number");
                tryLogin("phone", userpass, username);
            }
            else if(username.contains("@"))
            {
                Log.e("SplashActivity: User Name is","Email Address");
                tryLogin("email",userpass, username);
            }else{
                Snackbar snackbar = Snackbar
                        .make(clayout,"Incorrect Login Credentials. Kindly Check Again.", Snackbar.LENGTH_LONG);
                snackbar.show();
                Intent i=new Intent(SplashActivity.this,
                        LoginActivity.class);
                finish();
                startActivity(i);
            }
        }
        else {
            Snackbar snackbar = Snackbar
                    .make(clayout,"Incorrect Login Credentials. Kindly Check Again.", Snackbar.LENGTH_LONG);
            snackbar.show();
            Intent i=new Intent(SplashActivity.this,
                    LoginActivity.class);
            finish();
            startActivity(i);
        }
    }

    private void tryLogin(String type, String pass, String id)
    {
        Log.e("SplashActivity: Status","tryLogin");
        //Log.e("SplashActivity: tryLoginType",type);
        //Log.e("SplashActivity: tryLoginPass",pass);
        //Log.e("SplashActivity: tryLoginID",id);

        LoginUserPhone newLoginPhone = null;
        LoginUserEmail newLoginEmail = null;

        if(type.equalsIgnoreCase("phone")){
            newLoginPhone = new LoginUserPhone("phone", id, pass);
            //Log.e("SplashActivity: LoginUserPhone",newLoginPhone.toString());
        }else if(type.equalsIgnoreCase("email")){
            newLoginEmail = new LoginUserEmail("email", id, pass);
            //Log.e("SplashActivity: LoginUserEmail",newLoginEmail.toString());
        }

        if(checkNetwork()){
            Log.e("SplashActivity: Status","Network Check Passed");

            progressbar.setVisibility(View.VISIBLE);

            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(5, TimeUnit.SECONDS)
                    .build();
            //.writeTimeout(20, TimeUnit.SECONDS)
            //.readTimeout(30, TimeUnit.SECONDS)

            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(okHttpClient)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }

            LoginApi LoginService = retrofit.create(LoginApi.class);
            Call<LoginResponse> call = null;

            if(type.equalsIgnoreCase("phone")) {
                call = LoginService.loginUserPhone(newLoginPhone);
            }else if(type.equalsIgnoreCase("email")){
                call = LoginService.loginUserEmail(newLoginEmail);
            }

            Log.e("SplashActivity: Status","Retrofit Login HTTP Call");

            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if(response.code()==401){
                        //progressDialog.dismiss();
                        progressbar.setVisibility(View.INVISIBLE);
                        showLoginFailed();
                    }
                    else{
                        if(response.body()!=null){
                            loginResponse = response.body();

                            Log.e("SplashActivity: Login Response",loginResponse.toString());

                            //Cache Token Response
                            editor = pref.edit();
                            editor.putString("loginToken", loginResponse.getToken());
                            loginToken = loginResponse.getToken();
                            editor.putString("loginId", loginResponse.getId());
                            editor.putString("loginEmail", loginResponse.getEmail());
                            editor.putString("loginPhone", loginResponse.getPhoneNumber());
                            editor.putString("loginName", loginResponse.getName());
                            //TODO Append Child ID
                            if(loginResponse.getLastdevid()!=null){
                                Log.e("LAST DID",loginResponse.getLastdevid());
                                editor.putString("devid", loginResponse.getLastdevid()+"-0"+
                                        loginResponse.getChildId());
                            }else{
                                editor.putString("devid", "0");
                            }
                            editor.putString("loginPassword", pass);

                            if(loginResponse.getAddress()!=null)
                            {
                                Log.e("SplashActivity: loginAddress",loginResponse.getAddress());
                                editor.putString("loginAddress", loginResponse.getAddress());
                                Log.e("SplashActivity: loginCity",loginResponse.getCity());
                                editor.putString("loginCity", loginResponse.getCity());
                                Log.e("SplashActivity: loginCountry",loginResponse.getCountry());
                                editor.putString("loginCountry", loginResponse.getCountry());
                                Log.e("SplashActivity: loginState",loginResponse.getState());
                                editor.putString("loginState", loginResponse.getState());
                            }
                            else{
                                Log.e("SplashActivity: Status","No address registered.");
                                editor.putString("loginAddress", " ");
                                editor.putString("loginCity", " ");
                                editor.putString("loginCountry", " ");
                                editor.putString("loginState", " ");
                            }
                            editor.apply();

                            //Get Previous TransactionID
                            List<String> transactionIDList = loginResponse.getTransactionId();
                            if((transactionIDList == null)||(transactionIDList.isEmpty())){

                            }else{
                                if((transactionIDList!=null)&&(transactionIDList.size()>0)) {
                                    String recenttransaction = transactionIDList.get(transactionIDList.size() - 1);
                                    Log.e("SplashActivity: Recent Transaction", recenttransaction);
                                    editor.putString("transID",recenttransaction);
                                    editor.apply();
                                    checkTransactionID(recenttransaction, loginResponse.getLastdevid());

                                    //LATER
                                    Log.e("SplashActivity: STATUS", "Why Return?");
                                    String tIDS = transactionIDList.get(0);
                                    Log.e("SplashActivity: TIDS0", tIDS);
                                    for(int pp=1; pp<transactionIDList.size(); pp++){
                                        tIDS = tIDS + ","+transactionIDList.get(pp);
                                    }
                                    Log.e("SplashActivity: Old Transactions", tIDS.toString());
                                    editor.putString("transactions",tIDS.toString());
                                    editor.apply();
                                }
                            }

                            Log.e("SplashActivity: Login Token", loginResponse.getToken());

                            //progressDialog.dismiss();
                            ////progressbar.setVisibility(View.INVISIBLE);
                            loginSuccess(loginResponse.getToken(), pass);
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable throwable) {
                    //progressDialog.dismiss();
                    progressbar.setVisibility(View.INVISIBLE);
                    showLoginFailed();
                }
            });
        }else{
            Toast.makeText(this, "No Network Available. Kindly switch on Internet and Try again.", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkNetwork() {
        final ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    private void loginSuccess(String loginToken, String loginPassword) {

        Log.e("SplashActivity: Status","Login Success.");

        editor.putString("loginPassword", loginPassword);
        editor.apply();

        //Intent intent = new Intent(this, LoginActivity.class);
        //TODO TEMP FORGET OLD DETAILS
        /*
        String tID = "0";
        String devid = "0";
        editor.putString("transID","0");
        editor.putString("devid","0");

        Intent intent = new Intent(this, MultiStationsMapActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("loginToken",loginToken);
        finish();
        startActivity(intent);
        */
        //TODO
        String tID = "0";
        String devid = "0";
        Log.e("SplashActivity:: transID:", pref.getString("transID", "0"));
        if(pref.getString("transID", "0")!="0"){
            tID = pref.getString("transID", "0");
            devid = pref.getString("devid", "0");
            Log.e("SplashActivity: Cached Old TID",tID);

            if(!tID.equalsIgnoreCase("0")){
                checkTransactionID(tID, devid);
                progressbar.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(), "Welcome", Toast.LENGTH_LONG).show();
                Log.e("SplashActivity: Status","Login Success! Going to Splash >> MultiStationMapActivity");

                //Intent intent = new Intent(this, LoginActivity.class);
                Intent intent1 = new Intent(this, MultiStationsMapActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.putExtra("loginToken",loginToken);

                editor.putString("transID", "0");
                editor.putString("devid", "0");
                editor.apply();
                tID = "0";
                devid = "0";

                finish();
                startActivity(intent1);
            }else{
                Intent intent1 = new Intent(this, MultiStationsMapActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent1.putExtra("loginToken",loginToken);
                finish();
                startActivity(intent1);
            }
        }
        else{
            progressbar.setVisibility(View.INVISIBLE);
            Toast.makeText(getApplicationContext(), "Welcome", Toast.LENGTH_LONG).show();
            Log.e("SplashActivity: Status","Login Success! Going to Splash >> MultiStationMapActivity");

            //Intent intent = new Intent(this, LoginActivity.class);
            Intent intent1 = new Intent(this, MultiStationsMapActivity.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent1.putExtra("loginToken",loginToken);
            finish();
            startActivity(intent1);
        }
    }

    private void showLoginFailed() {
        Log.e("SplashActivity: Status","Login Failed");
        Snackbar snackbar = Snackbar
                .make(clayout,"Incorrect Login Credentials. Kindly Check Again.", Snackbar.LENGTH_LONG);
        snackbar.show();

        editor = pref.edit();
        editor.clear();
        editor.apply();

        Intent i=new Intent(SplashActivity.this,
                LoginActivity.class);
        finish();
        startActivity(i);
    }

    public static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    private void checkTransactionID(String tid, String devid) {
        Log.e("SplashActivity: Transaction ID", tid);
        Log.e("SplashActivity: Old Device ID", devid);

        //TODO Working
        /*
        Intent intent = new Intent(SplashActivity.this, MultiStationsMapActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("loginToken",loginToken);
        finish();
        startActivity(intent);
        */

        //TODO Check if old transaction is ongoing
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        TransactionRequest transactionRequest = new TransactionRequest(tid);
        TransactionAPI transactionService = retrofit.create(TransactionAPI.class);
        Call<TransactionResponse> call = transactionService.getTransactionResponse(transactionRequest);
        Log.e("SplashActivity: Status", "Fetching Transaction Details");
        Log.e("SplashActivity: Request", transactionRequest.toString());

        call.enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {

                TransactionResponse transactionResponse = response.body();
                Log.e("SplashActivity: TransactionResponse Code", Integer.toString(response.code()));

                if(response.isSuccessful()){
                    Log.e("SplashActivity: Status", "Session Stopped.");
                    Log.e("SplashActivity: Previous Transaction", transactionResponse.toString());
                    //Log.e("SplashActivity: Transaction Status", transactionResponse.getTransactionID());
                    Log.e("SplashActivity: Trasnsaction Response", transactionResponse.toString());
                    editor.remove("devid");
                    editor.remove("transID");
                    editor.commit();
                    progressbar.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(), "Welcome", Toast.LENGTH_LONG).show();
                    Log.e("SplashActivity: Status","Login Success! Going to Splash >> MultiStationMapActivity");
                    Intent intent = new Intent(SplashActivity.this, MultiStationsMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("loginToken",loginToken);
                    finish();
                    startActivity(intent);
                }
                else if (response.code() == 400) {
                    Log.e("SplashActivity: Status", "Session is on going!!");
                    Log.e("SplashActivity: Status", "Olde DEVID " + devid);
                    Log.e("SplashActivity: Status", "Olde TID " + tid);

                    progressbar.setVisibility(View.INVISIBLE);

                    Intent homeint = new Intent(SplashActivity.this, UserControlActivityPre.class);
                    homeint.putExtra("devid",devid);
                    editor.putString("devid",devid);
                    editor.putString("transID",tid);
                    editor.commit();
                    finish();
                    startActivity(homeint);
                }
                }
            //}

            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable throwable) {
                Log.e("SplashActivity: Status", "Retrofit session status HTTP Calling Failed");
            }
        });
    }

    }