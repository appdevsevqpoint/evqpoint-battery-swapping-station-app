package com.evqpoint.evqpointbatteryswappingstationapp.activities.chargestationslistactivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.mapactivity.MultiStationsMapActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.AddChargingStation;
import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.ChargeStationAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.Coordinates;
import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.DeleteChargingStation;
import com.evqpoint.evqpointbatteryswappingstationapp.network.getstations.ChargeStationsResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.getstations.StationLocation;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.ChargingStation;

//Recycler Activity showing list of existing stations
public class ChargeStationsActivity extends AppCompatActivity implements ChargeStationsAdapter.ChargeStationsAdapterCallback{//}, onStationAddListener {

    private RecyclerView mRecyclerView;
    private ChargeStationsAdapter mAdapter;
    private CoordinatorLayout coordinatorLayout;
    public Context global_context;

    private List<ChargingStation> chargeStationsList;
    private List<ChargingStation> filterStationsList;
    private boolean searchFlag;

    private String authToken;

    public static final String BASE_URL = "https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //fetchAuthToken
        Intent intentOne = this.getIntent();
        authToken = intentOne.getStringExtra("loginToken");

        //fetch data from server
        fetchStations();
        chargeStationsList = new ArrayList<>();
        filterStationsList = new ArrayList<>();
        searchFlag = false;

        //UI initialization
        setContentView(R.layout.activity_stations);
        coordinatorLayout = findViewById(R.id.coordinator_layout);
        global_context = getApplicationContext();

        //toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // toolbar fancy stuff
        //getSupportActionBar().setTitle("EV Charging Stations");

        //RECYCLER
        mRecyclerView = findViewById(R.id.stations_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        //RECYCLER ADAPTER
        mAdapter = new ChargeStationsAdapter(chargeStationsList, ChargeStationsActivity.this);
        mRecyclerView.setAdapter(mAdapter);

        //FAB Search
        FloatingActionButton fabSearch = findViewById(R.id.fabSearch);
        fabSearch.setOnClickListener(view -> {
            if(!searchFlag){
                final BottomSheetDialog mBottomSheetDialog1 = new BottomSheetDialog(
                        ChargeStationsActivity.this);

                final View sheetView1 = ChargeStationsActivity.this.getLayoutInflater().inflate(R.layout.bottom_sheet_search, null);
                mBottomSheetDialog1.setContentView(sheetView1);

                final EditText inputSearchTerm = sheetView1.findViewById(R.id.EditTextSearch);
                final Button searchButton = sheetView1.findViewById(R.id.ButtonSearch);
                searchButton.setText(R.string.search);
                searchButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        searchFlag = true;
                        String searchString = inputSearchTerm.getText().toString();

                        for(int k=0; k<chargeStationsList.size(); k++){
                            if((chargeStationsList.get(k).getName().toLowerCase().contains(searchString.toLowerCase()))||(chargeStationsList.get(k).getAddress().toLowerCase().contains(searchString.toLowerCase()))){
                                filterStationsList.add(chargeStationsList.get(k));
                            }
                        }

                        if(filterStationsList.size()!=0){
                            searchAdapterReset();

                            fabSearch.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_refresh_white));
                        }
                        else{
                            //Toast No Station Found
                            //fabSearch.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_search_icon));
                            searchFlag = false;
                            resetAdapter();
                        }

                        if(mBottomSheetDialog1.isShowing()){
                            mBottomSheetDialog1.dismiss();
                        }
                    }
                });

                mBottomSheetDialog1.show();
            }
            else{
                searchFlag = false;
                filterStationsList.clear();
                //fabSearch.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_search_24px));
                fabSearch.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_station_1_dark));
                resetAdapter();
            }

        });

        //FAB Add Station
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(ChargeStationsActivity.this);

                final View sheetView = ChargeStationsActivity.this.getLayoutInflater().inflate(R.layout.bottom_sheet, null);
                mBottomSheetDialog.setContentView(sheetView);

                final EditText inputStationName = sheetView.findViewById(R.id.EditTextName);
                final EditText inputStationAddress = sheetView.findViewById(R.id.EditTextAddress);
                final EditText inputStationCoordinates = sheetView.findViewById(R.id.EditTextCoordinates);
                final EditText inputStationImage = sheetView.findViewById(R.id.EditTextImage);
                final Button saveButton = sheetView.findViewById(R.id.ButtonSave);
                saveButton.setText(R.string.save);
                saveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String stationName = inputStationName.getText().toString();
                        String stationAddress = inputStationAddress.getText().toString();
                        String stationCoordinate = inputStationCoordinates.getText().toString();
                        Coordinates stationCoordinates = new Coordinates(stationCoordinate);
                        String stationImage = inputStationImage.getText().toString();

                        //Log Input Station Details
                        //Log.e("Station Name",stationName);
                        //Log.e("Station Address",stationAddress);
                        //Log.e("Station Coordinate",stationCoordinate);
                        //Log.e("Station Coordinates",stationCoordinates.toString());
                        //Log.e("Station Image",stationImage);
                        //

                        onStationAdded(stationName,stationAddress, stationCoordinates, stationImage);
                        if(mBottomSheetDialog.isShowing()){
                            mBottomSheetDialog.dismiss();

                            String name = stationName;
                            Snackbar snackbar = Snackbar
                                    .make(coordinatorLayout, name + " added!", Snackbar.LENGTH_LONG);
                            snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
                            snackbar.show();
                        }
                    }
                });

                mBottomSheetDialog.show();
            }
        });
    }

    public void onStationAdded(String stationName, String stationAddress, Coordinates stationCoordinates, String stationImge) {
        Log.e("Status", "Invoke HTTP POST for adding new station.");
        Log.e("Img URL", stationImge);

        AddChargingStation chargeStation = new AddChargingStation(stationName, stationAddress,
                stationCoordinates.getCoordinates(), stationImge);

        //Retrofit Add Station
        addStationToServer(chargeStation);
        //Update Adapter
        mAdapter.onStationAdd(chargeStationsList);

        if(chargeStationsList.size()==1){
            //TODO
            resetAdapter();
        }
    }

    public boolean onStationRemoved(String id) {
        Boolean deletionSuccess = false;
        Log.e("Delete Station", "Station ID = "+id);

        //Retrofit Remove Station
        removeStationFromServer(id);
        //Update Adapter
        for(int i=0;i<chargeStationsList.size(); i++){
            if(chargeStationsList.get(i).getId().equalsIgnoreCase(id)){
                mAdapter.removeStation(i);
                resetAdapter();
                deletionSuccess = true;
            }
        }

        return deletionSuccess;
    }

    @Override
    public void onStationSelected(final int position)
    {
        final ChargingStation selectedStation = chargeStationsList.get(position);
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(ChargeStationsActivity.this);

        final View sheetView = ChargeStationsActivity.this.getLayoutInflater().inflate(R.layout.bottom_sheet, null);
        mBottomSheetDialog.setContentView(sheetView);

        final EditText inputStationName = sheetView.findViewById(R.id.EditTextName);
        inputStationName.setText(selectedStation.getName());
        final EditText inputStationAddress = sheetView.findViewById(R.id.EditTextAddress);
        inputStationAddress.setText(selectedStation.getAddress());
        final EditText inputStationCoordinates = sheetView.findViewById(R.id.EditTextCoordinates);
        inputStationCoordinates.setText(selectedStation.getLocation().get(0) + "," + selectedStation.getLocation().get(1));
        final EditText inputStationImage = sheetView.findViewById(R.id.EditTextImage);
        inputStationImage.setText(selectedStation.getImage());

        final Button saveButton = sheetView.findViewById(R.id.ButtonSave);
        saveButton.setText(R.string.delete);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Status","Deletion initiated");

                String stationId = selectedStation.getId();
                String name = selectedStation.getName();

                if(onStationRemoved(stationId)){
                    if (mBottomSheetDialog.isShowing()) {
                        mBottomSheetDialog.dismiss();
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, name + " Deleted!", Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
                        snackbar.show();
                    }
                }else{
                    if (mBottomSheetDialog.isShowing()) {
                        mBottomSheetDialog.dismiss();
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, name + " Delete Failed!", Snackbar.LENGTH_LONG);
                        snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
                        snackbar.show();
                    }
                }
            }
        });

        final Button gotoButton = sheetView.findViewById(R.id.ButtonGoto);
        gotoButton.setText(R.string.go);
        gotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Status","Navigation initiated");

                String stationId = selectedStation.getId();
                String name = selectedStation.getName();

                if (mBottomSheetDialog.isShowing()) {
                    mBottomSheetDialog.dismiss();
                    //Intent intentMaps = new Intent(getApplicationContext(), CachedMapActivity.class);
                    Intent intentMaps = new Intent(getApplicationContext(), MultiStationsMapActivity.class);

                    intentMaps.putExtra("stationLat",selectedStation.getLocation().get(0));
                    intentMaps.putExtra("stationLon",selectedStation.getLocation().get(1));

                    startActivity(intentMaps);
                }
            }
        });

        mBottomSheetDialog.show();
    }

    private void addStationToServer(final AddChargingStation newStation)
    {
        //TODO To fix bug.
        Log.e("Status","addStationToServer");
        Log.e("New Station to Add",newStation.toString());

        final ProgressDialog progressDialog = ProgressDialog.show(this, "Adding Station", "Please wait", false, false);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ChargeStationAPI ChargeStationService = retrofit.create(ChargeStationAPI.class);

        Call<ChargingStation> call = ChargeStationService.setNewChargePoint(authToken, newStation);

        Log.e("Status","Retrofit add station HTTP Call");
        call.enqueue(new Callback<ChargingStation>() {
            @Override
            public void onResponse(Call<ChargingStation> call, Response<ChargingStation> response) {
                ChargingStation addStationResponse = response.body();
                Log.e("Add Response Body", response.body().toString());

                addStationResponse.setImage(newStation.getImage());
                chargeStationsList.add(addStationResponse);
                resetAdapter();

                progressDialog.dismiss();
                Log.e("RF HTTP Succeeded","Station Addition Succeeded.");
            }

            @Override
            public void onFailure(Call<ChargingStation> call, Throwable throwable) {
                progressDialog.dismiss();
                Log.e("RF HTTP Failed","Station Addition Failed.");
                Log.e("RF HTTP Failed",call.request().toString());
                Log.e("RF HTTP Failed",call.request().body().toString());
                Log.e("RF HTTP Failed", call.request().header("authorization"));

                //TODO Proper Failure Resposes
                //showStationAdditionFailed("Incorrect Login ID or Password.");
            }
        });
    }

    private void removeStationFromServer(String id)
    {
        Log.e("Status","removeStationFromServer");

        final ProgressDialog progressDialog = ProgressDialog.show(this, "Removing Station", "Please wait", false, false);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ChargeStationAPI ChargeStationService = retrofit.create(ChargeStationAPI.class);

        final DeleteChargingStation deleteStation = new DeleteChargingStation(id);
        Call<String> call = ChargeStationService.removeChargingStation(authToken, deleteStation);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.e("Deletion HTTP Response",response.toString());
                progressDialog.dismiss();
                Log.e("RF HTTP Succeeded","Station Deletion Succeeded.");
            }

            @Override
            public void onFailure(Call<String> call, Throwable throwable) {
                progressDialog.dismiss();
                //Log.e("RF HTTP Failed","Station Deletion Failed.");
                //Log.e("RF HTTP Failed",throwable.toString());
                //Log.e("RF HTTP Failed",throwable.getMessage());
                //TODO Fix False Positives
            }
        });
    }

    public void resetAdapter(){
        mAdapter = new ChargeStationsAdapter(chargeStationsList,ChargeStationsActivity.this);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyItemRangeChanged(0,chargeStationsList.size());
    }

    public void searchAdapterReset(){
        mAdapter = new ChargeStationsAdapter(filterStationsList,ChargeStationsActivity.this);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyItemRangeChanged(0,filterStationsList.size());
    }

    public void fetchStations() {
        //TODO Proper Failure Resposes
        final ProgressDialog progressDialog = ProgressDialog.show(this, "Loading posts", "Please wait", false, false);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ChargeStationAPI EVQAdminService = retrofit.create(ChargeStationAPI.class);

        //TODO Current Coordinates ~ StationLocation
        List<Double> coords = new ArrayList<Double>();
        coords.add(0.0);
        coords.add(0.0);
        StationLocation loc = new StationLocation(coords);
        //
        Call<ChargeStationsResponse> call = EVQAdminService.getChargingStations(authToken, loc);

        Log.e("Request HTTP Header", call.request().headers().toString());
        Log.e("Request HTTP Body", call.request().body().toString());
        Log.e("Request HTTP Body", coords.toString());

        call.enqueue(new Callback<ChargeStationsResponse>() {
            @Override
            public void onResponse(Call<ChargeStationsResponse> call, Response<ChargeStationsResponse> response) {

                Log.e("Response",response.body().toString());

                List<ChargingStation> chargingStationsIN = response.body().getChargingStations();

                for(int z=0; z<chargingStationsIN.size(); z++){
                    //Log.e("ChargeStation", chargingStationsIN.get(z).toString());
                    chargeStationsList.add(new ChargingStation(chargingStationsIN.get(z)));
                }

                //Dismissing the progress dialog
                progressDialog.dismiss();
                mAdapter.onStationAdd(chargeStationsList);
            }

            @Override
            public void onFailure(Call<ChargeStationsResponse> call, Throwable throwable) {
                for(int y=0; y<chargeStationsList.size(); y++){
                    Log.e("Station List",Double.toString(chargeStationsList.get(y).getLocation().get(0)));
                }
            }
        });
    }
}
