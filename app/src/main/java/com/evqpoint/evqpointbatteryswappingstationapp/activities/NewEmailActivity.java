package com.evqpoint.evqpointbatteryswappingstationapp.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.network.otp.OtpApi;
import com.evqpoint.evqpointbatteryswappingstationapp.network.otp.OtpResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.user_details.UpdateUserDetailsApi;
import com.evqpoint.evqpointbatteryswappingstationapp.network.user_details.UpdateUserEmailRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.user_details.UpdatedUserResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class NewEmailActivity extends AppCompatActivity {

    String email;
    String EOTP;

    //RETROFIT
    public static final String BASE_URL = "https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    //CachedOTP
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    //UI
    EditText editNewEmail;
    EditText editEOTP;
    Button btnSend;
    Button btnValidate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_update);

        pref = this.getSharedPreferences("LoginPref", 0);
        EOTP = pref.getString("eotp", "");

        editNewEmail = findViewById(R.id.editNewEmail);
        editEOTP = findViewById(R.id.editEOTP);
        btnSend = findViewById(R.id.btnChangePass);
        btnValidate = findViewById(R.id.validateEOTP);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSend.setText("RESEND OTP");
                sendEmailOTP(editNewEmail.getText().toString());
            }
        });

        btnValidate.setOnClickListener(v -> {
            //editNewEmail.setEnabled(false);
            validateEOTP();
        });
    }

    private void validateEOTP() {

        String inOTP = editEOTP.getText().toString().trim();
        Log.e("EnteredOTP", inOTP);

        EOTP = EOTP.trim();
        Log.e("EOTP", EOTP);
        String newEmail = editNewEmail.getText().toString();

        if((newEmail!=null)&&(newEmail!=""))
        {
            if(EOTP.equals(inOTP)){
                String loginID = pref.getString("loginId","");
                String userPhone = pref.getString("loginPhone","");

                final String BASE_URL = "https://client.evqpoint.com/api/user/";

                if (retrofit == null) {
                    retrofit = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                }

                UpdateUserDetailsApi UpdateService = retrofit.create(UpdateUserDetailsApi.class);

                UpdateUserEmailRequest updateUserEmailRequest = new UpdateUserEmailRequest(loginID,
                        newEmail,userPhone);
                Call<UpdatedUserResponse> call = UpdateService.updateUserEmail(updateUserEmailRequest);

                Log.e("LoginActivity: Call",call.toString());
                Log.e("LoginActivity: Request String",updateUserEmailRequest.toString());

                call.enqueue(new Callback<UpdatedUserResponse>() {
                    @Override
                    public void onResponse(Call<UpdatedUserResponse> call, retrofit2.Response<UpdatedUserResponse> response) {
                        String responseString = "";

                        if (response.isSuccessful()) {
                            responseString = response.body().toString();

                            if(responseString!=null){
                                Log.e("NewEmailActivity: User Update response",responseString);
                                //TODO RELOGIN
                                Toast.makeText(NewEmailActivity.this,
                                        "Email Address Updated", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else{
                            Log.e("NewEmailActivity: User Update response","Update Failed.");
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdatedUserResponse> call, Throwable throwable) {
                        Log.e("LoginActivity: User response","Update Failed.");
                    }
                });
            }else{
                editEOTP.setError("Incorrect OTP. Reenter OTP.");
            }
        }else{
            Toast.makeText(NewEmailActivity.this,
                    "Please enter all fields and retry.", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendEmailOTP(String newemail) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                            .baseUrl(BASE_URL)
                            .build();

        OtpApi otpApi = retrofit.create(OtpApi.class);
        otpApi.sendEmailOTP("https://api.evqpoint.com/user/emailOtp/"+newemail).enqueue(new Callback<OtpResponse>() {
            @Override
            public void onResponse(Call<OtpResponse> call, Response<OtpResponse> response) {

                if((response.isSuccessful())&&(response!=null)&&(response.body().getOTP()!=null)){
                    OtpResponse otpEmailResp = response.body();
                    EOTP = otpEmailResp.getOTP();
                    Log.e("NewEmailActivity: EOTP", EOTP);
                }else{
                    Toast.makeText(NewEmailActivity.this, "Email OTP Error. Use another Email Address to generate OTP.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OtpResponse> call, Throwable t) {
                Log.e("NewEmailActivity: OTP Error", "OTP ERROR");
            }
        });
    }
}