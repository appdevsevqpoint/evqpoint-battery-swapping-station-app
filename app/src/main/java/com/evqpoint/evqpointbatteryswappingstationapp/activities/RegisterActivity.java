package com.evqpoint.evqpointbatteryswappingstationapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.network.login.LoginApi;
import com.evqpoint.evqpointbatteryswappingstationapp.network.login.NewUserResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.signup.RegisterNewUser;
import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {

  LinearLayout clayout;

  //RETROFIT
  public static final String BASE_URL = "https://client.evqpoint.com/api/user/";//"https://client.evqpoint.com/user/";//"https://api.evqpoint.com/user/";
  private static Retrofit retrofit = null;

  //Peferences to be saved for Auto Login
  SharedPreferences pref;
  SharedPreferences.Editor editor;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_register);
    Log.e("Status","Registration Activity");

    //UI
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    toolbar.setLogo(R.mipmap.app_logo_round);

    pref = this.getSharedPreferences("LoginPref", 0);

    final EditText fullNameEditText = findViewById(R.id.fullname);
    final EditText phoneEmailEditText = findViewById(R.id.userphone_email);

    final Button registerButton = findViewById(R.id.signup);
    final ProgressBar loadingProgressBar = findViewById(R.id.loading);
    clayout = findViewById(R.id.container);

    registerButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        try {
          Log.e("Status","Closing Keyboard");
          InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
          imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

          loadingProgressBar.setVisibility(View.VISIBLE);
          String userFullName = fullNameEditText.getText().toString();
          String userPhoneEmail = phoneEmailEditText.getText().toString();

          initRegistration(userFullName, userPhoneEmail);
        } catch (Exception e) {
          // TODO: handle exception
        }
      }
    });
  }

  private void initRegistration(String userFullName, String userPhoneEmail) {

    Log.e("RegisterActivity: UserName", userFullName);
    Log.e("RegisterActivity: UserPhoneEmail", userPhoneEmail);

    if(userFullName!=null||userFullName!=""||userFullName!=" "){
      if(userPhoneEmail!=null||userPhoneEmail!=""||userPhoneEmail!=" "){

        Log.e("RegisterActivity: Status", "Initiating Registration");
        RegisterNewUser newUser = new RegisterNewUser(userFullName, userPhoneEmail);
        tryRegistration(newUser);
      }
      else{
        Snackbar snackbar = Snackbar
                .make(clayout,"Missing Mobile Number or Email address. Kindly Enter Again.", Snackbar.LENGTH_LONG);
        snackbar.show();
      }
    }
    else{
      Snackbar snackbar = Snackbar
              .make(clayout,"Missing User Full Name. Kindly Enter Again.", Snackbar.LENGTH_LONG);
      snackbar.show();
    }
  }

  private void tryRegistration(final RegisterNewUser newUser)
  {
    final ProgressDialog progressDialog = ProgressDialog.show(this, "Registering", "Please wait", false, false);

    if (retrofit == null) {
      retrofit = new Retrofit.Builder()
              .baseUrl(BASE_URL)
              .addConverterFactory(GsonConverterFactory.create())
              .build();
    }

    LoginApi registrationService = retrofit.create(LoginApi.class);
    Call<NewUserResponse> call = registrationService.registerNewUser(newUser);

    call.enqueue(new Callback<NewUserResponse>() {
      @Override
      public void onResponse(Call<NewUserResponse> call, Response<NewUserResponse> response) {
        NewUserResponse newUserResponse = response.body();

        if((response.body() != null)&&(response.code()==200)) {
          //Cache Password Response
          Log.e("Registration Response", response.body().toString());
          forwardToLogin(0);
        }
        else if(response.code()==404) {
          Log.e("Registration Response", "Already Exists!");
          Toast.makeText(getApplicationContext(), "Registration Failed! Account Already Exists!",
                  Toast.LENGTH_SHORT).show();
          forwardToLogin(1);
        }else{
          Log.e("Registration Response", response.toString() + "Failed!!");
          Toast.makeText(getApplicationContext(), "Registration Failed! Try another phone or email.", Toast.LENGTH_SHORT).show();
        }
        //editor.putString("loginPhone", newUserResponse.getPhoneNumber());
        //editor.putString("loginPassword", newUserResponse.getPassword());
        //editor.putString("loginEmail", newUserResponse.getEmail());
        //editor.apply();

        progressDialog.dismiss();

        //Redirect to Login on Registration
        //TODO
        //forwardToLogin();
      }

      @Override
      public void onFailure(Call<NewUserResponse> call, Throwable throwable) {
        Log.e("RF Data Error on POST", throwable.toString());
      }
    });
  }

  private void forwardToLogin(int type) {

    if(type==0){
      Toast.makeText(getApplicationContext(), "Registered", Toast.LENGTH_LONG).show();
    }
    else{
      //
    }
    Intent intent = new Intent(this, LoginActivity.class);
    finish();
    startActivity(intent);
  }

  private void showLoginFailed(String errorString) {
    Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
  }
}
