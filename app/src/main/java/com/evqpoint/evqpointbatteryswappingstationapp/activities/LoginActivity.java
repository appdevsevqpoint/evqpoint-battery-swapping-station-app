package com.evqpoint.evqpointbatteryswappingstationapp.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.VisibleForTesting;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.control.UserControlActivityPre;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.mapactivity.MultiStationsMapActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.AddTransactionAsync;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.TransactionEntity;
import com.evqpoint.evqpointbatteryswappingstationapp.network.login.LoginApi;
import com.evqpoint.evqpointbatteryswappingstationapp.network.login.LoginResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.login.LoginUserEmail;
import com.evqpoint.evqpointbatteryswappingstationapp.network.login.LoginUserPhone;
import com.evqpoint.evqpointbatteryswappingstationapp.network.password.PasswordApi;
import com.evqpoint.evqpointbatteryswappingstationapp.network.password.RecoverRequestEmail;
import com.evqpoint.evqpointbatteryswappingstationapp.network.password.RecoverRequestPhone;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.user_details.UpdateUserDetailsApi;
import com.evqpoint.evqpointbatteryswappingstationapp.network.user_details.UpdateUserEmailRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.user_details.UpdateUserPhoneRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.user_details.UpdatedUserResponse;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class LoginActivity extends AppCompatActivity {

    LinearLayout clayout;
    EditText passwordEditText;
    //RETROFIT
    public static final String BASE_URL = "https://client.evqpoint.com/api/user/";//"https://client.evqpoint.com/user/";//https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    //Peferences to be saved for Auto Login
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String userID;
    String loginID;
    String emailID;
    String userPhone;
    String userpass;

    //Login Details
    LoginResponse loginResponse;
    BottomSheetDialog dialogEmail;
    BottomSheetDialog dialogPhone;
    boolean dialogPhoneBool;
    boolean dialogEmailBool;
    boolean loginSuccessBool;
    String logPass;

    String loginToken;
    String childid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Log.e("Status","Login Activity");

        pref = this.getSharedPreferences("LoginPref", 0);
        //editor = pref.edit();

        //
        logPass = "";
        dialogPhoneBool = false;
        dialogEmailBool = false;
        loginSuccessBool = true;
        //

        final EditText usernameEditText = findViewById(R.id.userNameOrPhone);
        passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final TextView registerLinkButton = findViewById(R.id.signup);
        final TextView forgotLinkButton = findViewById(R.id.forgotpassword);

        final ProgressBar loadingProgressBar = findViewById(R.id.loading);
        clayout = findViewById(R.id.container);

        //Previously Stored Password
        if(pref.getString("loginPassword", null) != null) {
            userpass = pref.getString("loginPassword", null);
            Log.e("Saved Pass",userpass);
            //passwordEditText.setText(pref.getString("loginPassword", null));
        }
        if(pref.getString("loginEmail", null) != null){
            userID = pref.getString("loginEmail", null);
            Log.e("Saved Email",userID);
            //usernameEditText.setText(pref.getString("loginemail", null));
        }
        if(pref.getString("loginPhone", null) != null){
            userPhone = pref.getString("loginPhone", null);
            Log.e("Saved Phone",userPhone);
            //usernameEditText.setText(pref.getString("loginemail", null));
        }

        if((userID!=null)&&(userpass!=null)){
            initLogin(userID, userpass);
        }
        if((userPhone!=null)&&(userpass!=null)){
            initLogin(userID, userpass);
        }

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    // TODO: handle exception
                }

                //loadingProgressBar.setVisibility(View.VISIBLE);
                userID = usernameEditText.getText().toString();
                userpass = passwordEditText.getText().toString();

                initLogin(userID, userpass);
            }
        });

        registerLinkButton.setOnClickListener(v -> {
            //Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
            Intent intent = new Intent(getApplicationContext(), BetaTermsActivity.class);
            startActivity(intent);
        });

        forgotLinkButton.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setTitle("Password Recovery");
            builder.setMessage("Enter your registered email address or mobile number.");

            final EditText input = new EditText(LoginActivity.this);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);

            builder.setPositiveButton("OK", (dialog, which) -> {
                String m_Text = input.getText().toString();

                if((m_Text.length()>5)&&(m_Text.contains("@"))&&(m_Text.contains(".com"))){
                    recoverPassword(m_Text);
                }else if((isNumeric(m_Text))&&(!m_Text.contains("@"))&&(!m_Text.contains(".com"))){
                    recoverPassword(m_Text);
                }else{
                    Snackbar snackbar = Snackbar
                            .make(v, "Re-enter correct email address.", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            });
            builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

            builder.show();
        });
    }

    private void initLogin(String username, String userpass)
    {
        Log.e("Status","initLogin");
        Log.e("User Name is",username);
        Log.e("User Pass is",userpass);

        if(username!=null||username!=""||username!=" ")
        {
            Log.e("Status","Username check passed.");

            if(username.matches("\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}")) {

                //Log.e("User Name is","Phone Number");
                tryLogin("phone", userpass, username);
            }
            else if(username.contains("@"))
            {
                //Log.e("User Name is","Email Address");
                tryLogin("email", userpass, username);
            }else{
                Log.e("Status","InitLogin issue.");

                Snackbar snackbar = Snackbar
                        .make(clayout,"Incorrect Login Credentials. Kindly Check Again.", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
        else {
            Log.e("Status","InitLogin issue.");

            Snackbar snackbar = Snackbar
                    .make(clayout,"Incorrect Login Credentials. Kindly Check Again.", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void tryLogin(String type, String pass, String id)
    {
        Log.e("LoginActivity: Status","tryLogin");
        //Log.e("LoginActivity: tryLoginType",type);
        //Log.e("LoginActivity: tryLoginPass",pass);
        //Log.e("LoginActivity: tryLoginID",id);

        LoginUserPhone newLoginPhone = null;
        LoginUserEmail newLoginEmail = null;

        if(type.equalsIgnoreCase("phone")){
            newLoginPhone = new LoginUserPhone("phone", id, pass);
            //Log.e("LoginActivity: LoginUserPhone",newLoginPhone.toString());
        }else if(type.equalsIgnoreCase("email")){
            newLoginEmail = new LoginUserEmail("email", id, pass);
            //Log.e("LoginActivity: LoginUserEmail",newLoginEmail.toString());
        }

        if(checkNetwork()){
            Log.e("LoginActivity: Status","Network Check Passed");

            //progressbar.setVisibility(View.VISIBLE);

            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(5, TimeUnit.SECONDS)
                    .build();
            //.writeTimeout(20, TimeUnit.SECONDS)
            //.readTimeout(30, TimeUnit.SECONDS)

            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(okHttpClient)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }

            LoginApi LoginService = retrofit.create(LoginApi.class);
            Call<LoginResponse> call = null;

            if(type.equalsIgnoreCase("phone")) {
                call = LoginService.loginUserPhone(newLoginPhone);
            }else if(type.equalsIgnoreCase("email")){
                call = LoginService.loginUserEmail(newLoginEmail);
            }

            Log.e("LoginActivity: Status","Retrofit Login HTTP Call");

            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if(response.code()==401){
                        //progressDialog.dismiss();
                        //progressbar.setVisibility(View.INVISIBLE);
                        showLoginFailed();
                    }
                    else{
                        if(response.body()!=null){
                            loginResponse = response.body();

                            Log.e("LoginActivity: Login Response",loginResponse.toString());

                            //Cache Token Response
                            editor = pref.edit();
                            editor.putString("loginToken", loginResponse.getToken());
                            loginToken = loginResponse.getToken();
                            editor.putString("loginId", loginResponse.getId());
                            loginID = loginResponse.getId();

                            editor.putString("loginEmail", loginResponse.getEmail());
                            emailID = loginResponse.getEmail();

                            editor.putString("loginPhone", loginResponse.getPhoneNumber());
                            userPhone = loginResponse.getPhoneNumber();

                            editor.putString("loginName", loginResponse.getName());

                            //Appending Child ID if Missing
                            if(loginResponse.getLastdevid()!=null){
                                Log.e("LAST DID",loginResponse.getLastdevid());
                                editor.putString("devid", loginResponse.getLastdevid()+"-0"+
                                        loginResponse.getChildId());
                            }else{
                                editor.putString("devid", "0");
                            }
                            editor.putString("loginPassword", pass);

                            if(loginResponse.getAddress()!=null)
                            {
                                Log.e("LoginActivity: loginAddress",loginResponse.getAddress());
                                editor.putString("loginAddress", loginResponse.getAddress());
                                Log.e("LoginActivity: loginCity",loginResponse.getCity());
                                editor.putString("loginCity", loginResponse.getCity());
                                Log.e("LoginActivity: loginCountry",loginResponse.getCountry());
                                editor.putString("loginCountry", loginResponse.getCountry());
                                Log.e("LoginActivity: loginState",loginResponse.getState());
                                editor.putString("loginState", loginResponse.getState());
                            }
                            else{
                                Log.e("LoginActivity: Status","No address registered.");
                                editor.putString("loginAddress", " ");
                                editor.putString("loginCity", " ");
                                editor.putString("loginCountry", " ");
                                editor.putString("loginState", " ");
                            }
                            editor.apply();

                            //Get Previous TransactionID
                            List<String> transactionIDList = loginResponse.getTransactionId();
                            if((transactionIDList == null)||(transactionIDList.isEmpty())){

                            }else{
                                if((transactionIDList!=null)&&(transactionIDList.size()>0)) {
                                    String tIDS = transactionIDList.get(0);
                                    for(int pp=1; pp<transactionIDList.size(); pp++){
                                        tIDS = tIDS + ","+transactionIDList.get(pp);
                                    }
                                    //Log.e("LoginActivity: Old Transactions", tIDS.toString());
                                    editor.putString("transactions",tIDS.toString());

                                    String recenttransaction = transactionIDList.get(transactionIDList.size() - 1);
                                    Log.e("LoginActivity: Recent Transaction", recenttransaction);
                                    editor.putString("transID",recenttransaction);
                                    editor.apply();
                                    checkTransactionID(recenttransaction, loginResponse.getLastdevid());

                                    //LATER
                                    /*
                                    Log.e("LoginActivity: STATUS", "Why Return?");
                                    String tIDS = transactionIDList.get(0);
                                    Log.e("LoginActivity: TIDS0", tIDS);
                                    for(int pp=1; pp<transactionIDList.size(); pp++){
                                        tIDS = tIDS + ","+transactionIDList.get(pp);
                                    }
                                    Log.e("LoginActivity: Old Transactions", tIDS.toString());
                                    editor.putString("transactions",tIDS.toString());
                                    editor.apply();
                                    */
                                }
                            }

                            //Check for both contacts email & phone
                            if((loginResponse.getEmail()==null)||
                                    (loginResponse.getEmail()==""))
                            {
                                Log.e("Status","Email Check Failed!");

                                View dialogView = getLayoutInflater().inflate(R.layout.bottom_sheet_email, null);
                                dialogEmail = new BottomSheetDialog(LoginActivity.this);
                                dialogEmail.setContentView(dialogView);

                                Button SaveButton = dialogView.findViewById(R.id.ButtonSave);
                                EditText userEmail = dialogView.findViewById(R.id.EditTextEmail);
                                SaveButton.setOnClickListener(v->{
                                    editor.putString("loginEmail", userEmail.getText().toString());
                                    editor.apply();
                                    updateUser("email",userEmail.getText().toString());
                                    dialogEmail.dismiss();
                                    Toast.makeText(LoginActivity.this,
                                            "Kindly relogin to start using app.",
                                            Toast.LENGTH_SHORT).show();
                                    loginSuccessBool = true;
                                });

                                dialogEmailBool = true;
                                dialogEmail.show();
                            }

                            editor.putString("loginPhone", loginResponse.getPhoneNumber());
                            userPhone = loginResponse.getPhoneNumber();

                            //Log.e("Phone",loginResponse.getPhoneNumber());
                            //if(loginResponse.getPhoneNumber().length()==0)
                            if((loginResponse.getPhoneNumber()==null)||
                                    (loginResponse.getPhoneNumber()==""))
                            {
                                Log.e("Status","Phone Check Failed!");
                                dialogPhoneBool = true;

                                View dialogView = getLayoutInflater().inflate(R.layout.bottom_sheet_phone, null);
                                dialogPhone = new BottomSheetDialog(LoginActivity.this);
                                dialogPhone.setContentView(dialogView);

                                Button SaveButton = dialogView.findViewById(R.id.ButtonSave);
                                EditText userPhoneNumber = dialogView.findViewById(R.id.EditTextPhone);
                                SaveButton.setOnClickListener(v->{
                                    editor.putString("loginPhone", userPhoneNumber.getText().toString());
                                    editor.apply();
                                    updateUser("phone",userPhoneNumber.getText().toString());
                                    dialogPhoneBool = false;
                                    dialogPhone.dismiss();
                                    Toast.makeText(LoginActivity.this,
                                            "Kindly relogin to start using app.",
                                            Toast.LENGTH_SHORT).show();
                                });

                                dialogPhone.show();
                            }

                            Log.e("LoginActivity: Login Token", loginResponse.getToken());

                            //progressDialog.dismiss();
                            ////progressbar.setVisibility(View.INVISIBLE);
                            loginSuccess(loginResponse.getToken(), pass);
                        }
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable throwable) {
                    //progressDialog.dismiss();
                    //progressbar.setVisibility(View.INVISIBLE);
                    showLoginFailed();
                }
            });
        }else{
            Toast.makeText(this, "No Network Available. Kindly switch on Internet and Try again.", Toast.LENGTH_SHORT).show();
        }
    }

    /*
    private void tryLogin11(String type, String id, String pass)
    {
        Log.e("Status","tryLogin");
        
        LoginUserPhone newLoginPhone = null;
        LoginUserEmail newLoginEmail = null;

        if(type.equalsIgnoreCase("phone")){
            newLoginPhone = new LoginUserPhone("phone", id, pass);
            Log.e("LoginUserPhone",newLoginPhone.toString());
        }else if(type.equalsIgnoreCase("email")){
            newLoginEmail = new LoginUserEmail("email", id, pass);
            Log.e("LoginUserEmail",newLoginEmail.toString());
        }

        if(checkNetwork()){
            Log.e("Status","Network Check Passed");

            //final ProgressDialog progressDialog = ProgressDialog.show(this, "Authenticating", "Please wait", false, false);

            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(5, TimeUnit.SECONDS)
                    .build();
                    //.writeTimeout(20, TimeUnit.SECONDS)
                    //.readTimeout(30, TimeUnit.SECONDS)

            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .client(okHttpClient)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }

            LoginApi LoginService = retrofit.create(LoginApi.class);
            Call<LoginResponse> call = null;

            if(type.equalsIgnoreCase("phone")) {
                call = LoginService.loginUserPhone(newLoginPhone);
            }else if(type.equalsIgnoreCase("email")){
                call = LoginService.loginUserEmail(newLoginEmail);
            }

            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                    if((response.code()==401)||(response.code()==402))
                    {
                        showLoginFailed();
                    }
                    else if(response.code()==502){
                        Snackbar snackbar = Snackbar
                                .make(clayout,"Server Issue. Kindly Login again later.", Snackbar.LENGTH_LONG);
                        snackbar.show();

                        editor = pref.edit();
                        editor.clear();
                        editor.apply();
                    }
                    else if(response.code()==200){
                        loginResponse = response.body();

                        Log.e("Login Response",loginResponse.toString());

                        //Cache Token Response
                        editor = pref.edit();
                        editor.putString("loginToken", loginResponse.getToken());
                        editor.putString("loginId", loginResponse.getId());
                        loginID = loginResponse.getId();
                        editor.putString("loginEmail", loginResponse.getEmail());
                        emailID = loginResponse.getEmail();

                        editor.putString("loginName", loginResponse.getName());
                        Log.e("LAST DID",loginResponse.getLastdevid());
                        if(loginResponse.getLastdevid().length()<17){
                            editor.putString("devid", loginResponse.getLastdevid()+"-0"+
                                    loginResponse.getChildId());
                        }

                        childid = Integer.toString(loginResponse.getChildId());
                        editor.putString("oldChildID", Integer.toString(loginResponse.getChildId()));
                        editor.putString("loginPassword", pass);
                        editor.apply();

                        if(loginResponse.getEmail()==null){

                            Log.e("Status","Email Check Failed!");

                            View dialogView = getLayoutInflater().inflate(R.layout.bottom_sheet_email, null);
                            dialogEmail = new BottomSheetDialog(LoginActivity.this);
                            dialogEmail.setContentView(dialogView);

                            Button SaveButton = dialogView.findViewById(R.id.ButtonSave);
                            EditText userEmail = dialogView.findViewById(R.id.EditTextEmail);
                            SaveButton.setOnClickListener(v->{
                                editor.putString("loginEmail", userEmail.getText().toString());
                                editor.apply();
                                updateUser("email",userEmail.getText().toString());
                                dialogEmail.dismiss();
                                Toast.makeText(LoginActivity.this,
                                        "Kindly relogin to start using app.",
                                        Toast.LENGTH_SHORT).show();
                                loginSuccessBool = true;
                            });

                            dialogEmailBool = true;
                            dialogEmail.show();
                        }

                        editor.putString("loginPhone", loginResponse.getPhoneNumber());
                        userPhone = loginResponse.getPhoneNumber();

                        //Log.e("Phone",loginResponse.getPhoneNumber());
                        //if(loginResponse.getPhoneNumber().length()==0)
                        if(loginResponse.getPhoneNumber()==null)
                        {
                            Log.e("Status","Phone Check Failed!");
                            dialogPhoneBool = true;

                            View dialogView = getLayoutInflater().inflate(R.layout.bottom_sheet_phone, null);
                            dialogPhone = new BottomSheetDialog(LoginActivity.this);
                            dialogPhone.setContentView(dialogView);

                            Button SaveButton = dialogView.findViewById(R.id.ButtonSave);
                            EditText userPhoneNumber = dialogView.findViewById(R.id.EditTextPhone);
                            SaveButton.setOnClickListener(v->{
                                editor.putString("loginPhone", userPhoneNumber.getText().toString());
                                editor.apply();
                                updateUser("phone",userPhoneNumber.getText().toString());
                                dialogPhoneBool = false;
                                dialogPhone.dismiss();
                                Toast.makeText(LoginActivity.this,
                                        "Kindly relogin to start using app.",
                                        Toast.LENGTH_SHORT).show();
                            });

                            dialogPhone.show();
                        }

                        logPass = pass;

                        if(loginResponse.getAddress()!=null)
                        {
                            Log.e("loginAddress",loginResponse.getAddress());
                            editor.putString("loginAddress", loginResponse.getAddress());
                            Log.e("loginCity",loginResponse.getCity());
                            editor.putString("loginCity", loginResponse.getCity());
                            Log.e("loginCountry",loginResponse.getCountry());
                            editor.putString("loginCountry", loginResponse.getCountry());
                            Log.e("loginState",loginResponse.getState());
                            editor.putString("loginState", loginResponse.getState());
                        }
                        else{
                            Log.e("Status","No address registered.");
                            editor.putString("loginAddress", " ");
                            editor.putString("loginCity", " ");
                            editor.putString("loginCountry", " ");
                            editor.putString("loginState", " ");
                        }
                        editor.apply();

                        //Get Previous TransactionID
                        List<String> transactionIDList = loginResponse.getTransactionId();
                        if((transactionIDList == null)||(transactionIDList.isEmpty())){

                        }else{
                            if((transactionIDList!=null)&&(transactionIDList.size()>0)) {
                                Log.e("Old Transactions", transactionIDList.get(0));
                                String recenttransaction = transactionIDList.get(transactionIDList.size() - 1);
                                Log.e("LoginActivity: Recent Transaction", recenttransaction);
                                editor.putString("transID",recenttransaction);
                                editor.apply();
                                checkTransactionID(transactionIDList.get(transactionIDList.size() - 1),
                                        loginResponse.getLastdevid());
                            }
                        }
                        Log.e("Login Token", loginResponse.getToken());

                        //progressDialog.dismiss();

                        if(loginSuccessBool){
                            loginToken = loginResponse.getToken();
                            loginSuccess(loginResponse.getToken(), pass);
                        }
                    }
                    else {
                        Log.e("Login Response","Failed");
                        //progressDialog.dismiss();
                        showLoginFailed();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable throwable) {
                    //progressDialog.dismiss();
                    showLoginFailed();
                }
            });
        }else{
            Toast.makeText(this, "No Network Available. Kindly switch on Internet and Try again.", Toast.LENGTH_SHORT).show();
        }
    }*/

    private void updateUser(String type, String information) {

        Log.e("Update Information",information);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        UpdateUserDetailsApi UpdateService = retrofit.create(UpdateUserDetailsApi.class);

        if(type.equalsIgnoreCase("phone"))
        {
            UpdateUserPhoneRequest updateUserPhoneRequest = new UpdateUserPhoneRequest(loginID,emailID,information);
            Call<UpdatedUserResponse> call = UpdateService.updateUserPhone(updateUserPhoneRequest);

            //Log.e("Call",call.toString());
            Log.e("Request String",updateUserPhoneRequest.toString());

            call.enqueue(new Callback<UpdatedUserResponse>() {
                @Override
                public void onResponse(Call<UpdatedUserResponse> call, Response<UpdatedUserResponse> response) {
                    String responseString = "";

                    if (response.isSuccessful()) {
                        responseString = response.body().toString();
                        Log.e("User Update","Successful");

                        editor.putString("loginPhone", information);
                        editor.putString("loginEmail", userID);
                        editor.putString("loginName", loginResponse.getName());
                        editor.commit();

                        if(dialogPhone.isShowing()){
                            dialogPhoneBool = false;
                            dialogPhone.dismiss();
                        }

                        Toast.makeText(LoginActivity.this, "Mobile number updated. Kindly re-login.", Toast.LENGTH_SHORT).show();
                        loginSuccessBool = true;


                        Log.e("Status","Invoke MultiMap 1");
                        Intent i = getBaseContext().getPackageManager().
                                getLaunchIntentForPackage(getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }
                    else{
                        Log.e("User Update response","Update Failed.");
                    }

                    //Cache Token Response
                    editor.clear();
                    editor.apply();
                }

                @Override
                public void onFailure(Call<UpdatedUserResponse> call, Throwable throwable) {
                    Log.e("User response","Update Failed.");
                }
            });
        }else{

            UpdateUserEmailRequest updateUserEmailRequest = new UpdateUserEmailRequest(loginID,information,userPhone);
            Call<UpdatedUserResponse> call = UpdateService.updateUserEmail(updateUserEmailRequest);

            Log.e("Call",call.toString());
            Log.e("Request String",updateUserEmailRequest.toString());

            call.enqueue(new Callback<UpdatedUserResponse>() {
                @Override
                public void onResponse(Call<UpdatedUserResponse> call, Response<UpdatedUserResponse> response) {
                    String responseString = "";

                    if (response.isSuccessful()) {
                        responseString = response.body().toString();

                        if(responseString!=null){
                            Log.e("User Update response",responseString);
                        }

                        if(dialogPhone!=null)
                        {
                            if(dialogPhone.isShowing()){
                                dialogPhone.dismiss();
                                dialogPhoneBool = false;
                            }
                        }
                        dialogPhoneBool = false;

                        Intent inte = new Intent(LoginActivity.this, SplashActivity.class);
                        startActivity(inte);
                        finish();
                    }
                    else{
                        Log.e("User Update response","Update Failed.");
                    }

                    //Cache Token Response
                    editor.clear();
                    editor.apply();
                }

                @Override
                public void onFailure(Call<UpdatedUserResponse> call, Throwable throwable) {
                    Log.e("User response","Update Failed.");
                }
            });
        }
    }

    private boolean checkNetwork() {
        final ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    private void loginSuccess(String loginToken, String loginPassword) {

        Log.e("Login Activity: Status","Login Success");

        editor.putString("loginPassword", loginPassword);
        editor.apply();

        //TODO TEMP FORGET OLD DETAILS
        String tID = "0";
        String devid = "0";

        /*
        editor.putString("transID",null);
        editor.putString("devid",null);

        Intent intent = new Intent(this, MultiStationsMapActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("loginToken",loginToken);
        finish();
        startActivity(intent);*/

        if(pref.getString("transID", "0")!="0"){
            tID = pref.getString("transID", "0");
            devid = pref.getString("devid", "0");

            Log.e("Status","Checking Previous Transaction");
            if(tID!="0")
            {
                checkTransactionID(tID, devid);
            }
            else {
                editor.putString("transID","0");

                Toast.makeText(getApplicationContext(), "Welcome", Toast.LENGTH_LONG).show();
                //Intent intent = new Intent(this, GridNavigationMenuActivity.class);
                Log.e("Status","Login Success! Going to MultiStationMapActivity");
                Log.e("Status Email",Boolean.toString(dialogEmailBool));
                Log.e("Status Phone",Boolean.toString(dialogPhoneBool));

                if(dialogEmailBool) {
                    dialogEmail.show();
                }
                else if(dialogPhoneBool){
                    dialogPhone.show();
                }
                else {
                    //Intent intent = new Intent(this, CachedMapActivity.class);
                    Log.e("Status","No previous transactions");
                    Intent intent = new Intent(this, MultiStationsMapActivity.class);
                    intent.putExtra("loginToken",loginToken);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    finish();
                    startActivity(intent);
                }
            }
        }
        else{
            editor.putString("transID","0");

            Toast.makeText(getApplicationContext(), "Welcome", Toast.LENGTH_LONG).show();
            //Intent intent = new Intent(this, GridNavigationMenuActivity.class);
            Log.e("Status","Login Success! Going to MultiStationMapActivity");
            Log.e("Status Email",Boolean.toString(dialogEmailBool));
            Log.e("Status Phone",Boolean.toString(dialogPhoneBool));

            if(dialogEmailBool) {
                dialogEmail.show();
            }
            else if(dialogPhoneBool){
                dialogPhone.show();
            }
            else {
                //Intent intent = new Intent(this, CachedMapActivity.class);
                Log.e("Status","No previous transactions");
                Intent intent = new Intent(this, MultiStationsMapActivity.class);
                intent.putExtra("loginToken",loginToken);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                finish();
                startActivity(intent);
            }
        }
    }

    private void showLoginFailed() {
        Log.e("Status","Login Failed");

        Snackbar snackbar = Snackbar
                .make(clayout,"Incorrect Login Credentials. Kindly Check Again.", Snackbar.LENGTH_LONG);
        snackbar.show();

        //passwordEditText.setText("Incorrect Login ID or Password");
        editor = pref.edit();
        editor.clear();
        editor.apply();
    }

    @VisibleForTesting(otherwise = VisibleForTesting.NONE)
    public void resetCache() {
        pref = this.getSharedPreferences("LoginPref", 0);
        editor = pref.edit();
        editor.clear();
        editor.apply();
    }

    @VisibleForTesting(otherwise = VisibleForTesting.NONE)
    public void setCache() {
        pref = this.getSharedPreferences("LoginPref", 0);
        editor = pref.edit();
        editor.putString("loginPassword", "");
        editor.apply();
    }

    private void recoverPassword(final String identifier)
    {
        Log.e("Status","requestRecovery");
        Log.e("id",identifier);

        final ProgressDialog progressDialog = ProgressDialog.show(this, "Recovering Password", "Please wait", false, false);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        PasswordApi RecoverService = retrofit.create(PasswordApi.class);

        Log.e("Status","Retrofit Recover Password HTTP Call");

        if(identifier.contains("@"))
        {
            Log.e("Status","Reset on EMail");
            Call<String> call = RecoverService.requestPasswordByEmail(new RecoverRequestEmail(identifier));

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    String responseString = "";

                    Log.e("Status","Response Code on Forgot Password: "+response.code());

                    if (response.isSuccessful()) {
                        responseString = response.body();
                        Log.e("Recovery Response",responseString);

                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        builder.setTitle("New Password Generated!");
                        builder.setMessage("Kindly check your registered email for new password.");
                        builder.setPositiveButton("OK", (dialog, which) -> {
                            dialog.cancel();
                        });

                        progressDialog.dismiss();
                        builder.show();
                    }
                    else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        builder.setTitle("Email Address not Found!");
                        builder.setMessage("Kindly enter your registered email for new password.");
                        builder.setPositiveButton("OK", (dialog, which) -> {
                            dialog.cancel();
                        });

                        progressDialog.dismiss();
                        builder.show();
                    }

                    //Cache Token Response
                    editor = pref.edit();
                    editor.clear();
                    editor.apply();
                }

                @Override
                public void onFailure(Call<String> call, Throwable throwable) {
                    progressDialog.dismiss();
                    showLoginFailed();
                }
            });
        }
        else if(isNumeric(identifier)){
            Log.e("Status","Reset on Phone");

            Call<String> call = RecoverService.requestPasswordByPhone(new RecoverRequestPhone(identifier));

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    String responseString = "";

                    if (response.isSuccessful()) {
                        responseString = response.body();
                        Log.e("Recovery Response",responseString);

                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        builder.setTitle("New Password Generated!");
                        builder.setMessage("Kindly check your registered phone for new password.");
                        builder.setPositiveButton("OK", (dialog, which) -> {
                            dialog.cancel();
                        });

                        builder.show();
                    }
                    else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        builder.setTitle("Phone number not Found!");
                        builder.setMessage("Kindly enter your registered phone number for new password.");
                        builder.setPositiveButton("OK", (dialog, which) -> {
                            dialog.cancel();
                        });

                        builder.show();
                    }

                    //Cache Token Response
                    editor.clear();
                    editor.apply();

                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<String> call, Throwable throwable) {
                    progressDialog.dismiss();
                    showLoginFailed();
                }
            });
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setTitle("Incorrect phone/email.");
            builder.setMessage("Kindly enter registered phone number or email address only.");
            builder.setPositiveButton("Ok", (dialog, which) -> {
                dialog.cancel();
            });

            builder.show();
        }
    }

    public static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    private void checkTransactionID(String tid, String devid) {
        Log.e("LoginActivity: Transaction ID", tid);
        String devid111 = devid;
        Log.e("LoginActivity: Old Device ID", devid111);

        if(devid.length()<17){
            String childid = pref.getString("oldChildID","00");

            if(childid.length()<2){
                devid111 = devid+"-0"+childid;
            }else{
                devid111 = devid+"-"+childid;
            }
        }

        /*
        Intent intent = new Intent(LoginActivity.this, MultiStationsMapActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("loginToken",loginToken);
        finish();
        startActivity(intent);
        */

        //TODO Check if old transaction is ongoing
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        TransactionRequest transactionRequest = new TransactionRequest(tid);

        TransactionAPI transactionService = retrofit.create(TransactionAPI.class);
        Call<TransactionResponse> call = transactionService.getTransactionResponse(transactionRequest);

        Log.e("LoginActivity: Status", "Fetching Transaction Details");

        call.enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                    TransactionResponse transactionResponse = response.body();
                    Log.e("LoginActivity: TransactionResponse Code", Integer.toString(response.code()));
                    if (response.code() == 400) {
                        Log.e("LoginActivity: Status", "Session is on going!!");

                        Intent homeint = new Intent(LoginActivity.this, UserControlActivityPre.class);
                        homeint.putExtra("devid",devid);
                        Log.e("LAST DID 400", devid);
                        editor.putString("devid",devid);
                        editor.putString("oldChildID",childid);
                        editor.putString("transID",tid);
                        editor.commit();
                        finish();
                        startActivity(homeint);
                    } else if (response.isSuccessful()) {
                        Log.e("LoginActivity: Status", "Session Stopped.");
                        Log.e("LoginActivity: Previous Transaction", transactionResponse.toString());
                        Log.e("LoginActivity: Transaction Status", transactionResponse.getTransactionId());
                        Log.e("LoginActivity: Trasnsaction Response", transactionResponse.toString());

                        String elapsedtime = "";
                        long unixDiffSeconds = transactionResponse.getEndTime()-transactionResponse.getStartTime();
                        long elapsed_hours;
                        long elapsed_minutes;
                        long elapsed_seconds;
                        elapsed_hours = TimeUnit.SECONDS.toHours(unixDiffSeconds);
                        elapsed_minutes = TimeUnit.SECONDS.toMinutes(unixDiffSeconds) - (TimeUnit.SECONDS.toHours(unixDiffSeconds) * 60);
                        elapsed_seconds = TimeUnit.SECONDS.toSeconds(unixDiffSeconds) - (TimeUnit.SECONDS.toMinutes(unixDiffSeconds) * 60);

                        if (elapsed_hours == 0 && elapsed_minutes == 0) {
                            elapsedtime = elapsed_seconds + " s";
                        } else if (elapsed_hours == 0) {
                            elapsedtime = elapsed_minutes + " m: " + elapsed_seconds + " s";
                        } else {
                            elapsedtime = elapsed_hours + " h: " + elapsed_minutes + " m: " + elapsed_seconds + " s";
                        }

                        TransactionEntity transactionE = new TransactionEntity(transactionResponse.getId(),
                                transactionResponse.getDeviceId(), transactionResponse.getChildId(),
                                epochToHumanDate(transactionResponse.getStartTime()),
                                transactionResponse.getStartTime(),
                                epochToMonthNumber(transactionResponse.getStartTime()),
                                transactionResponse.getTransactionId(),
                                transactionResponse.getEnergyConsumed(),
                                elapsedtime,
                                transactionResponse.getSoc());

                        AddTransactionAsync addTask = new AddTransactionAsync(getApplicationContext(),transactionE);
                        addTask.execute();

                        editor.putString("devid","0");
                        editor.putString("transID","0");
                        editor.commit();

                        Toast.makeText(getApplicationContext(), "Welcome", Toast.LENGTH_LONG).show();
                        Log.e("LoginActivity: Status","Login Success! Going to Splash >> MultiStationMapActivity");
                        Intent intent = new Intent(LoginActivity.this, MultiStationsMapActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("loginToken",loginToken);
                        finish();
                        startActivity(intent);
                    }
            }

            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable throwable) {
                Log.e("LoginActivity: Status", "Retrofit session status HTTP Calling Failed");
            }
        });
    }

    private String epochToHumanDate(long epochSeconds){
        long itemLong = epochSeconds;
        java.util.Date d = new java.util.Date(itemLong*1000L);
        String itemDateStr = new SimpleDateFormat("dd-MMM-YY | HH:mm a").format(d);
        return itemDateStr;
    }

    private int epochToMonthNumber(long epochSeconds){
        long itemLong = epochSeconds;
        java.util.Date d = new java.util.Date(itemLong*1000L);
        String itemDateStr = new SimpleDateFormat("MM").format(d);
        return Integer.valueOf(itemDateStr);
    }
}
