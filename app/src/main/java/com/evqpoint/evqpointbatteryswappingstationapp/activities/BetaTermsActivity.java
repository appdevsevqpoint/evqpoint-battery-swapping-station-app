package com.evqpoint.evqpointbatteryswappingstationapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;

public class BetaTermsActivity extends AppCompatActivity {

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        pref = this.getSharedPreferences("LoginPref", 0);
        editor = pref.edit();

        TextView tvURL = findViewById(R.id.tvURL);
        tvURL.setOnClickListener(view -> {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://drive.google.com/open?id=1m9bwZn5GhTK9FfWyxAgFsWoIzTttDeR9")));
        });

        Button btnAccept= (Button) findViewById(R.id.button);
        btnAccept.setOnClickListener(view -> {
            Intent intent = new Intent(BetaTermsActivity.this, RegisterActivity.class);
            finish();
            startActivity(intent);
        });
    }
}