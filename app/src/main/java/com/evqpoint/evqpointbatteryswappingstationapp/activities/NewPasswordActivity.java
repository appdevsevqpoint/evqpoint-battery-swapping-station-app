package com.evqpoint.evqpointbatteryswappingstationapp.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.network.password.ChangePasswordByEmailRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.password.ChangePasswordByPhoneRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.password.PasswordApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class NewPasswordActivity extends AppCompatActivity {

    String email;
    String EOTP;

    //RETROFIT
    public static final String BASE_URL = "https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    //CachedOTP
    SharedPreferences pref;

    //UI
    EditText editNewPassword;
    EditText editReNewPassword;
    EditText editOldPassword;
    Button btnSend;

    String userpass = "";
    String userMail = "";
    String userPhone = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_update);

        pref = this.getSharedPreferences("LoginPref", 0);
        if(pref.getString("loginPassword", null) != null) {
            userpass = pref.getString("loginPassword", null);
        }

        if(pref.getString("loginPhone", "") != null) {
            userPhone = pref.getString("loginPhone", null);
        }

        if(pref.getString("loginEmail", "") != null) {
            userMail = pref.getString("loginEmail", null);
        }

        editNewPassword = findViewById(R.id.editNewPassword);
        editReNewPassword = findViewById(R.id.editReNewPassword);
        editOldPassword = findViewById(R.id.editOldPassword);
        btnSend = findViewById(R.id.btnChangePass);

        btnSend.setOnClickListener(v -> {
            String newpass1 = editNewPassword.getText().toString();
            String newpass2 = editReNewPassword.getText().toString();
            String oldpass = editOldPassword.getText().toString();

            if((newpass1!=null)&&(newpass2!=null)&&(oldpass!=null)){
                if((newpass1.trim()!="")&&(newpass2.trim()!="")&&(oldpass.trim()!="")){
                    if(oldpass.equals(userpass)){
                        if(newpass1.equals(newpass2)){
                            updatePassword(oldpass, newpass1);
                        }else{
                            editReNewPassword.setError("New Password mismatched");
                        }
                    }else{
                        editOldPassword.setError("Incorrect Current Password Entered");
                    }
                }else{
                    Toast.makeText(this, "Please enter all fields.", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(this, "Please enter all fields.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updatePassword(String oldpass, String newpass1) {
        final String BASE_URL = "https://client.evqpoint.com/api/user/";

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        PasswordApi changePassService = retrofit.create(PasswordApi.class);
        Call<String> call = null;

        ChangePasswordByPhoneRequest newPassReqPhone;
        ChangePasswordByEmailRequest newPassReqEmail;

        if((userMail!="")){
            newPassReqEmail = new ChangePasswordByEmailRequest(
                    oldpass, newpass1, userMail
            );
            Log.e("NewPasswordActivity: Password by","Email");

            call = changePassService.requestPasswordChangeByEmail(newPassReqEmail);
        }
        else if(userPhone!=""){
            newPassReqPhone = new ChangePasswordByPhoneRequest(
                    oldpass, newpass1, userPhone
            );
            Log.e("NewPasswordActivity: Password by","Phone");

            call = changePassService.requestPasswordChangeByPhone(newPassReqPhone);
        }

        /*if((userMail!="")){
            newPassReqEmail = new ChangePasswordByEmailRequest(
                    oldpass, newpass1, userMail
            );
            Log.e("NewPasswordActivity: Password by","Email");

            call = changePassService.requestPasswordChangeByEmail(newPassReqEmail);
        }*/

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                String responseString = "";

                if (response.isSuccessful()) {
                    responseString = response.body().toString();

                    if(responseString!=null){
                        Toast.makeText(NewPasswordActivity.this,
                                "Password Updated", Toast.LENGTH_SHORT).show();
                    }
                }else if (response.code()==401) {
                    responseString = response.body().toString();

                    if(responseString!=null){
                        Toast.makeText(NewPasswordActivity.this,
                                "Incorrect Old Password", Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    Log.e("NewPasswordActivity: Password","Update Failed.");
                    Toast.makeText(NewPasswordActivity.this, "Password Change Failed.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable throwable) {
                Log.e("NewPasswordActivity: ","Update Failed.");
            }
        });
    }

}