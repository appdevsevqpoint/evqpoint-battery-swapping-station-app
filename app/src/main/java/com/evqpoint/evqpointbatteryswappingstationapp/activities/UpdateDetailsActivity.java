package com.evqpoint.evqpointbatteryswappingstationapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.vehicles.VehicleListActivity;

public class UpdateDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_update_details);

        Button btnAccountDetails = findViewById(R.id.accDetailsButton);
        btnAccountDetails.setOnClickListener(v ->{
            Intent deetsIntent1 = new Intent(UpdateDetailsActivity.this, AccountDetailsActivity.class);
            startActivity(deetsIntent1);
        });

        Button btnVehicleDetails = findViewById(R.id.vehicleDetailsButton);
        btnVehicleDetails.setOnClickListener(v ->{
            Log.e("Vehicle Details","Button Clicked");
            //Intent deetsIntent2 = new Intent(UpdateDetailsActivity.this, VehicleDetailsActivity.class);
            Intent deetsIntent2 = new Intent(UpdateDetailsActivity.this, VehicleListActivity.class);
            startActivity(deetsIntent2);
        });

        Button btnAddressDetails = findViewById(R.id.addressButton);
        btnAddressDetails.setOnClickListener(v ->{
            Intent deetsIntent3 = new Intent(UpdateDetailsActivity.this, AddressDetailsActivity.class);
            startActivity(deetsIntent3);
        });

    }
}
