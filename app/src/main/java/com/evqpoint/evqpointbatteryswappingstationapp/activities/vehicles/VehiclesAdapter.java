package com.evqpoint.evqpointbatteryswappingstationapp.activities.vehicles;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.evqpoint.evqpointbatteryswappingstationapp.R;

import java.util.List;

import retrofit2.Retrofit;

public class VehiclesAdapter extends RecyclerView.Adapter<VehiclesAdapter.VehicleViewHolder> {

    //RETROFIT
    public static final String BASE_URL = "https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    private List<Vehicle> vehiclesList;
    String userid;
    SharedPreferences pref;
    Context context;

    public class VehicleViewHolder extends RecyclerView.ViewHolder {

        TextView tvVehicleName;
        TextView tvVehicleMake;
        TextView tvVehicleModel;
        ImageView ivVehiclePic;
        Context context;

        public VehicleViewHolder(@NonNull final View itemView) {
            super(itemView);
            context = itemView.getContext();

            tvVehicleName = itemView.findViewById(R.id.tvVehicleName);
            tvVehicleMake = itemView.findViewById(R.id.editVehicleMake);
            tvVehicleModel = itemView.findViewById(R.id.editVehicleModel);
            ivVehiclePic = itemView.findViewById(R.id.ivVehicleImage);
        }
    }

    public VehiclesAdapter(List<Vehicle> vehiclesListIn, Context inContext) {
        this.vehiclesList = vehiclesListIn;
        context = inContext;
        pref = inContext.getSharedPreferences("LoginPref", 0);
        userid = pref.getString("loginId", null);
    }

    @NonNull
    @Override
    public VehicleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vehicle_item_view, parent, false);

        return new VehicleViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VehicleViewHolder holder, int position) {
        final Vehicle vehicle = vehiclesList.get(position);

        holder.tvVehicleName.setText(vehicle.getVehicleNickName());
        holder.tvVehicleMake.setText(vehicle.getVehicleMake());
        holder.tvVehicleModel.setText(vehicle.getVehicleModel());
        holder.ivVehiclePic.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return this.vehiclesList.size();
    }

}
