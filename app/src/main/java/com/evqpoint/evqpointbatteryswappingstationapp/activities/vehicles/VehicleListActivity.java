package com.evqpoint.evqpointbatteryswappingstationapp.activities.vehicles;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.network.vehicles.GetAllVehiclesRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.vehicles.GetAllVehiclesResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.vehicles.VehiclesAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VehicleListActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private VehiclesAdapter mAdapter;
    List<Vehicle> vehiclesList;
    private TextView placeholderTextView;

    public static final String BASE_URL = "https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    //User Details Cached
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String userID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_vehicles);

        pref = this.getSharedPreferences("LoginPref", 0);
        userID = pref.getString("loginId", null);

        //placeholderTextView =  findViewById(R.id.placeholder_textview);
        placeholderTextView =  findViewById(R.id.placeholderVehicles);
        mRecyclerView = findViewById(R.id.vehicles_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, 0));

        //Init dataset
        vehiclesList = new ArrayList<>();
        setupDB();
        Log.e("Number of Vehicles",Integer.toString(vehiclesList.size()));

        /*
        //TODO Test this piece of defensive logic
        if((vehiclesList!=null)&&(vehiclesList.size()!=0))
            Log.e("Number of Vehicles",Integer.toString(vehiclesList.size()));
        */

        //RECYCLER ADAPTER
        //mAdapter = new VehiclesAdapter(vehiclesList, getApplicationContext());
        //mRecyclerView.setAdapter(mAdapter);
        /*
        if(vehiclesList.size()>0){
            mAdapter = new VehiclesAdapter(vehiclesList, getApplicationContext());
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setVisibility(View.VISIBLE);
        }else{
            mRecyclerView.setVisibility(View.GONE);
            placeholderTextView.setVisibility(View.VISIBLE);
        }*/

        //Add Vehicle FAB
        FloatingActionButton fabAdd = findViewById(R.id.fabAdd);
        fabAdd.setOnClickListener(v->{
            Intent addVehicleIntent = new Intent(VehicleListActivity.this, AddVehicleActivity.class);
            startActivity(addVehicleIntent);
        });
    }

    public void setupDB() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        VehiclesAPI getVehiclesService = retrofit.create(VehiclesAPI.class);
        Call<GetAllVehiclesResponse> call = getVehiclesService.getAllVehicles(new GetAllVehiclesRequest(userID));

        call.enqueue(new Callback<GetAllVehiclesResponse>() {
            @Override
            public void onResponse(Call<GetAllVehiclesResponse> call, Response<GetAllVehiclesResponse> response) {

                if (response.isSuccessful()) {
                    GetAllVehiclesResponse getAllVehicles = response.body();
                    vehiclesList = getAllVehicles.fetchVehicles();

                    Log.e("Vehicles Fetched", Integer.toString(vehiclesList.size()));

                    if(vehiclesList.size()!=0) {
                        Log.v("Vehicle 0", vehiclesList.get(0).toString());
                        Log.v("Vehicle 0 Details", vehiclesList.get(0).getVehicleNickName());

                        for (int k = 0; k < getAllVehicles.getSize(); k++) {
                            Log.e("Vehicle " + k + " Detail:", vehiclesList.get(k).toString());
                        }
                    }

                    if(vehiclesList.size()>0){
                        mAdapter = new VehiclesAdapter(vehiclesList, getApplicationContext());
                        mRecyclerView.setAdapter(mAdapter);
                        mRecyclerView.setVisibility(View.VISIBLE);
                    }else{
                        mRecyclerView.setVisibility(View.GONE);
                        placeholderTextView.setVisibility(View.VISIBLE);
                    }
                }
                else{
                    Log.e("Vehicle Addition","Failed.");
                    mRecyclerView.setVisibility(View.GONE);
                    placeholderTextView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<GetAllVehiclesResponse> call, Throwable throwable) {
                Log.e("Vehicle Addition","Failed.");
                mRecyclerView.setVisibility(View.GONE);
                placeholderTextView.setVisibility(View.VISIBLE);
            }
        });
    }
}
