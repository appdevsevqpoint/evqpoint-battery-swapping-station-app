package com.evqpoint.evqpointbatteryswappingstationapp.activities.user;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.AccountDetailsActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.LoginActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.UpdateDetailsActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.help.HelpMenuActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.mapactivity.MultiStationsMapActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.transactions.NewTransactionsActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.FetchLast5TransactionsAsync;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.TransactionEntity;
import com.evqpoint.evqpointbatteryswappingstationapp.qrscanner.NewDecoderActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class UserActivity extends AppCompatActivity implements TransactionsAdapter.TransactionsAdapterCallback{//}, onStationAddListener {

    private RecyclerView mRecyclerView;
    private TransactionsAdapter mAdapter;
    private LinearLayout linearLayout;
    public Context global_context;

    private List<EVTransaction> transactionsList;
    private List<TransactionEntity> transactionsList2;

    private TextView tvName;
    private TextView tvEmail;
    private TextView tvPhone;

    private TextView searchText;
    private TextView homeText;
    private TextView accountText;
    private ImageView searchButton;
    private ImageView homeButton;
    private ImageView accountButton;

    private TextView placeholderTextView;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    SharedPreferences preferences1;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //UI initialization
        //setContentView(R.layout.activity_user);
        setContentView(R.layout.drawer_layout_user);
        linearLayout = findViewById(R.id.linRoot);
        global_context = getApplicationContext();

        tvName = findViewById(R.id.editName);
        tvEmail = findViewById(R.id.tvEmail);
        tvPhone = findViewById(R.id.tvPhone);

        TextView tvPlaceHolder = findViewById(R.id.placeholder_textview);
        tvPlaceHolder.setOnClickListener(
                v -> {
                    //TODO
                    //Intent transactionsIntent = new Intent(UserActivity.this, TransactionsActivity.class);
                    Intent transactionsIntent = new Intent(UserActivity.this, NewTransactionsActivity.class);
                    startActivity(transactionsIntent);
                }
        );

        pref = this.getSharedPreferences("LoginPref", 0);

        tvEmail.setText(pref.getString("loginEmail", null));
        tvPhone.setText(pref.getString("loginPhone", null));
        tvName.setText(pref.getString("loginName", null));

        //toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        searchButton = findViewById(R.id.searchButton);
        searchText = findViewById(R.id.searchText);
        homeButton = findViewById(R.id.homeButton);
        homeText = findViewById(R.id.homeText);
        accountButton = findViewById(R.id.personButton);
        accountText = findViewById(R.id.personText);

        resetBar();

        homeButton.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                   homeButton.setImageResource(R.drawable.ic_power_24px_white);
                    homeText.setTextColor(Color.parseColor("#ffffff"));
                    //accountButton.setImageResource(R.drawable.ic_person_24px_disabled);
                    accountButton.setImageResource(R.drawable.ic_person_alt1_disabled);
                    accountText.setTextColor(Color.parseColor("#9e9e9e"));
                    return true;
                }
                case MotionEvent.ACTION_UP: {
                    //ivHome.setImageResource(R.drawable.ic_power_24px);
                    homeButton.setImageResource(R.drawable.ic_powerplug_24px_dark);
                    homeText.setTextColor(Color.parseColor("#FF007BB6"));
                    Intent intent = new Intent(UserActivity.this, NewDecoderActivity.class);
                    startActivity(intent);
                    return true;
                }
            }
            return false;
        });

        searchButton.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    searchButton.setImageResource(R.drawable.ic_station_1_white);
                    //searchButton.setImageResource(R.drawable.ic_search_24px_white);
                    searchText.setTextColor(Color.parseColor("#ffffff"));
                    //accountButton.setImageResource(R.drawable.ic_person_24px_disabled);
                    accountButton.setImageResource(R.drawable.ic_person_alt1_disabled);
                    accountText.setTextColor(Color.parseColor("#9e9e9e"));
                    return true;
                }
                case MotionEvent.ACTION_UP: {
                    //searchButton.setImageResource(R.drawable.ic_search_24px);
                    searchButton.setImageResource(R.drawable.ic_station_1_dark);
                    searchText.setTextColor(Color.parseColor("#FF007BB6"));
                    //Intent intent = new Intent(UserActivity.this, CachedMapActivity.class);
                    Intent intent = new Intent(UserActivity.this, MultiStationsMapActivity.class);
                    startActivity(intent);
                    return true;
                }
            }
            return false;
        });

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout_user);
        NavigationView mNavigationView = findViewById(R.id.navigation);

        preferences1 = getSharedPreferences("LoginPref", 0);
        View headerView = mNavigationView.getHeaderView(0);
        TextView tvUserNameBar = headerView.findViewById(R.id.tvUserNameBar);
        tvUserNameBar.setText(preferences1.getString("loginName","Mr.Nobody"));

        ImageView menuButton = findViewById(R.id.menuButton);
        menuButton.setOnClickListener(v -> {
            if(!drawerLayout.isDrawerOpen(findViewById(R.id.navigation)))
            {
                drawerLayout.openDrawer(GravityCompat.START);
            }else{
                drawerLayout.openDrawer(GravityCompat.END);
            }
        });

        //SideBar Menu
        mNavigationView.setNavigationItemSelectedListener(item -> {
            int id = item.getItemId();
            switch(id)
            {
                case R.id.logout:
                    Toast.makeText(this, "Logged Out!", Toast.LENGTH_SHORT).show();
                    Log.e("Status","Logging Out.");
                    SharedPreferences preferences = getSharedPreferences("LoginPref", 0);

                    if(preferences.getString("transID", "0").equalsIgnoreCase("0"))
                    {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.apply();

                        Intent intent = new Intent(UserActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        AlertDialog alertDialog = new AlertDialog.Builder(UserActivity.this).create();
                        alertDialog.setTitle("Charging is On Going!");
                        alertDialog.setMessage("Stop the charging session before logging out.");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                (dialog, which) -> dialog.dismiss());
                        alertDialog.show();
                    }
                    break;
                case R.id.account:
                    //Intent transactionsIntent = new Intent(UserActivity.this, TransactionsActivity.class);
                    Intent transactionsIntent = new Intent(UserActivity.this, AccountDetailsActivity.class);
                    startActivity(transactionsIntent);
                    break;
                case R.id.help:
                    Intent intentHelp = new Intent(UserActivity.this, HelpMenuActivity.class);
                    startActivity(intentHelp);
                    break;
                case R.id.recent:
                    Intent transIntent = new Intent(UserActivity.this, NewTransactionsActivity.class);
                    startActivity(transIntent);
                    break;
                default:
                    return true;
            }

            return true;
        });
        
        ImageView ivReservation = findViewById(R.id.imgReservation);
        ivReservation.setOnClickListener(v -> {
            Toast t = Toast.makeText(getApplicationContext(),"Coming Soon!", Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
        });

        ImageView ivCO2 = findViewById(R.id.imgCO2);
        ivCO2.setOnClickListener(v -> {
            Toast t = Toast.makeText(getApplicationContext(),"Coming Soon!", Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
        });

        ImageView ivHelp = findViewById(R.id.imgHelp);
        ivHelp.setOnClickListener(v -> {
            Toast t = Toast.makeText(getApplicationContext(),"11-03-2020-BSS-DEMO-SINGLEUSER-MASTER", Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
        });

        //TODO Update flow to User Details Activity
        ImageView ivAccount = findViewById(R.id.imgAccount);
        ivAccount.setOnClickListener(v -> {
            Intent updateDeetsIntent = new Intent(UserActivity.this, UpdateDetailsActivity.class);
            startActivity(updateDeetsIntent);
        });

        ImageView ivDashboard = findViewById(R.id.imgDashboard);
        ivDashboard.setOnClickListener(v -> {
            Toast t = Toast.makeText(getApplicationContext(),"Coming Soon!", Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
        });

        ImageView ivRewards = findViewById(R.id.imgRewards);
        ivRewards.setOnClickListener(v -> {
            Toast t = Toast.makeText(getApplicationContext(),"Coming Soon!", Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
        });

        //RECYCLER
        mRecyclerView = findViewById(R.id.transactions_recycler_view);
        placeholderTextView = findViewById(R.id.placeholder_textview);
        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        //Init dataset
        transactionsList = new ArrayList<>();
        initTransactions();

        if(transactionsList.size()>0) {
            Log.v("Vals", transactionsList.get(0).toString());
        }
        //RECYCLER ADAPTER
        mAdapter = new TransactionsAdapter(transactionsList, UserActivity.this);
        mRecyclerView.setAdapter(mAdapter);

        //Edit User Profile
        ImageView imEdit = findViewById(R.id.editButton);
        imEdit.setOnClickListener(v -> {
            Intent updateDeetsIntent = new Intent(UserActivity.this, UpdateDetailsActivity.class);
            startActivity(updateDeetsIntent);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        resetBar();
    }

    @Override
    public void onTransactionSelected(final int position)
    {
        //final EVTransaction selectedTransaction = transactionsList.get(position);
    }

    public void resetAdapter(){
        //mAdapter = new TransactionsAdapterNew(transactionsList2, UserActivity.this);
        mAdapter = new TransactionsAdapter(transactionsList, UserActivity.this);
        mRecyclerView.setAdapter(mAdapter);
        //mAdapter.notifyItemRangeChanged(0,transactionsList.size());
    }

    public void initTransactions() {
        FetchLast5TransactionsAsync fetchDB = new FetchLast5TransactionsAsync(getApplicationContext());
        try {
            transactionsList2 = fetchDB.execute().get();

            if(transactionsList2.isEmpty()){
                Log.e("DB response","PANIC! ABANDON SHIP!!");
                mRecyclerView.setVisibility(View.GONE);
                placeholderTextView.setVisibility(View.VISIBLE);
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for(int k=0;k<transactionsList2.size();k++){
            String summary = "Battery charged for " + transactionsList2.get(k).getEnergyConsumed() +"Wh.";
            Log.v("Summary:",summary);
            Log.v("TEnergy DB:",transactionsList2.get(k).getEnergyConsumed().toString());

            String more;
            if(transactionsList2.get(k).getStartTime().matches("\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}")) {
                more = epochToHumanDate(Long.parseLong(transactionsList2.get(k).getStartTime())) +
                        " \t\tDuration: " + transactionsList2.get(k).getDuration();
            }else {
                more = transactionsList2.get(k).getStartTime() +
                        " \t\tDuration: " + transactionsList2.get(k).getDuration();
            }

            EVTransaction evTransact = new EVTransaction(summary,more,"");
            transactionsList.add(evTransact);
        }

        /*
        transactionsList.add(new EVTransaction("Rs 200 charged at XYZ Street.","Energy Consumed : 20 kWh","http://cogentlegal.com/wp-content/uploads/2013/01/Screen-Shot-2013-01-03-at-12.50.09-PM1.png"));
        transactionsList.add(new EVTransaction("Rs 10 charged at DA Street.","Energy Consumed : 10 kWh","http://cogentlegal.com/wp-content/uploads/2013/01/Screen-Shot-2013-01-03-at-12.50.09-PM1.png"));
        transactionsList.add(new EVTransaction("Rs 123 charged at My Street.","Energy Consumed : 17 kWh","http://cogentlegal.com/wp-content/uploads/2013/01/Screen-Shot-2013-01-03-at-12.50.09-PM1.png"));
        transactionsList.add(new EVTransaction("Rs 5 charged at Quick Street.","Energy Consumed : 5 kWh","http://cogentlegal.com/wp-content/uploads/2013/01/Screen-Shot-2013-01-03-at-12.50.09-PM1.png"));
        transactionsList.add(new EVTransaction("Rs 2.5 charged at Very Quick Street.","Energy Consumed : 2 kWh","http://cogentlegal.com/wp-content/uploads/2013/01/Screen-Shot-2013-01-03-at-12.50.09-PM1.png"));
        */
    }

    //Resets Bottom Nav Bar
    private void resetBar() {
        //searchButton.setImageResource(R.drawable.ic_search_24px_disabled);
        searchButton.setImageResource(R.drawable.ic_station_1_disabled);
        searchText.setTextColor(Color.parseColor("#9e9e9e"));
        //homeButton.setImageResource(R.drawable.ic_power_24px_disabled);
        homeButton.setImageResource(R.drawable.ic_powerplug_24px_disabled);
        homeText.setTextColor(Color.parseColor("#9e9e9e"));
        //accountButton.setImageResource(R.drawable.ic_person_24px);
        accountButton.setImageResource(R.drawable.ic_person_alt1);
        accountText.setTextColor(Color.parseColor("#004F86"));
    }


    private String epochToHumanDate(long epochSeconds){
        long itemLong = epochSeconds;
        java.util.Date d = new java.util.Date(itemLong*1000L);
        String itemDateStr = new SimpleDateFormat("dd-MMM-YY | HH:mm a").format(d);
        return itemDateStr;
    }
}
