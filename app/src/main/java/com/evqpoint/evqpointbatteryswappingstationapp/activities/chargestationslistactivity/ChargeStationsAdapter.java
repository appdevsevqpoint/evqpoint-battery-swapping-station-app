package com.evqpoint.evqpointbatteryswappingstationapp.activities.chargestationslistactivity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.ChargingStation;


import java.util.List;


public class ChargeStationsAdapter extends RecyclerView.Adapter<ChargeStationsAdapter.StationViewHolder>{

    private List<ChargingStation> chargeStationsList;
    private ChargeStationsAdapterCallback callback;

    public interface ChargeStationsAdapterCallback{
        void onStationSelected(int position);
    }

    public class StationViewHolder extends RecyclerView.ViewHolder {

        private TextView tvStation;
        private TextView tvStationAddress;
        private ImageView ivStationImage;
        private Context context;

        public StationViewHolder(@NonNull final View itemView) {
            super(itemView);

            context = itemView.getContext();

            tvStation = itemView.findViewById(R.id.tvStationName);
            tvStationAddress = itemView.findViewById(R.id.tvStationAddress);
            ivStationImage = itemView.findViewById(R.id.ivStationImage);

            tvStation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onStationSelected(getLayoutPosition());
                }
            });

            tvStationAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onStationSelected(getLayoutPosition());
                }
            });

            ivStationImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onStationSelected(getLayoutPosition());
                }
            });
        }
    }

    public ChargeStationsAdapter(List<ChargingStation> stationListIn, ChargeStationsAdapterCallback callback) {
        this.chargeStationsList = stationListIn;
        this.callback = callback;
    }

    @Override
    public StationViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.station_item_view, parent, false);

        return new StationViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final StationViewHolder stationViewHolder, int i) {
        final ChargingStation ChargingStation = chargeStationsList.get(i);

        stationViewHolder.tvStation.setText(ChargingStation.getName());
        stationViewHolder.tvStationAddress.setText(ChargingStation.getAddress());
        Glide.with(stationViewHolder.context)
                .load(ChargingStation.getImage())
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.ic_image_black_24dp)
                .into(stationViewHolder.ivStationImage);
    }

    @Override
    public int getItemCount() {
        return this.chargeStationsList.size();
    }

    public void removeStation(int position) {
        chargeStationsList.remove(position);
        //notifyItemRemoved(position);
    }

    public void restoreStation(ChargingStation ChargingStation, int position) {
        chargeStationsList.add(position, ChargingStation);
        notifyItemInserted(position);
    }

    public void updateStation(ChargingStation ChargingStation, int index) {
        chargeStationsList.get(index).setName(ChargingStation.getName());
        chargeStationsList.get(index).setAddress(ChargingStation.getAddress());
        chargeStationsList.get(index).getLocation().get(0); //Latitude
        chargeStationsList.get(index).getLocation().get(1); //Longitude
        notifyItemChanged(index);
    }

    public void onStationAdd(List<ChargingStation> inStationList) {
        this.chargeStationsList = inStationList;
        notifyItemInserted(chargeStationsList.size());
    }
}
