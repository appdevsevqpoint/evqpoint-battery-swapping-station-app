package com.evqpoint.evqpointbatteryswappingstationapp.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.network.otp.OtpApi;
import com.evqpoint.evqpointbatteryswappingstationapp.network.otp.OtpResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.user_details.UpdateUserDetailsApi;
import com.evqpoint.evqpointbatteryswappingstationapp.network.user_details.UpdateUserPhoneRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.user_details.UpdatedUserResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class NewPhoneActivity extends AppCompatActivity {

    String MOTP;

    //RETROFIT
    public static final String BASE_URL = "https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    //CachedOTP
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    //UI
    EditText editNewPhone;
    EditText editMOTP;
    Button btnSend;
    Button btnValidate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_update);

        pref = this.getSharedPreferences("LoginPref", 0);
        MOTP = pref.getString("motp", "");

        editNewPhone = findViewById(R.id.editNewPhone);
        editMOTP = findViewById(R.id.editMOTP);
        btnSend = findViewById(R.id.btnChangePass);
        btnValidate = findViewById(R.id.validateMOTP);
        btnValidate.setEnabled(false);

        btnSend.setOnClickListener(v -> {
            btnSend.setText("RESEND OTP");
            String newnumber = editNewPhone.getText().toString();
            if((newnumber!=null)&&(newnumber!="")){
                sendPhoneOTP(newnumber);
            }
        });

        btnValidate.setOnClickListener(v -> {
            validateMOTP();
        });
    }

    private void validateMOTP() {
        String inOTP = editMOTP.getText().toString().trim();
        Log.e("EnteredOTP", inOTP);

        MOTP = MOTP.trim();
        Log.e("MOTP", MOTP);

        if((MOTP!=null)&&(MOTP!="")){
            if(MOTP.equals(inOTP)){
                String newPhone = editNewPhone.getText().toString();
                String loginID = pref.getString("loginId","");
                String userEmail = pref.getString("loginEmail","");

                final String BASE_URL = "https://client.evqpoint.com/api/user/";

                if (retrofit == null) {
                    retrofit = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                }

                UpdateUserPhoneRequest updateUserPhoneRequest = new UpdateUserPhoneRequest(loginID,
                        userEmail,newPhone);
                UpdateUserDetailsApi UpdateService = retrofit.create(UpdateUserDetailsApi.class);

                Call<UpdatedUserResponse> call = UpdateService.updateUserPhone(updateUserPhoneRequest);

                Log.e("NewPhoneActivity: Call",call.toString());
                Log.e("NewPhoneActivity: Request String",updateUserPhoneRequest.toString());

                call.enqueue(new Callback<UpdatedUserResponse>() {
                    @Override
                    public void onResponse(Call<UpdatedUserResponse> call, Response<UpdatedUserResponse> response) {
                        String responseString = "";

                        if (response.isSuccessful()) {
                            responseString = response.body().toString();

                            if(responseString!=null){
                                Log.e("NewPhoneActivity: User Update response",responseString);
                                //TODO RELOGIN
                                Toast.makeText(NewPhoneActivity.this,
                                        "Phone Number Updated", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else{
                            Log.e("NewPhoneActivity: User Update response","Update Failed.");
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdatedUserResponse> call, Throwable throwable) {
                        Log.e("LoginActivity: User response","Update Failed.");
                    }
                });
            }else{
                editMOTP.setError("Incorrect OTP. Reenter OTP.");
            }
        }else{
            Toast.makeText(this, "Please enter all fields.", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendPhoneOTP(String newphone) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                            .baseUrl(BASE_URL)
                            .build();

        OtpApi otpApi = retrofit.create(OtpApi.class);
        otpApi.sendEmailOTP("https://api.evqpoint.com/user/phoneOtp/"+newphone).enqueue(new Callback<OtpResponse>() {
            @Override
            public void onResponse(Call<OtpResponse> call, Response<OtpResponse> response) {

                if((response.isSuccessful())&&(response!=null)&&(response.body().getOTP()!=null)){
                    OtpResponse otpPhoneResp = response.body();
                    MOTP = otpPhoneResp.getOTP();
                    Log.e("NewPhoneActivity: MOTP", MOTP);
                    btnValidate.setEnabled(true);
                }else{
                    Toast.makeText(NewPhoneActivity.this, "Mobile OTP Error. Use another mobile number to generate OTP.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<OtpResponse> call, Throwable t) {
                Log.e("NewPhoneActivity: OTP Error", "OTP ERROR");
            }
        });
    }
}