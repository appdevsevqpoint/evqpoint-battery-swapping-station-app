package com.evqpoint.evqpointbatteryswappingstationapp.activities.transactions;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.mapactivity.MultiStationsMapActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.TransactionEntity;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.Transaction;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionPaymentStatus;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionResponse;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TransactionsAdapterNew extends RecyclerView.Adapter<TransactionsAdapterNew.TransactionViewHolder>{

    //RETROFIT
    public static final String BASE_URL = "https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    private List<TransactionEntity> transactionsList;
    //private List<Transaction> transactionsList;
    private boolean val;
    String username;
    String email;
    TransactionResponse tR;

    SharedPreferences pref;
    Context context;

    public class TransactionViewHolder extends RecyclerView.ViewHolder {

    public TextView tvStation;
    private TextView tvStartTime;
    private TextView tvAmount;
    private TextView tvEnergyConsumed;
    private TextView tvDuration;
    private Button btnInvoice;
    private Context context;

    public TransactionViewHolder(@NonNull final View itemView) {
        super(itemView);
        context = itemView.getContext();

        tvDuration = itemView.findViewById(R.id.tvDuration);
        tvStartTime= itemView.findViewById(R.id.tvStartTime);
        //tvTransID = itemView.findViewById(R.id.tvTransID);
        tvAmount = itemView.findViewById(R.id.tvTotalAmount);
        tvStation = itemView.findViewById(R.id.tvStation);
        tvEnergyConsumed = itemView.findViewById(R.id.tvEnergy);
        //tvEndTime = itemView.findViewById(R.id.tvEndTime);

        //tvSummary.setOnClickListener(v -> callback.onTransactionSelected(getLayoutPosition()));
    }
    }

    public TransactionsAdapterNew(List<TransactionEntity> transactionListIn, Context inContext) {
    //public TransactionsAdapterNew(List<Transaction> transactionListIn, Context inContext) {
        this.transactionsList = transactionListIn;
        context = inContext;
        pref = inContext.getSharedPreferences("LoginPref", 0);
        email = pref.getString("loginEmail", null);
        username = pref.getString("loginName", null);
    }

    @Override
    public TransactionViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transaction_circle_item, parent, false);
                //.inflate(R.layout.transaction_new_item, parent, false);

        return new TransactionViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final TransactionViewHolder transactionViewHolder, int i) {
        final TransactionEntity transactionEntity = transactionsList.get(i);
        //final Transaction transaction1 = transactionsList.get(i);
        Log.e("Val in Adapter", transactionEntity.toString());

        if(transactionEntity.getStartTime().matches("\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}")) {
            transactionViewHolder.tvStartTime.setText(epochToHumanDate(Integer.parseInt(transactionEntity.getStartTime())));
        }else {
            transactionViewHolder.tvStartTime.setText(transactionEntity.getStartTime());
        }

        transactionViewHolder.tvDuration.setText("Duration: " + transactionEntity.getDuration());
        //transactionViewHolder.tvDuration.setVisibility(View.GONE);

        double denergy = transactionEntity.getEnergyConsumed();
        if(denergy>1000){
            denergy = (double)(denergy/1000);
            transactionViewHolder.tvEnergyConsumed.setText("Energy Billed: " +
                    denergy +" kWh");
        }
        else{
            transactionViewHolder.tvEnergyConsumed.setText("Energy Billed: " +
                    denergy +" Wh");
        }

        //String energyDumped = String.format("%.2f", transactionEntity.getEnergyConsumed()) + " wh";
        String energyDumped;
        denergy = transactionEntity.getEnergyConsumed();
        if(denergy>1000){
            denergy = (double)(denergy/1000);
            energyDumped = String.format("%.2f", denergy) + " kWh";
        }
        else{
            energyDumped = String.format("%.2f", denergy) + " Wh";
        }

        if(energyDumped.length()>6){
            transactionViewHolder.tvAmount.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        }else if(energyDumped.length()>4){
            transactionViewHolder.tvAmount.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        }else{
            transactionViewHolder.tvAmount.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        }
        transactionViewHolder.tvAmount.setText(energyDumped);
    }

    private int epochToMonthNumber(long epochSeconds){
        long itemLong = epochSeconds;
        java.util.Date d = new java.util.Date(itemLong*1000L);
        String itemDateStr = new SimpleDateFormat("MM").format(d);
        return Integer.valueOf(itemDateStr);
    }

    private String epochToHumanDate(long epochSeconds){
        long itemLong = epochSeconds;
        java.util.Date d = new java.util.Date(itemLong*1000L);
        String itemDateStr = new SimpleDateFormat("dd-MMM-YY | HH:mm a").format(d);
        return itemDateStr;
    }

    @Override
    public int getItemCount() {
        return this.transactionsList.size();
    }
}