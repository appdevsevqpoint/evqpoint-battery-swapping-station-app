package com.evqpoint.evqpointbatteryswappingstationapp.activities.user;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.transactions.NewTransactionsActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.transactions.TransactionsActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.ChargingStation;
import com.evqpoint.evqpointbatteryswappingstationapp.qrscanner.NewDecoderActivity;

import java.util.List;


public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.TransactionViewHolder>{

    private List<EVTransaction> transactionsList;
    private TransactionsAdapterCallback callback;

    public interface TransactionsAdapterCallback{
        void onTransactionSelected(int position);
    }

    public class TransactionViewHolder extends RecyclerView.ViewHolder {

        private TextView tvSummary;
        private TextView tvMore;
        private ImageView ivMapImage;
        private Context context;

        public TransactionViewHolder(@NonNull final View itemView) {
            super(itemView);

            context = itemView.getContext();

            tvSummary = itemView.findViewById(R.id.tvSummary);
            tvMore = itemView.findViewById(R.id.tvMore);
            ivMapImage = itemView.findViewById(R.id.ivMapSnap);

            tvSummary.setOnClickListener(v -> {
                        //Intent intent = new Intent(v.getContext(), TransactionsActivity.class);
                        Intent intent = new Intent(v.getContext(), NewTransactionsActivity.class);
                        v.getContext().startActivity(intent);
            }
            );

            tvMore.setOnClickListener(v -> {
                //Intent intent = new Intent(v.getContext(), TransactionsActivity.class);
                Intent intent = new Intent(v.getContext(), NewTransactionsActivity.class);
                v.getContext().startActivity(intent);
            });

            ivMapImage.setOnClickListener(v -> {
                //Intent intent = new Intent(v.getContext(), TransactionsActivity.class);
                Intent intent = new Intent(v.getContext(), NewTransactionsActivity.class);
                v.getContext().startActivity(intent);
            });

            //ivMapImage.setOnClickListener(v -> callback.onTransactionSelected(getLayoutPosition()));
        }
    }

    public TransactionsAdapter(List<EVTransaction> evtransactionListIn, TransactionsAdapterCallback callback) {
        this.transactionsList = evtransactionListIn;
        this.callback = callback;
    }

    @Override
    public TransactionViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.transaction_item, parent, false);

        return new TransactionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final TransactionViewHolder transactionViewHolder, int i) {
        final EVTransaction evTransaction = transactionsList.get(i);

        //Log.v("Val in Adapter", evTransaction.toString());

        //TODO
        //String summary = evTransaction.getSummary().replace("","");

        transactionViewHolder.tvSummary.setText(evTransaction.getSummary());
        transactionViewHolder.tvMore.setText(evTransaction.getMore());
        Glide.with(transactionViewHolder.context)
                .load(evTransaction.getImage())
                .apply(RequestOptions.circleCropTransform())
                .placeholder(R.drawable.map_placeholder)
                .into(transactionViewHolder.ivMapImage);
    }

    @Override
    public int getItemCount() {
        return this.transactionsList.size();
    }
}
