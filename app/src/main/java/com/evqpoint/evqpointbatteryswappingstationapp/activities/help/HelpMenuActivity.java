package com.evqpoint.evqpointbatteryswappingstationapp.activities.help;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HelpMenuActivity extends AppCompatActivity {

    /*
    * 1. Contact Us - Activity with intents to email, phone and website
    * 2. About Us - Webview about us page
    * 3. Licenses - Google OSS, QRScanner - Use library tool
    * 4. Privacy Policy - PDF/HTML Linking to Webview
    * 5. Terms & Conditions - Webview or Checkable page?
    */

    private ListView mListView;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        context = getApplicationContext();

        //RecyclerView with Above Options
        mListView = findViewById(R.id.listview);
        String[] values = new String[] { "Contact Us", "About Us", "Licenses",
                "Privacy Policy", "Terms and Conditions" };

        final ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < values.length; ++i) {
            list.add(values[i]);
        }

        StableArrayAdapter adapter = new StableArrayAdapter(this,R.layout.help_menu_item_view,list);
        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id){
                if(position==0){
                    String url = "https://drive.google.com/open?id=1LYOba9NqBZNFAATnUen2zI8KmG5P8F5K";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }else if(position==1){
                    String url = "https://www.evqpoint.com/about-us/";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }else if(position==2){
                    String url = "https://drive.google.com/open?id=1LYOba9NqBZNFAATnUen2zI8KmG5P8F5K";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }else if(position==3){
                    String url = "https://drive.google.com/open?id=1LYOba9NqBZNFAATnUen2zI8KmG5P8F5K";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }else if(position==4){
                    String url = "https://drive.google.com/open?id=1LYOba9NqBZNFAATnUen2zI8KmG5P8F5K";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
            }
        });
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

}
