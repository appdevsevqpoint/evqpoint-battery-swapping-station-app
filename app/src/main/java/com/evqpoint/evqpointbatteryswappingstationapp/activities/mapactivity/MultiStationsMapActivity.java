package com.evqpoint.evqpointbatteryswappingstationapp.activities.mapactivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.LoginActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.control.UserControlActivityPre;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.help.HelpMenuActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.transactions.NewTransactionsActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.user.UserActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.db.stations.AddAllStationsAsync;
import com.evqpoint.evqpointbatteryswappingstationapp.db.stations.FetchStationsAsync;
import com.evqpoint.evqpointbatteryswappingstationapp.db.stations.StationEntity;
import com.evqpoint.evqpointbatteryswappingstationapp.network.OpenChargeMap.OpenChargeMapAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.OpenChargeMap.OpenChargeMapResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.ChargeStationAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.ChargingStation;
import com.evqpoint.evqpointbatteryswappingstationapp.network.getstations.ChargeStationsResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.getstations.StationLocation;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.DeviceIDRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.DeviceStatusResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.StationControlAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.qrscanner.NewDecoderActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MultiStationsMapActivity extends FragmentActivity implements GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback {

    //UI Nav Bar
    TextView searchText;
    ImageView searchButton;
    TextView qrText;
    ImageView qrButton;
    TextView accountText;
    ImageView accountButton;

    ProgressDialog progressDialog;

    double passedLat, passedLon;
    double currLat, currLon;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    boolean mLocationPermissionGranted = false;

    private boolean mPermissionDenied = false;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    private GoogleMap mMap;

    //Charge Stations Info
    List<ChargingStation> chargeStationsList;
    List<ChargingStation> filteredList;
    public static final String BASE_URL = "https://api.evqpoint.com/user/";
    public static final String BASE_URL_OPEN_CHARGE_MAP = "https://api.openchargemap.io/v3/";
    private static Retrofit retrofit = null;
    private static Retrofit retrofit2 = null;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.my_location_demo);
        setContentView(R.layout.drawer_layout_map);

        pref = this.getSharedPreferences("LoginPref", 0);

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout_map);
        NavigationView mNavigationView = findViewById(R.id.navigation);

        SharedPreferences preferences1 = getSharedPreferences("LoginPref", 0);
        View headerView = mNavigationView.getHeaderView(0);
        TextView tvUserNameBar = headerView.findViewById(R.id.tvUserNameBar);
        tvUserNameBar.setText(preferences1.getString("loginName", "Mr.Nobody"));

        ImageView menuButton = findViewById(R.id.menuButton);
        menuButton.setOnClickListener(v -> {
            if (!drawerLayout.isDrawerOpen(findViewById(R.id.navigation))) {
                drawerLayout.openDrawer(GravityCompat.START);
            } else {
                drawerLayout.openDrawer(GravityCompat.END);
            }
        });

        //SideBar Menu
        mNavigationView.setNavigationItemSelectedListener(item -> {
            int id = item.getItemId();
            switch (id) {
                case R.id.logout:
                    Toast.makeText(this, "Logged Out!", Toast.LENGTH_SHORT).show();
                    SharedPreferences preferences = getSharedPreferences("LoginPref", 0);

                    if (preferences.getString("transID", "0").equalsIgnoreCase("0")) {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        //editor.commit();
                        editor.apply();

                        Intent intent = new Intent(MultiStationsMapActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        String devid11 = preferences.getString("devid", "0");
                        if(!devid11.equalsIgnoreCase("0")){
                            Log.e("MapAct: Log Out Devid",devid11);
                            if(checkChildStatus(devid11)){
                                AlertDialog alertDialog = new AlertDialog.Builder(MultiStationsMapActivity.this).create();
                                alertDialog.setTitle("Charging is On Going!");
                                alertDialog.setMessage("Stop the charging session before logging out.");
                                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                        (dialog, which) -> dialog.dismiss());
                                alertDialog.show();
                            }else{
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.clear();
                                //editor.commit();
                                editor.apply();

                                Intent intent = new Intent(MultiStationsMapActivity.this, LoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }else{
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.clear();
                            //editor.commit();
                            editor.apply();

                            Intent intent = new Intent(MultiStationsMapActivity.this, LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                    break;
                case R.id.account:
                    Intent intent1 = new Intent(MultiStationsMapActivity.this, UserActivity.class);
                    startActivity(intent1);
                    break;
                case R.id.help:
                    Intent intentHelp = new Intent(MultiStationsMapActivity.this, HelpMenuActivity.class);
                    startActivity(intentHelp);
                    break;
                case R.id.recent:
                    //Intent transactionsIntent = new Intent(MultiStationsMapActivity.this, TransactionsActivity.class);
                    Intent transactionsIntent = new Intent(MultiStationsMapActivity.this, NewTransactionsActivity.class);
                    startActivity(transactionsIntent);
                    break;
                default:
                    return true;
            }

            return true;
        });

        //UI Action Bar
        AppBarLayout topBar = findViewById(R.id.topbar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageButton searchButtonX = findViewById(R.id.searchButtonX);
        EditText editTextSearchKey = findViewById(R.id.searchEditText);

        searchButtonX.setOnClickListener(v -> {
            String key = editTextSearchKey.getText().toString();
            showSearchedList(key);
        });

        //Bottom Nav Bar
        searchButton = findViewById(R.id.searchButton);
        searchButton.setOnClickListener(v -> {
            Log.e("Map Status:","Search Button Tapped");
            if ((topBar.getVisibility() == View.INVISIBLE)||
                    (topBar.getVisibility() == View.GONE)) {
                Log.e("Map Status:","Search Bar Visible");
                topBar.setVisibility(View.VISIBLE);
                editTextSearchKey.setVisibility(View.VISIBLE);
                editTextSearchKey.setFocusable(true);
                editTextSearchKey.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editTextSearchKey, InputMethodManager.SHOW_IMPLICIT);
            } else {
                topBar.setVisibility(View.GONE);
                Log.e("Map Status:","Search Bar Hidden");
            }
        });
        searchText = findViewById(R.id.searchText);

        qrButton = findViewById(R.id.homeButton);
        qrText = findViewById(R.id.homeText);
        accountButton = findViewById(R.id.personButton);
        accountText = findViewById(R.id.personText);

        resetBar();

        qrButton.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    qrButton.setImageResource(R.drawable.ic_power_24px_white);
                    qrText.setTextColor(Color.parseColor("#ffffff"));
                    //qrText.setBackgroundColor(Color.parseColor("#ffffff"));
                    //searchButton.setImageResource(R.drawable.ic_search_24px_disabled);
                    searchButton.setImageResource(R.drawable.ic_station_1_disabled);
                    searchText.setTextColor(Color.parseColor("#9e9e9e"));
                    return true;
                }
                case MotionEvent.ACTION_UP: {
                    //qrButton.setImageResource(R.drawable.ic_power_24px);
                    qrButton.setImageResource(R.drawable.ic_powerplug_24px_dark);
                    //qrButton.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    qrText.setTextColor(Color.parseColor("#FF007BB6"));
                    //qrText.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    Intent intent = new Intent(MultiStationsMapActivity.this, NewDecoderActivity.class);
                    startActivity(intent);
                    return true;
                }
            }
            return false;
        });

        accountButton.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    //accountButton.setImageResource(R.drawable.ic_person_24px_white);
                    accountButton.setImageResource(R.drawable.ic_person_alt1_white);
                    accountText.setTextColor(Color.parseColor("#ffffff"));
                    //searchButton.setImageResource(R.drawable.ic_search_24px_disabled);
                    searchButton.setImageResource(R.drawable.ic_station_1_disabled);
                    searchText.setTextColor(Color.parseColor("#9e9e9e"));
                    return true;
                }
                case MotionEvent.ACTION_UP: {
                    //accountButton.setImageResource(R.drawable.ic_person_24px);
                    accountButton.setImageResource(R.drawable.ic_person_alt1);
                    accountText.setTextColor(Color.parseColor("#FF007BB6"));
                    Intent intent = new Intent(MultiStationsMapActivity.this, UserActivity.class);
                    startActivity(intent);
                    return true;
                }
            }
            return false;
        });

        //Initialize stations list
        chargeStationsList = new ArrayList<>();
        filteredList = new ArrayList<>();

        Intent intent = getIntent();
        passedLon = intent.getDoubleExtra("stationLat", 77.665075); //13.004800, 77.665075
        passedLat = intent.getDoubleExtra("stationLon", 13.004800);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private boolean checkChildStatus(String devidin) {

        final String[] initDeviceStatus = new String[1];
        initDeviceStatus[0] = "offline";
        
        if(devidin.length()<17){
            String childid = pref.getString("oldChildID","00");

            if(childid.length()<2){
                devidin = devidin+"-0"+childid;
            }else{
                devidin = devidin+"-"+childid;
            }
        }

        DeviceIDRequest devIDRequest = new DeviceIDRequest(devidin);
        StationControlAPI chargeStationService = retrofit.create(StationControlAPI.class);
        Call<DeviceStatusResponse> call = chargeStationService.getDeviceStatus(devIDRequest);

        //TODO
        Log.e("Device Status Request", devIDRequest.toString());

        String finalDevidin = devidin;
        call.enqueue(new Callback<DeviceStatusResponse>() {
            @Override
            public void onResponse(Call<DeviceStatusResponse> call, Response<DeviceStatusResponse> response) {

                if(response.code()==200){
                    if(response.body().getDeviceStatus()!=null)
                    {
                        Log.e("Control:: Status",response.body().getDeviceStatus());
                        if(response.body().getDeviceStatus().equalsIgnoreCase("Device is offline"))
                        {
                            initDeviceStatus[0] = "offline";
                        }
                    }
                    else{
                        DeviceStatusResponse deviceResponse = response.body();
                        Log.e("Device Status Response",deviceResponse.toString());
                        String childid = finalDevidin.substring(finalDevidin.length()-2);
                        Log.e("ChildID",childid);
                        editor.putString("oldChildID",childid);
                        editor.apply();

                        Log.e("ParentStatus", deviceResponse.getParentDeviceStatus());
                        if(childid.equalsIgnoreCase("01")){
                            initDeviceStatus[0] = deviceResponse.getChild1DeviceStatus();
                            Log.e("Child1Status",deviceResponse.getChild1DeviceStatus());
                        }else if(childid.equalsIgnoreCase("02")){
                            initDeviceStatus[0] = deviceResponse.getChild2DeviceStatus();
                            Log.e("Child2Status",deviceResponse.getChild2DeviceStatus());
                        }else if(childid.equalsIgnoreCase("03")){
                            initDeviceStatus[0] = deviceResponse.getChild3DeviceStatus();
                            Log.e("Child3Status",deviceResponse.getChild3DeviceStatus());
                        }
                    }
                }
                else
                    initDeviceStatus[0] = "offline";

            }

            @Override
            public void onFailure(Call<DeviceStatusResponse> call, Throwable throwable) {
                Log.e("initDeviceStatus","Offline");
                initDeviceStatus[0] = "offline";
            }
        });

        if(initDeviceStatus[0].equalsIgnoreCase("CHARGE")){
            return true;
        }
        else{
            return false;
        }
    }

    //Resets Bottom Nav Bar
    private void resetBar() {
        //searchButton.setImageResource(R.drawable.ic_search_24px);
        searchButton.setImageResource(R.drawable.ic_station_1_dark);
        searchText.setTextColor(Color.parseColor("#004F86"));
        //qrButton.setImageResource(R.drawable.ic_power_24px_disabled);
        qrButton.setImageResource(R.drawable.ic_powerplug_24px_disabled);
        qrText.setTextColor(Color.parseColor("#9e9e9e"));
        //accountButton.setImageResource(R.drawable.ic_person_24px_disabled);
        accountButton.setImageResource(R.drawable.ic_person_alt1_disabled);
        accountText.setTextColor(Color.parseColor("#9e9e9e"));
    }

    private void showSearchedList(String key) {
        for (int u = 0; u < chargeStationsList.size(); u++) {
            if ((chargeStationsList.get(u).getName().toLowerCase().contains(key.toLowerCase())) ||
                    ((chargeStationsList.get(u).getAddress().toLowerCase().contains(key.toLowerCase())))) {
                filteredList.add(chargeStationsList.get(u));
            }
        }
        if (filteredList.size() != 0) {
            filterStationsMarkers();
        } else {
            Toast.makeText(this, "No Stations Found.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        try {
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));
            if (!success) {
                Log.e("Status MapActivity", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("Status MapActivity", "Can't find style. Error: ", e);
        }

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        //TODO
        //enableMyLocation();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

                LocationManager lm = (LocationManager)
                        getSystemService(Context. LOCATION_SERVICE ) ;
                boolean gps_enabled = false;
                boolean network_enabled = false;
                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager. GPS_PROVIDER ) ;
                } catch (Exception e) {
                    e.printStackTrace() ;
                }
                try {
                    network_enabled = lm.isProviderEnabled(LocationManager. NETWORK_PROVIDER ) ;
                } catch (Exception e) {
                    e.printStackTrace() ;
                }
                if (!gps_enabled && !network_enabled) {
                    new AlertDialog.Builder(MultiStationsMapActivity.this )
                            .setMessage( "Switch on GPS to locate yourself on map.")
                            .setTitle("Enable GPS")
                            .setPositiveButton( "Settings" , (paramDialogInterface, paramInt) -> startActivity( new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS )))
                            .setNegativeButton( "Cancel" , null )
                            .show() ;
                }

            mMap.setMyLocationEnabled(true);
        }

        mMap.setTrafficEnabled(false);

        mMap.setOnInfoWindowClickListener(arg0 -> {
            final String ssid = arg0.getSnippet();

            showCustomDialog(ssid);
        });

        //Set Map UI Settings
        UiSettings mapUISettings = mMap.getUiSettings();
        mapUISettings.setZoomControlsEnabled(true);
        mapUISettings.setMapToolbarEnabled(true);
        //mapUISettings.setMyLocationButtonEnabled(true);
        //fetchStationsMarkers();
        setStationMarkers();
    }

    private void showCustomDialog(String snippet) {

        String source_snippet = snippet.replaceAll("\n", "");

        snippet = snippet.replaceAll("\n", "+");
        final String snippet1 = snippet.replaceAll(" ", "%20");

        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(MultiStationsMapActivity.this);

        final View sheetView = MultiStationsMapActivity.this.getLayoutInflater().inflate(R.layout.bottom_sheet_map_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        final ImageView mapIcon = sheetView.findViewById(R.id.mapIcon);
        mapIcon.setOnClickListener(v -> {
            if (mBottomSheetDialog.isShowing()) {
                mBottomSheetDialog.dismiss();
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + Uri.encode(snippet1));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        final ImageView scanIcon = sheetView.findViewById(R.id.scanIcon);
        scanIcon.setOnClickListener(v -> {
            if (mBottomSheetDialog.isShowing()) {
                mBottomSheetDialog.dismiss();
                //editor.putString("stationInfo", source_snippet);
                //editor.putString("stationLocation", "something");
                //editor.commit();
                Intent intent = new Intent(this, NewDecoderActivity.class);
                startActivity(intent);
            }
        });

        final ImageView navigateIcon = sheetView.findViewById(R.id.navigateIcon);
        navigateIcon.setOnClickListener(v -> {
            if (mBottomSheetDialog.isShowing()) {
                mBottomSheetDialog.dismiss();
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + snippet1);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        final TextView navigateText = sheetView.findViewById(R.id.navigateText);
        navigateText.setOnClickListener(v -> {
            if (mBottomSheetDialog.isShowing()) {
                mBottomSheetDialog.dismiss();
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + snippet1);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        final TextView scanText = sheetView.findViewById(R.id.scanText);
        scanText.setOnClickListener(v -> {
            if (mBottomSheetDialog.isShowing()) {
                mBottomSheetDialog.dismiss();
                //editor.putString("stationInfo", source_snippet);
                //editor.putString("stationLocation", "something");
                //editor.commit();

                Intent intent = new Intent(this, NewDecoderActivity.class);
                startActivity(intent);
            }
        });

        final TextView mapText = sheetView.findViewById(R.id.mapText);
        mapText.setOnClickListener(v -> {
            if (mBottomSheetDialog.isShowing()) {
                mBottomSheetDialog.dismiss();
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + Uri.encode(snippet1));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });

        mBottomSheetDialog.show();
    }

    public void fetchStationsRemote() {

        if (!progressDialog.isShowing()) {
            progressDialog.setMessage("Syncing latest stations...");
            progressDialog.show();
        }

        //OUR STATIONS LOOK UP
        ////Log.e("Status Map Activity", "Fetching Station from our Remote.");

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ChargeStationAPI EVQAdminService = retrofit.create(ChargeStationAPI.class);

        List<Double> lox = new ArrayList<>();
        lox.add(currLat);
        lox.add(currLon);

        StationLocation currLocation = new StationLocation(lox);

        Call<ChargeStationsResponse> call = EVQAdminService.getChargingStations("", currLocation);

        call.enqueue(new Callback<ChargeStationsResponse>() {
            @Override
            public void onResponse(Call<ChargeStationsResponse> call,
                                   Response<ChargeStationsResponse> response) {
                chargeStationsList = response.body().getChargingStations();
                List<StationEntity> inStationEntityList = new ArrayList<StationEntity>();

                for (int l = 0; l < chargeStationsList.size(); l++) {
                    StationEntity neoStationEntity = new StationEntity(chargeStationsList.get(l));
                    inStationEntityList.add(neoStationEntity);
                }

                AddAllStationsAsync addTasks = new AddAllStationsAsync(getApplicationContext(),
                        inStationEntityList);
                addTasks.execute();

                //renderMarkers();
            }

            @Override
            public void onFailure(Call<ChargeStationsResponse> call, Throwable throwable) {
                for (int y = 0; y < chargeStationsList.size(); y++) {
                    ////Log.e("Station List Errored", Double.toString(chargeStationsList.get(y).getLocation().get(0)));
                }
            }
        });

        //OPEN CHARGE MAP LOOK UP
        ////Log.e("Status", "Fetching Station from Open Charge Map.");

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);  // <-- this is the important line!


        retrofit2 = new Retrofit.Builder()
                .baseUrl("https://api.openchargemap.io/v3/")//.baseUrl(BASE_URL_OPEN_CHARGE_MAP)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        OpenChargeMapAPI openChargeMapAPI = retrofit2.create(OpenChargeMapAPI.class);

        Call<List<OpenChargeMapResponse>> call1 = openChargeMapAPI.getOpenStations("json",
                "IN", 50, "true", "false");

        call1.enqueue(new Callback<List<OpenChargeMapResponse>>() {
            @Override
            public void onResponse(Call<List<OpenChargeMapResponse>> call,
                                   Response<List<OpenChargeMapResponse>> response) {

                ////Log.e("OCMP Req URL",response.raw().request().url().toString());
                ////Log.e("OCMP Req",response.raw().request().toString());
                ////Log.e("OCMP Resp Status",Integer.toString(response.code()));

                List<OpenChargeMapResponse> openChargeMapResponseList = response.body();
                for (int ii = 0; ii < openChargeMapResponseList.size(); ii++) {

                    //chargeStationsList = response.body().getChargingStations();
                    List<StationEntity> inStationEntityList = new ArrayList<StationEntity>();

                    for (int l = 0; l < openChargeMapResponseList.size(); l++) {
                        chargeStationsList.add(openChargeMapResponseList.get(l).convertToChargingStationFormat());
                        StationEntity neoStationEntity = new StationEntity(openChargeMapResponseList.get(l).convertToChargingStationFormat());
                        inStationEntityList.add(neoStationEntity);
                    }

                    AddAllStationsAsync addTasks = new AddAllStationsAsync(getApplicationContext(), inStationEntityList);
                    addTasks.execute();

                    //TODO
                    editor = pref.edit();
                    if (pref.getString("maprun", "first") == "first") {
                        editor.putString("maprun", "second");
                        editor.apply();
                        Toast.makeText(MultiStationsMapActivity.this, "Reloading map with latest charging stations.", Toast.LENGTH_SHORT).show();

                        Intent intent1 = getIntent();
                        finish();
                        startActivity(intent1);
                    }

                    ////Log.e("OCMResp Add",openChargeMapResponseList.get(ii).getAddressInfo()
                    //        .getAddressLine1());
                }
            }

            @Override
            public void onFailure(Call<List<OpenChargeMapResponse>> call, Throwable throwable) {
                ////Log.e("OCMResp Resp","ERRORED");
            }
        });

        renderMarkers();
    }

    public void setStationMarkers() {

        progressDialog = ProgressDialog.show(this, "Loading stations", "Please wait", true, true);

        if (!fetchStationsFromLocal()) {
            fetchStationsRemote();
        }

        ////Log.e("Status", "Rendering stations as marker on map.");

        progressDialog.dismiss();

        if (passedLat != 13.004800) {
            LatLng latLng = new LatLng(passedLat, passedLon);

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        } else {
            LatLng latLng = new LatLng(passedLat, passedLon);

            ////Log.e("Camera Vals",passedLat+"+"+passedLon);

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        }

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, false);
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);

            return;
        }else{
            Location location = locationManager.getLastKnownLocation(provider);
            if (location != null) {
                //Log.e("Provider ", provider + " has been selected.");
                onLocationChanged(location);
                LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());

                currLat = location.getLatitude();
                currLon = location.getLongitude();

                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(12));
            } else {
                //Log.e("LAT","Lat not available");
                //Log.e("LON","Lon not available");
            }
        }
    }

    public void onLocationChanged(Location location) {
        int lat = (int) (location.getLatitude());
        int lng = (int) (location.getLongitude());
        //Log.e("LAT val",String.valueOf(lat));
        //Log.e("LON val",String.valueOf(lng));
    }

    private void renderMarkers() {
        ////Log.e("Status", "Rendering Markers on Map.");
        for (int z = 0; z < chargeStationsList.size(); z++) {
            ////Log.e("Station to Mark", chargeStationsList.get(z).toString());

            Double lat = chargeStationsList.get(z).getLocation().get(1);
            Double lng = chargeStationsList.get(z).getLocation().get(0);
            String placeName = chargeStationsList.get(z).getName();
            String address = chargeStationsList.get(z).getAddress();

            MarkerOptions markerOptions = new MarkerOptions();
            LatLng latLng = new LatLng(lat, lng);
            markerOptions.position(latLng);

            markerOptions.title(placeName);
            markerOptions.snippet(formatAddress(address));

            //markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

            if(chargeStationsList.get(z).getName().toUpperCase().contains("EVQ")){
                //Log.e("Render Status","EVQ Station "+chargeStationsList.get(z).getName());
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            }
            else{
                //Log.e("Render Status","Unknown Station "+chargeStationsList.get(z).getName());
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
            }

            Marker m = mMap.addMarker(markerOptions);

            // move map camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

            //Reshape info screen
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                @Override
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    LinearLayout info = new LinearLayout(MultiStationsMapActivity.this);
                    info.setOrientation(LinearLayout.VERTICAL);

                    TextView title = new TextView(MultiStationsMapActivity.this);
                    title.setTextColor(Color.BLACK);
                    title.setGravity(Gravity.CENTER);
                    title.setTypeface(null, Typeface.BOLD);
                    title.setText(marker.getTitle());

                    TextView snippet = new TextView(MultiStationsMapActivity.this);
                    snippet.setTextColor(Color.GRAY);
                    snippet.setText(marker.getSnippet());

                    info.addView(title);
                    info.addView(snippet);

                    return info;
                }
            });
        }
    }

    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }

    public boolean fetchStationsFromLocal() {
        boolean dbresponse = false;

        FetchStationsAsync fetchDB = new FetchStationsAsync(getApplicationContext());
        try {
            List<StationEntity> stationEntityList = fetchDB.execute().get();

            for (int k = 0; k < stationEntityList.size(); k++) {
                chargeStationsList.add(stationEntityList.get(k).convertToChargingStation());
                dbresponse = true;
            }

            if (chargeStationsList.isEmpty()) {
                ////Log.e("DB response", "No Local Stations Found.");
                dbresponse = false;
            }
            else{
                ////Log.e("DB response", "Stations Found.");
                renderMarkers();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return dbresponse;
    }

    private String formatAddress(String address) {
        String newaddress = address;
        ////Log.e("Before String",newaddress);

        newaddress = newaddress.replaceAll(",", ",\n");

        ////Log.e("After String",newaddress);
        return newaddress;
    }

    public void filterStationsMarkers() {

        ////Log.e("Status", "Filtering Station");
        mMap.clear();

        ////Log.e("Stations Limit", Integer.toString(filteredList.size()));
        for (int z = 0; z < filteredList.size(); z++) {

            ////Log.e("Filtered Stations", filteredList.get(z).getLocation().get(1).toString() + "," + filteredList.get(z).getLocation().get(0).toString());

            Double lat = filteredList.get(z).getLocation().get(1);
            Double lng = filteredList.get(z).getLocation().get(0);
            String placeName = filteredList.get(z).getName();
            String address = filteredList.get(z).getAddress();

            MarkerOptions markerOptions = new MarkerOptions();
            LatLng latLng = new LatLng(lat, lng);
            markerOptions.position(latLng);

            markerOptions.title(placeName);
            markerOptions.snippet(formatAddress(address));

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            Marker m = mMap.addMarker(markerOptions);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }

        //Reshape info screen
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                LinearLayout info = new LinearLayout(MultiStationsMapActivity.this);
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(MultiStationsMapActivity.this);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(MultiStationsMapActivity.this);
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                info.addView(snippet);

                return info;
            }
        });
        // move map camera
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.

            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);

        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
            mLocationPermissionGranted = true;
            //TODO
            //mPermissionDenied = false;
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "You are here.", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Your current location.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }

        resetBar();
    }

    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    private void checkTransactionID(String tid) {
        ////Log.e("Transaction ID", tid);
        TransactionRequest transactionRequest = new TransactionRequest(tid);

        TransactionAPI transactionService = retrofit.create(TransactionAPI.class);
        Call<TransactionResponse> call = transactionService.getTransactionResponse(transactionRequest);

        ////Log.e("Status", "Fetching Transaction Details");

        call.enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {

                TransactionResponse transactionResponse = response.body();

                ////Log.e("Status", "Got response from Transaction API.");
                if (response.code() == 400) {
                    ////Log.e("Status", "Session is on going!!");
                    Intent homeint = new Intent(MultiStationsMapActivity.this, UserControlActivityPre.class);
                    finish();
                    startActivity(homeint);
                } else if (transactionResponse != null) {
                    ////Log.e("Status", "Session Stopped.");
                    ////Log.e("Transaction Status", transactionResponse.getTransactionID());
                    ////Log.e("Transaction Response", transactionResponse.toString());
                }
            }

            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable throwable) {
                ////Log.e("Status", "Retrofit session status HTTP Calling Failed");
            }
        });
    }
}
