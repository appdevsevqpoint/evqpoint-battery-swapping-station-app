package com.evqpoint.evqpointbatteryswappingstationapp.activities.transactions;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.mapactivity.MultiStationsMapActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.AddTransactionAsync;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.FetchLastMonthTransactionsSumAsync;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.FetchLastWeekTransactionsSumAsync;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.FetchTransactionsAsync;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.TransactionEntity;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.Transaction;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionsRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionsResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.qrscanner.NewDecoderActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@SuppressLint("ClickableViewAccessibility")
public class NewTransactionsActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private TransactionsAdapterNew mAdapter;
    private List<TransactionEntity> transactionsList;
    private TextView placeholderTextView;

    //Navigation Bar
    private TextView searchText;
    private TextView homeText;
    private TextView accountText;
    private ImageView searchButton;
    private ImageView homeButton;
    private ImageView accountButton;

    //
    SharedPreferences pref;
    public static final String BASE_URL = "https://client.evqpoint.com/api/user/";//"https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e("New Transactions Activity","Started New transactions Activity");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        placeholderTextView =  findViewById(R.id.placeholder_textview);
        mRecyclerView = findViewById(R.id.transactions_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, 0));

        //Init dataset
        pref = this.getSharedPreferences("LoginPref", 0);
        transactionsList = new ArrayList<>();
        setupDB();

        //if((transactionsList!=null)||(transactionsList.size()!=0))
        //Log.e("Dummy Vals",Integer.toString(transactionsList.size()));
        //Log.e("Dummy Vals",transactionsList.get(0).toString());

        //RECYCLER ADAPTER
        mAdapter = new TransactionsAdapterNew(transactionsList, getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);

        //Bottom Navigation Bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        searchButton = findViewById(R.id.searchButton);
        homeButton = findViewById(R.id.homeButton);
        accountButton = findViewById(R.id.personButton);

        searchButton = findViewById(R.id.searchButton);
        searchText = findViewById(R.id.searchText);
        homeButton = findViewById(R.id.homeButton);
        homeText = findViewById(R.id.homeText);
        accountButton = findViewById(R.id.personButton);
        accountText = findViewById(R.id.personText);

        resetBar();

        homeButton.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    homeButton.setImageResource(R.drawable.ic_power_24px_white);
                    homeText.setTextColor(Color.parseColor("#ffffff"));
                    //accountButton.setImageResource(R.drawable.ic_person_24px_disabled);
                    accountButton.setImageResource(R.drawable.ic_person_alt1_disabled);
                    accountText.setTextColor(Color.parseColor("#9e9e9e"));
                    return true;
                }
                case MotionEvent.ACTION_UP: {
                    //homeButton.setImageResource(R.drawable.ic_power_24px);
                    homeButton.setImageResource(R.drawable.ic_powerplug_24px_dark);
                    homeText.setTextColor(Color.parseColor("#FF007BB6"));
                    Intent intent = new Intent(NewTransactionsActivity.this, NewDecoderActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    return true;
                }
            }
            return false;
        });

        searchButton.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    //searchButton.setImageResource(R.drawable.ic_search_24px_white);
                    searchButton.setImageResource(R.drawable.ic_station_1_white);
                    searchText.setTextColor(Color.parseColor("#ffffff"));
                    //accountButton.setImageResource(R.drawable.ic_person_24px_disabled);
                    accountButton.setImageResource(R.drawable.ic_person_alt1_disabled);
                    accountText.setTextColor(Color.parseColor("#9e9e9e"));
                    return true;
                }
                case MotionEvent.ACTION_UP: {
                    //searchButton.setImageResource(R.drawable.ic_search_24px);
                    searchButton.setImageResource(R.drawable.ic_station_1_dark);
                    searchText.setTextColor(Color.parseColor("#FF007BB6"));
                    //Intent intent = new Intent(TransactionsActivity.this, CachedMapActivity.class);
                    Intent intent = new Intent(NewTransactionsActivity.this, MultiStationsMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    return true;
                }
            }
            return false;
        });

        TextView tvWeeklyEnergy = findViewById(R.id.tvWeeklyAmountVal);
        FetchLastWeekTransactionsSumAsync fetchWeeklySum = new FetchLastWeekTransactionsSumAsync(getApplicationContext());
        Double sum;
        try {
            sum = fetchWeeklySum.execute().get();

            if(sum!=null)
            {
                if(sum>1000){
                    sum = (double)(sum/1000);
                    tvWeeklyEnergy.setText(String.format("%.2f",sum) + " kWh" );
                }else{
                    tvWeeklyEnergy.setText(String.format("%.2f",sum) + " Wh" );
                }
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        TextView tvMonthlyEnergy = findViewById(R.id.tvMonthlyAmountVal);
        TextView tvQuarterlyEnergy = findViewById(R.id.tvQuarterlyAmountVal);
        FetchLastMonthTransactionsSumAsync fetchMonthlySum = new FetchLastMonthTransactionsSumAsync(getApplicationContext());
        Double sum1;
        try {
            sum1 = fetchMonthlySum.execute().get();

            if(sum1!=null) {
                if (sum1 > 1000) {
                    sum1 = (double) (sum1 / 1000);
                    tvMonthlyEnergy.setText(String.format("%.2f", sum1) + " kWh");
                } else {
                    tvMonthlyEnergy.setText(String.format("%.2f", sum1) + " Wh");
                }

                if (sum1 > 1000) {
                    sum1 = (double) (sum1 / 1000);
                    tvQuarterlyEnergy.setText(String.format("%.2f", sum1) + " kWh");
                } else {
                    tvQuarterlyEnergy.setText(String.format("%.2f", sum1) + " Wh");
                }
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Button btnSync = findViewById(R.id.fetchTransactions);
        //btnSync.setOnClickListener(v -> updateDB());
        btnSync.setOnClickListener(v -> {
            fetchTransactions();
            mAdapter.notifyDataSetChanged();
        });

        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        resetBar();
    }

    private void resetBar() {
        //searchButton.setImageResource(R.drawable.ic_search_24px_disabled);
        searchButton.setImageResource(R.drawable.ic_station_1_disabled);
        searchText.setTextColor(Color.parseColor("#9e9e9e"));
        //homeButton.setImageResource(R.drawable.ic_power_24px_disabled);
        homeButton.setImageResource(R.drawable.ic_powerplug_24px_disabled);
        homeText.setTextColor(Color.parseColor("#9e9e9e"));
        //accountButton.setImageResource(R.drawable.ic_person_24px);
        accountButton.setImageResource(R.drawable.ic_person_alt1);
        accountText.setTextColor(Color.parseColor("#004F86"));
    }

    private List<TransactionEntity> getMyDataset(){
        List<TransactionEntity> initTransactionsList = new ArrayList<>();
        //initTransactionsList.add(new TransactionEntity());
        return initTransactionsList;
    }

    private void setupDB() {
        //InitTransactionsAsync initDB = new InitTransactionsAsync(getApplicationContext(), getMyDataset());
        //initDB.execute();

        FetchTransactionsAsync fetchDB = new FetchTransactionsAsync(getApplicationContext());
        try {
            fetchTransactions();
            transactionsList = fetchDB.execute().get();

            if(transactionsList.isEmpty()){
                //Log.e("DB response","PANIC! ABANDON SHIP!!");
                mRecyclerView.setVisibility(View.GONE);
                placeholderTextView.setVisibility(View.VISIBLE);
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void fetchTransactions() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        TransactionAPI transactionService = retrofit.create(TransactionAPI.class);

        String userid = pref.getString("loginId", "");
        Log.e("NewTransactionsActivity: loginId", userid);
        TransactionsRequest transactionsRequest = new TransactionsRequest(userid);
        Call<TransactionsResponse> call = transactionService.getAllTransactions(transactionsRequest);

        call.enqueue(new Callback<TransactionsResponse>() {
            @Override
            public void onResponse(Call<TransactionsResponse> call,
                                   Response<TransactionsResponse> response) {
                if((response!=null)&&(response.code()==200)){
                    Log.e("NewTransactionsActivity: Transactions Number:", response.body().toString());

                    transactionsList = response.body().getTransactionsAsEntity();

                    for(int yy=0; yy<transactionsList.size(); yy++){
                        Log.e("NewTActivity: transactionsList "+yy,"::"+
                                transactionsList.get(yy).toString());

                    AddTransactionAsync addTask = new AddTransactionAsync(getApplicationContext(),transactionsList.get(yy));
                    addTask.execute();
                    }
                }
            }

            @Override
            public void onFailure(Call<TransactionsResponse> call, Throwable t) {
                Log.e("NewTActivity: Station List Errored", "Fetch Transactions Failed");
            }
        });
    }
}
