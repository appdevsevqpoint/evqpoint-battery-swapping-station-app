package com.evqpoint.evqpointbatteryswappingstationapp.activities.user;

public class EVTransaction {

    String summary;
    String more;
    String imagelink;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }

    public String getImage() {
        return imagelink;
    }

    public void setImagelink(String imagelink) {
        this.imagelink = imagelink;
    }

    public EVTransaction(String summary, String more, String imagelink) {
        this.summary = summary;
        this.more = more;
        this.imagelink = imagelink;
    }

    @Override
    public String toString() {
        return "EVTransaction{" +
                "summary='" + summary + '\'' +
                ", more='" + more + '\'' +
                ", imagelink='" + imagelink + '\'' +
                '}';
    }
}
