package com.evqpoint.evqpointbatteryswappingstationapp.activities.vehicles;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.network.vehicles.AddVehicleRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.vehicles.AddVehicleResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.vehicles.VehiclesAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class AddVehicleActivity extends AppCompatActivity {

    String vehicleNickName;
    String vehicleMake;
    String vehicleType;
    String vehicleModel;
    String vehiclePort;
    String vehicleMaxPower;
    String batteryType;
    String batteryCapacity;
    String batteryAmperage;
    String batteryWattage;
    String batteryVoltage;

    //RETROFIT
    public static final String BASE_URL = "https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    //User Details Cached
    SharedPreferences pref;

    String userID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicles);

        pref = this.getSharedPreferences("LoginPref", 0);

        userID = pref.getString("loginId", "DUMMY-UID");
        Log.e("UID in Address Details",userID);

        EditText editNickName = findViewById(R.id.editName);
        EditText editVehicleMake = findViewById(R.id.editVehicleMake);
        EditText editVehicleType = findViewById(R.id.editVehicleType);
        EditText editVehicleModel = findViewById(R.id.editVehicleModel);
        EditText editPortType = findViewById(R.id.editPortType);
        EditText editMaxPower = findViewById(R.id.editMaxPower);
        EditText editBatteryType = findViewById(R.id.editBatteryType);
        EditText editBatteryCapacity = findViewById(R.id.editBatteryCapacity);
        EditText editBatteryAmperage = findViewById(R.id.editBatteryAmp);
        EditText editBatteryWattage = findViewById(R.id.editBatteryWatt);
        EditText editBatteryVoltage = findViewById(R.id.editBatteryVoltage);

        Button saveVehicle = findViewById(R.id.btnSaveVehicle);
        saveVehicle.setOnClickListener(v->{
            vehicleNickName = editNickName.getText().toString();
            vehicleMake = editVehicleMake.getText().toString();
            vehicleType = editVehicleType.getText().toString();

            vehicleModel = editVehicleModel.getText().toString();
            vehiclePort = editPortType.getText().toString();
            vehicleMaxPower = editMaxPower.getText().toString();
            batteryType = editBatteryType.getText().toString();
            batteryCapacity = editBatteryCapacity.getText().toString();
            batteryAmperage = editBatteryAmperage.getText().toString();
            batteryWattage = editBatteryWattage.getText().toString();
            batteryVoltage = editBatteryVoltage.getText().toString();

            if(vehicleMaxPower.equals("")) vehicleMaxPower = "0";

            if(batteryAmperage.equals("")) batteryAmperage = "0";

            if(batteryWattage.equals("")) batteryWattage = "0";

            if(batteryVoltage.equals("")) batteryVoltage = "0";

            AddVehicleRequest addVehicleRequest = new AddVehicleRequest(
               userID, vehicleNickName, vehicleMake, vehicleType, vehicleModel,
               vehiclePort, Integer.parseInt(vehicleMaxPower), batteryType, batteryCapacity,
                    Integer.parseInt(batteryAmperage), Integer.parseInt(batteryWattage),
                    Integer.parseInt(batteryVoltage)
            );

            addVehicleToServer(addVehicleRequest);
        });
    }

    /**
     * Method responsible for HTTP Retrofit POST call to server to add new user vehicle.
     * @params addVehicleRequest An object of AddVehicleRequest class containing required vehicle details.
     * @see com.evqpoint.evqpointbatteryswappingstationapp.network.vehicles.AddVehicleRequest
    **/
    private void addVehicleToServer(AddVehicleRequest addVehicleRequest) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        VehiclesAPI addVehicleService = retrofit.create(VehiclesAPI.class);
        Call<AddVehicleResponse> call = addVehicleService.addVehicle(addVehicleRequest);

        call.enqueue(new Callback<AddVehicleResponse>() {
            @Override
            public void onResponse(Call<AddVehicleResponse> call, Response<AddVehicleResponse> response) {
                String responseString = "";

                if (response.isSuccessful()) {
                    responseString = response.body().toString();

                    Log.e("Vehicle Add Resp",responseString);
                    Log.e("Vehicle Addition","Successful");

                    Toast.makeText(AddVehicleActivity.this, "New vehicle added.", Toast.LENGTH_SHORT).show();

                    Intent backIntent = new Intent(AddVehicleActivity.this,VehicleListActivity.class);
                    finish();
                    startActivity(backIntent);
                    //startActivity(getIntent());
                }
                else{
                    Log.e("Vehicle Addition","Failed.");
                }

            }

            @Override
            public void onFailure(Call<AddVehicleResponse> call, Throwable throwable) {
                Log.e("Vehicle Addition","Failed.");
            }
        });
    }

}
