package com.evqpoint.evqpointbatteryswappingstationapp.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.network.user_details.UpdateUserAddressRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.user_details.UpdateUserDetailsApi;
import com.evqpoint.evqpointbatteryswappingstationapp.network.user_details.UpdatedUserResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class AddressDetailsActivity extends AppCompatActivity {

    String address;
    String city;
    String state;
    String pincode;
    String country;

    String userID;
    String emailID;
    String phone;

    //RETROFIT
    public static final String BASE_URL = "https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    //User Details Cached
    SharedPreferences pref;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_update);

        pref = this.getSharedPreferences("LoginPref", 0);

        userID = pref.getString("loginId", null);
        Log.e("UID in Address Details",userID);

        emailID = pref.getString("loginEmail", null);
        phone = pref.getString("loginPhone", null);

        address = pref.getString("loginAddress", "");
        city = pref.getString("loginCity", "");
        state = pref.getString("loginState", "");
        pincode = "";
        country = pref.getString("loginCountry", "");

        if(address.contains(", Pincode - ")){
            Log.e("Address:",address);
            pincode = address.substring(address.indexOf(", Pincode - ")+12);
            address = address.replace(", Pincode - " + pincode, "");
        }

        EditText editCityInfo = findViewById(R.id.tvCity);
        if(!city.equals("")){
            editCityInfo.setText(city.trim());
            address = address.replace(city, "");
        }

        EditText editHouseInfo = findViewById(R.id.tvAddress1);
        if(!address.equals("")){
            String k = address.substring(0,address.indexOf(","));
            editHouseInfo.setText(k.trim());
            address = address.replace(k,"");
        }

        EditText editStreetInfo = findViewById(R.id.tvAddress2);
        if(!address.equals("")) {
            editStreetInfo.setText(address.substring(address.indexOf(",") + 1, address.length()).trim());
        }

        EditText editStateInfo = findViewById(R.id.tvState);
        if(!state.equals("")){
            editStateInfo.setText(state.trim());
        }

        EditText editPinInfo = findViewById(R.id.tvPinCode);
        if(!pincode.equals("")){
            editPinInfo.setText(pincode.trim());
        }

        EditText editCountryInfo = findViewById(R.id.tvCountry);
        if(!country.equals("")){
            editCountryInfo.setText(country.trim());
        }

        Button saveAddressButton = findViewById(R.id.btnSaveAddress);
        if(!address.equals(""))
            saveAddressButton.setText(R.string.update);

        saveAddressButton.setOnClickListener(v-> {
            address = editHouseInfo.getText().toString();
            address = address + editStreetInfo.getText().toString();
            city = editCityInfo.getText().toString();
            state = editStateInfo.getText().toString();
            pincode = editPinInfo.getText().toString();
            country = editCountryInfo.getText().toString();
            saveAddressDetails();
        });
    }

    private void saveAddressDetails() {

        address = address + ","+ city +", Pincode - " + pincode;

        Log.e("Update Address", address);

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        UpdateUserDetailsApi updateService = retrofit.create(UpdateUserDetailsApi.class);

        Log.e("Address Update Request","Country:"+country+", State:"+state +", City:"+
                city +", Address:"+address + ",UID:"+userID +",emailID:"+emailID+",phone:"+phone);

        UpdateUserAddressRequest updateUserAddressRequest = new UpdateUserAddressRequest(country,
                state, city, address,
                userID,emailID,phone);
        Call<UpdatedUserResponse> call = updateService.updateUserAddress(updateUserAddressRequest);

        call.enqueue(new Callback<UpdatedUserResponse>() {
            @Override
            public void onResponse(Call<UpdatedUserResponse> call, Response<UpdatedUserResponse> response) {
                String responseString = "";

                if (response.isSuccessful()) {
                    responseString = response.body().toString();

                    Log.e("User Update Resp",responseString);
                    Log.e("User Update","Successful");

                    SharedPreferences.Editor editor;
                    editor = pref.edit();
                    editor.putString("loginCity", city);
                    editor.putString("loginCountry", country);
                    editor.putString("loginState", state);
                    editor.apply();

                    Toast.makeText(AddressDetailsActivity.this, "Address number updated.", Toast.LENGTH_SHORT).show();

                    finish(); startActivity(getIntent());
                }
                else{
                    Log.e("User Update response","Update Failed.");
                }

            }

            @Override
            public void onFailure(Call<UpdatedUserResponse> call, Throwable throwable) {
                Log.e("User response","Update Failed.");
            }
        });

    }
}
