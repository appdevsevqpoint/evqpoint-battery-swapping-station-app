package com.evqpoint.evqpointbatteryswappingstationapp.activities.transactions;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.mapactivity.MultiStationsMapActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.FetchLastMonthTransactionsSumAsync;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.FetchLastWeekTransactionsSumAsync;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.TransactionEntity;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionsRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionsResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.qrscanner.NewDecoderActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@SuppressLint("ClickableViewAccessibility")
public class TransactionsActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private TransactionsAdapterNew mAdapter;
    private List<TransactionEntity> transactionsList;
    //List<Transaction> transactionsList;
    private TextView placeholderTextView;

    //Navigation Bar
    private TextView searchText;
    private TextView homeText;
    private TextView accountText;
    private ImageView searchButton;
    private ImageView homeButton;
    private ImageView accountButton;

    //
    SharedPreferences pref;
    public static final String BASE_URL = "https://client.evqpoint.com/api/user/";//"https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        placeholderTextView =  findViewById(R.id.placeholder_textview);
        mRecyclerView = findViewById(R.id.transactions_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, 0));

        //Init dataset
        pref = this.getSharedPreferences("LoginPref", 0);
        transactionsList = new ArrayList<>();
        getMyDataset();

        //Bottom Navigation Bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        searchButton = findViewById(R.id.searchButton);
        homeButton = findViewById(R.id.homeButton);
        accountButton = findViewById(R.id.personButton);

        searchButton = findViewById(R.id.searchButton);
        searchText = findViewById(R.id.searchText);
        homeButton = findViewById(R.id.homeButton);
        homeText = findViewById(R.id.homeText);
        accountButton = findViewById(R.id.personButton);
        accountText = findViewById(R.id.personText);

        resetBar();

        homeButton.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    homeButton.setImageResource(R.drawable.ic_power_24px_white);
                    homeText.setTextColor(Color.parseColor("#ffffff"));
                    //accountButton.setImageResource(R.drawable.ic_person_24px_disabled);
                    accountButton.setImageResource(R.drawable.ic_person_alt1_disabled);
                    accountText.setTextColor(Color.parseColor("#9e9e9e"));
                    return true;
                }
                case MotionEvent.ACTION_UP: {
                    //homeButton.setImageResource(R.drawable.ic_power_24px);
                    homeButton.setImageResource(R.drawable.ic_powerplug_24px_dark);
                    homeText.setTextColor(Color.parseColor("#FF007BB6"));
                    Intent intent = new Intent(TransactionsActivity.this, NewDecoderActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                    return true;
                }
            }
            return false;
        });

        searchButton.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    //searchButton.setImageResource(R.drawable.ic_search_24px_white);
                    searchButton.setImageResource(R.drawable.ic_station_1_white);
                    searchText.setTextColor(Color.parseColor("#ffffff"));
                    //accountButton.setImageResource(R.drawable.ic_person_24px_disabled);
                    accountButton.setImageResource(R.drawable.ic_person_alt1_disabled);
                    accountText.setTextColor(Color.parseColor("#9e9e9e"));
                    return true;
                }
                case MotionEvent.ACTION_UP: {
                    //searchButton.setImageResource(R.drawable.ic_search_24px);
                    searchButton.setImageResource(R.drawable.ic_station_1_dark);
                    searchText.setTextColor(Color.parseColor("#FF007BB6"));
                    //Intent intent = new Intent(TransactionsActivity.this, CachedMapActivity.class);
                    Intent intent = new Intent(TransactionsActivity.this, MultiStationsMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    return true;
                }
            }
            return false;
        });

        TextView tvWeeklyAmount = findViewById(R.id.tvWeeklyAmountVal);
        FetchLastWeekTransactionsSumAsync fetchWeeklySum = new FetchLastWeekTransactionsSumAsync(getApplicationContext());
        Double sum;
        try {
            sum = fetchWeeklySum.execute().get();
            //Log.e("SUM",sum.toString());

            //Double.toString(sum)
            tvWeeklyAmount.setText(String.format("%.2f",sum)+" wh");//String.format("Value of a: %.2f", a)

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        TextView tvMonthlyAmount = findViewById(R.id.tvMonthlyAmountVal);
        TextView tvQuarterlyAmount = findViewById(R.id.tvQuarterlyAmountVal);
        FetchLastMonthTransactionsSumAsync fetchMonthlySum = new FetchLastMonthTransactionsSumAsync(getApplicationContext());
        Double sum1;
        try {
            sum1 = fetchMonthlySum.execute().get();
            //Log.e("SUM",sum1.toString());
            //Double.toString(sum1)
            tvMonthlyAmount.setText(String.format("%.2f",sum1)+" wh");
            tvQuarterlyAmount.setText(String.format("%.2f",sum1)+" wh");
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Button btnSync = findViewById(R.id.fetchTransactions);
        //TODO
        //btnSync.setOnClickListener(v -> updateDB());
        btnSync.setOnClickListener(v -> {
            Log.e("TransactionsActivity: Transactions",Integer.toString(transactionsList.size()));
            Log.e("TransactionsActivity: Status","Redraw Recycler");
            mAdapter = new TransactionsAdapterNew(transactionsList,getApplicationContext());
            mRecyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            Log.e("TransactionsActivity: Items",Integer.toString(mAdapter.getItemCount()));
        });

        //RECYCLER ADAPTER
        mAdapter = new TransactionsAdapterNew(transactionsList, getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        resetBar();
    }

    //Resets Bottom Nav Bar
    private void resetBar() {
        //searchButton.setImageResource(R.drawable.ic_search_24px_disabled);
        searchButton.setImageResource(R.drawable.ic_station_1_disabled);
        searchText.setTextColor(Color.parseColor("#9e9e9e"));
        //homeButton.setImageResource(R.drawable.ic_power_24px_disabled);
        homeButton.setImageResource(R.drawable.ic_powerplug_24px_disabled);
        homeText.setTextColor(Color.parseColor("#9e9e9e"));
        //accountButton.setImageResource(R.drawable.ic_person_24px);
        accountButton.setImageResource(R.drawable.ic_person_alt1);
        accountText.setTextColor(Color.parseColor("#004F86"));
    }

    private void getMyDataset(){
        //List<Transaction> initTransactionsList = new ArrayList<>();

        //TODO
        /*
        initTransactionsList.add(new TransactionEntity(
                "f2543dd2-31d9-46a4-8b60-7357b14367a8",
                "BNG-HYB-001-01",
                1,
                epochToHumanDate(1580705555),
                1580705555,
                epochToMonthNumber(1580705555),
                "e8e6548d-7e66-4389-a77e-8912a967a6ef",
                23.2,
                Integer.toString(1580725555-1580705555)+" Secs",
                20
        ));*/

        fetchTransactions();
        //return initTransactionsList;
    }

    private void fetchTransactions() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        TransactionAPI transactionService = retrofit.create(TransactionAPI.class);

        //String userid = "fe29e23a-08bf-4bb3-9f6a-ac6f647e3dc9";
        String userid = pref.getString("loginId", "");
        Log.e("TransactionsActivity: loginId", userid);

        TransactionsRequest transactionsRequest = new TransactionsRequest(userid);
        Call<TransactionsResponse> call = transactionService.getAllTransactions(transactionsRequest);

        call.enqueue(new Callback<TransactionsResponse>() {
            @Override
            public void onResponse(Call<TransactionsResponse> call,
                                   Response<TransactionsResponse> response) {

                if(response!=null){
                    transactionsList = response.body().getTransactionsAsEntity();

                    for(int yy=0; yy<transactionsList.size(); yy++){
                        Log.e("transactionsList "+yy,"::"+
                                transactionsList.get(yy).toString());
                    }
                }else{
                    Toast.makeText(TransactionsActivity.this,
                            "No transactions done yet.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TransactionsResponse> call, Throwable t) {
                Log.e("Station List Errored", "Fetch Transactions Failed");
            }
        });
    }

    private int epochToMonthNumber(long epochSeconds){
        long itemLong = epochSeconds;
        java.util.Date d = new java.util.Date(itemLong*1000L);
        String itemDateStr = new SimpleDateFormat("MM").format(d);
        return Integer.valueOf(itemDateStr);
    }

    private String epochToHumanDate(long epochSeconds){
        long itemLong = epochSeconds;
        java.util.Date d = new java.util.Date(itemLong*1000L);
        String itemDateStr = new SimpleDateFormat("dd-MMM-YY | HH:mm a").format(d);
        return itemDateStr;
    }

}
