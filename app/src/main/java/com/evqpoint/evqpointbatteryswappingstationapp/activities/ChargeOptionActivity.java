package com.evqpoint.evqpointbatteryswappingstationapp.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.control.UserControlActivityPre;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.mapactivity.MultiStationsMapActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.user.UserActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.qrscanner.NewDecoderActivity;

public class ChargeOptionActivity extends AppCompatActivity {

    LinearLayout linLayout;

    //UI
    LinearLayout btnPrePay;
    LinearLayout btnPostPay;
    LinearLayout btnTimedCharge;
    LinearLayout btnEnergyCharge;

    String devids;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_charge_option);

        //UI Action Bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView imhome = findViewById(R.id.homeButton);
        imhome.setOnClickListener(view -> {
            Intent homeint = new Intent(ChargeOptionActivity.this, NewDecoderActivity.class);
            startActivity(homeint);
        });

        ImageView searchButton1 = findViewById(R.id.searchButton);
        searchButton1.setImageResource(R.drawable.ic_station_1_disabled);
        searchButton1.setOnClickListener(v -> {
            Intent homeint = new Intent(ChargeOptionActivity.this, MultiStationsMapActivity.class);
            startActivity(homeint);
        });
        TextView tvSearchTitle = findViewById(R.id.searchText);
        tvSearchTitle.setTextColor(Color.parseColor("#9e9e9e"));

        ImageView accountButton = findViewById(R.id.personButton);
        //accountButton.setImageResource(R.drawable.ic_person_24px_disabled);
        accountButton.setImageResource(R.drawable.ic_person_alt1_disabled);
        accountButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, UserActivity.class);
            startActivity(intent);
        });
        TextView tvPersonTitle = findViewById(R.id.personText);
        tvPersonTitle.setTextColor(Color.parseColor("#9e9e9e"));

        if(getIntent()!=null) {
            devids = getIntent().getStringExtra("devid");
        }else{
            Log.e("Status","Errored devid not found.");
        }

        linLayout = findViewById(R.id.linearlayout1);

        btnPrePay = findViewById(R.id.prePayOption);
        btnPostPay = findViewById(R.id.postPayOption);
        btnTimedCharge = findViewById(R.id.timedOption);
        btnEnergyCharge = findViewById(R.id.energyOption);

        //Prepay
        btnPrePay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {/*
                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(ChargeOptionActivity.this);

                final View sheetView = ChargeOptionActivity.this.getLayoutInflater().inflate(R.layout.bottom_sheet_prepay, null);
                mBottomSheetDialog.setContentView(sheetView);

                final EditText inputPrePayAmount = sheetView.findViewById(R.id.EditTextAmount);
                final Button payButton = sheetView.findViewById(R.id.ButtonPay);
                payButton.setText("Pay");
                payButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String prePayAmount = inputPrePayAmount.getText().toString();

                        //Log Input Payment Details
                        if(mBottomSheetDialog.isShowing()){
                                mBottomSheetDialog.dismiss();

                                /*
                                String amount = prePayAmount;

                                Intent intentPaymentGateway = new Intent(getApplicationContext(), PrePaymentActivity.class);
                                intentPaymentGateway.putExtra("order-type","yes");
                                intentPaymentGateway.putExtra("order-prepay","0");
                                intentPaymentGateway.putExtra("amount", amount);
                                intentPaymentGateway.putExtra("order-amount",inputPrePayAmount.getText().toString());
                                intentPaymentGateway.putExtra("order-note","Payment for EV Charging of INR "+ inputPrePayAmount.getText().toString());
                                intentPaymentGateway.putExtra("order-name","Latif Ameer");
                                intentPaymentGateway.putExtra("order-phone","990011223");
                                intentPaymentGateway.putExtra("order-email","latif@evqpoint.com");

                                startActivity(intentPaymentGateway);


                            }else{
                                Toast.makeText(getApplicationContext(),"No amount entered.",Toast.LENGTH_LONG).show();
                            }
                        }
                });
                mBottomSheetDialog.show();*/
                Toast.makeText(getApplicationContext(),"Coming Soon",Toast.LENGTH_LONG).show();
            }
        });

        //Post Pay  <<<
        btnPostPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                            Intent intent = new Intent(ChargeOptionActivity.this, UserControlActivityPre.class);
                            //intent.putExtra("devid","BNG-001-07");
                            intent.putExtra("devid",devids);
                            intent.putExtra("order-prepay","1");
                            intent.putExtra("order-amount","0");
                            intent.putExtra("order-note","0");
                            intent.putExtra("order-name","0");
                            intent.putExtra("order-phone","0");
                            intent.putExtra("order-email","0");

                            startActivity(intent);
                            finish();
                    }
        });

        //Charge by Time
        btnTimedCharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {/*
                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(ChargeOptionActivity.this);

                final View sheetView = ChargeOptionActivity.this.getLayoutInflater().inflate(R.layout.bottom_sheet_prepay, null);
                mBottomSheetDialog.setContentView(sheetView);

                final EditText inputPrePayAmount = sheetView.findViewById(R.id.EditTextAmount);
                inputPrePayAmount.setHint("8 Hours");
                final Button payButton = sheetView.findViewById(R.id.ButtonPay);
                payButton.setText("Start");
                final TextView tvMessage = sheetView.findViewById(R.id.tvMessage);
                tvMessage.setText("How many hours do you want to charge for?");
                payButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String prePayAmount = inputPrePayAmount.getText().toString();

                        //Log Input Payment Details
                        Log.e("Charge Limit",prePayAmount);
                        if(prePayAmount!=""){
                        if(mBottomSheetDialog.isShowing()){
                            mBottomSheetDialog.dismiss();

                            String amount = prePayAmount;
                            Snackbar snackbar = Snackbar
                                    .make(linLayout, amount + " paid!", Snackbar.LENGTH_LONG);
                            snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
                            snackbar.show();

                            Toast.makeText(getApplicationContext(),"Coming Soon",Toast.LENGTH_LONG).show();

                            /*
                            Intent intent = new Intent(ChargeOptionActivity.this, UserControlActivityPre.class);
                            //intent.putExtra("devid","BNG-001-07");
                            intent.putExtra("devid",devids);
                            startActivity(intent);

                            }
                        }else{
                            Toast.makeText(getApplicationContext(),"No charge limit entered.",Toast.LENGTH_LONG).show();
                        }
                    }
                });

                mBottomSheetDialog.show();*/
                Toast.makeText(getApplicationContext(),"Coming Soon",Toast.LENGTH_LONG).show();
            }
        });

        //Charge by Energy
        btnEnergyCharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(ChargeOptionActivity.this);

                final View sheetView = ChargeOptionActivity.this.getLayoutInflater().inflate(R.layout.bottom_sheet_prepay, null);
                mBottomSheetDialog.setContentView(sheetView);

                final EditText inputPrePayAmount = sheetView.findViewById(R.id.EditTextAmount);
                inputPrePayAmount.setHint("1 kWh");
                final Button payButton = sheetView.findViewById(R.id.ButtonPay);
                payButton.setText("Start");
                final TextView tvMessage = sheetView.findViewById(R.id.tvMessage);
                tvMessage.setText("How much do you want to charge for?");
                payButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String prePayAmount = inputPrePayAmount.getText().toString();

                        //Log Input Payment Details
                        Log.e("Pre Pay Amount",prePayAmount);
                        if(prePayAmount!=""){
                        if(mBottomSheetDialog.isShowing()){
                            mBottomSheetDialog.dismiss();

                            String amount = prePayAmount;
                            Snackbar snackbar = Snackbar
                                    .make(linLayout, amount + " paid!", Snackbar.LENGTH_LONG);
                            snackbar.setActionTextColor(getResources().getColor(R.color.colorPrimary));
                            snackbar.show();

                            Toast.makeText(getApplicationContext(),"Coming Soon",Toast.LENGTH_LONG).show();

                            /*
                            Intent intent = new Intent(ChargeOptionActivity.this, UserControlActivityPre.class);
                            //intent.putExtra("devid","BNG-001-07");
                            intent.putExtra("devid",devids);
                            startActivity(intent);
                        }}else{
                        Toast.makeText(getApplicationContext(),"No amount entered.",Toast.LENGTH_LONG).show();
                    }
                    }
                });

                mBottomSheetDialog.show();*/
                Toast.makeText(getApplicationContext(),"Coming Soon",Toast.LENGTH_LONG).show();
            }
        });
    }
}
