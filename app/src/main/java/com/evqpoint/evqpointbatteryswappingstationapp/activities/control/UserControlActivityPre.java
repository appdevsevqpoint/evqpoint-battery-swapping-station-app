package com.evqpoint.evqpointbatteryswappingstationapp.activities.control;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.mapactivity.MultiStationsMapActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.user.UserActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.ChargingControl;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.ChargingSessionID;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.ChargingSessionResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.DeviceIDRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.DeviceStatusResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.StartResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.StationControlAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.StopResponse;
import com.github.anastr.speedviewlib.AwesomeSpeedometer;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserControlActivityPre extends AppCompatActivity {

    boolean updateBool;
    int stillCharging = 0;
    private int counter = 0;
    boolean repeatReq;
    boolean ourStop;
    String prevDeviceStatus;

    String unitAwe;

    //RETROFIT
    //"https://api.evqpoint.com/user/";
    public static final String BASE_URL = "https://client.evqpoint.com/api/user/";//"https://client.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    //UI
    TextView statsBatteryVoltage; //Consumer Display
    TextView statsPower; //Consumer Display
    TextView statsStartTime; //Consumer Display - Start Time
    TextView statsElapsedTime; //Consumer Display - Elapsed Time
    TextView statsDeviceStatus; //Consumer Display - Device Status
    TextView statsCCurrentStatus; //Consumer Display - Device Status
    TextView tvTimeTitle;

    public DrawerLayout drawerLayout;
    public NavController navController;
    public NavigationView navigationView;

    ProgressBar determinateTimeBar;

    //User Data
    private String devid;
    private boolean authStat;
    private long startTimeVal;
    private String startTime;
    private String userid;
    private String useremail;
    private String userphone;
    private String username;
    private String initDeviceStatus;
    private ChargingSessionID inSessionID;
    private String transID;

    //Cached information
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    ChargingSessionResponse chargingSessionResponse1;
    long elapsed_hours;
    long elapsed_minutes;
    long elapsed_seconds;

    //Alert Dialogs for Device Status
    AlertDialog.Builder alertUserVNC;
    AlertDialog.Builder alertUserIssue;

    //TODO Progress Dialog are deprecated. Replace with custom Dialogs?
    ProgressDialog progressDialogStarting;
    ProgressDialog progressDialogStopping;

    FloatingActionButton fabSwitch;

    AwesomeSpeedometer awesomeSpeedometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_user_control_prepay_stylish);
        //setContentView(R.layout.activity_user_control_prepay_const1);
        setContentView(R.layout.activity_user_control_prepay_dummy);


        //TODO
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        //TODO

        Log.e("Status","UCAPre");
        unitAwe = "wH";
        //UI Binding
        bindUI();
        updateBool = true;

        //RETROFIT
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        devid = getIntent().getStringExtra("devid");

        authStat = true;
        userid = "";

        pref = this.getSharedPreferences("LoginPref", 0);

        if(pref.getString("loginId", "") != null){
            userid = pref.getString("loginId", "");
            useremail = pref.getString("loginEmail", "");
            userphone = pref.getString("loginPhone", "");
            username = pref.getString("loginName", "");
        }

        //STEP 1 - Initial Station Status
        stillCharging = 0;
        initDeviceStatus = "EMPTY";
        ourStop = false;
        prevDeviceStatus = "";

        transID = "0";

        Log.e("GET DEVICE","On Create");
        Log.e("GET DEVICE",devid);
        getDeviceStatus(devid);

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss"); //String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
        startTime = sdf.format(new Date());

        editor = pref.edit();

        if(pref.getLong("loginStartSession",0)==0) {
            startTimeVal = System.currentTimeMillis() / 1000;
            editor.putLong("loginStartSession", startTimeVal);
            editor.apply();
        }else {
            startTimeVal = pref.getLong("loginStartSession",0);
        }
        statsStartTime.setText(startTime);
        //User Information Dialogs
        repeatReq = true;
        rapidUpdateUI();

        //STEP 2 - Start or Stop Station
        fabSwitch.setOnClickListener(view -> {
            /*
            * ["IDLE","BNC","CHARGE", "FAULT", "EM", "OVP", "BMSF"]
            *    BNC - Battery Not Connected
            *    CHARGE - Battery Connected
            *    EM - Emergency Button Stop
            *    OVP - Over voltage protection
            *    BMSF - BMS Fault
            *    FAULT - Fault
            *    Offline
            * */

            Log.e("GET DEVICE", "POWER BUTTON");
            Log.e("GET DEVICE",devid);
            getDeviceStatus(devid);

            if (stillCharging == 2)
            {
                stillCharging = 0;
                ViewCompat.setBackgroundTintList(fabSwitch,
                        ContextCompat.getColorStateList(UserControlActivityPre.this, R.color.colorAccent));
            }
            else if (stillCharging == 0)
            {
                stillCharging = 1;
                ViewCompat.setBackgroundTintList(fabSwitch,
                        ContextCompat.getColorStateList(UserControlActivityPre.this, android.R.color.holo_red_light));
            }

            if (initDeviceStatus.equalsIgnoreCase("IDLE")) //Starting Charge
            {
                    progressDialogStarting = ProgressDialog.show(UserControlActivityPre.this, "Starting Station", "Please wait..", false, false);

                    ChargingControl chargingControlRequest = new ChargingControl(devid, true, userid);

                    SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
                    startTime = sdf1.format(new Date());

                    startStation(chargingControlRequest);
                    Log.e("SAVE STATION", devid);
                    editor.putString("devid",devid);
                    editor.apply();
                    inSessionID = new ChargingSessionID(devid, transID);

                    rapidUpdateUI();
                    periodicallyUpdateUI();
                //}
            }
            else if ((initDeviceStatus.equalsIgnoreCase("CHARGE"))&&(stillCharging == 1))
            {
                ChargingControl chargingControlRequest = new ChargingControl(devid, false, userid);
                stopRepeatingTask(chargingControlRequest);

                rapidUpdateUI();
                periodicallyUpdateUI();
            }
            else if (initDeviceStatus.equalsIgnoreCase("BNC"))
            {
                progressDialogStarting = ProgressDialog.show(UserControlActivityPre.this, "Battery Not Found", "Please connect Battery.", false, false);

                /*
                ChargingControl chargingControlRequest = new ChargingControl(devid, true, userid);
                SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
                startTime = sdf1.format(new Date());
                startStation(chargingControlRequest);
                Log.e("SAVE STATION", devid);*/

                editor.putString("devid",devid);
                editor.apply();
                inSessionID = new ChargingSessionID(devid, transID);

                rapidUpdateUI();
                periodicallyUpdateUI();
            }
            else if ((initDeviceStatus.equalsIgnoreCase("ES")) ||
                    (initDeviceStatus.equalsIgnoreCase("OC")) ||
                    (initDeviceStatus.equalsIgnoreCase("GCFI")) ||
                    (initDeviceStatus.equalsIgnoreCase("EP")) ||
                    (initDeviceStatus.equalsIgnoreCase("P")) ||
                    (initDeviceStatus.equalsIgnoreCase("OV")) ||
                    (initDeviceStatus.equalsIgnoreCase("UV")))
            {
                alertUserIssue = new AlertDialog.Builder(UserControlActivityPre.this, R.style.AlertDialogCustom);
                LayoutInflater factory = LayoutInflater.from(UserControlActivityPre.this);
                final View viewUK = factory.inflate(R.layout.control_alert_dialog, null);
                viewUK.setBackgroundResource(android.R.color.transparent);
                alertUserIssue.setView(viewUK);

                alertUserIssue.setTitle("Device Inoperational");
                alertUserIssue.setMessage("Kindly connect your vehicle to another charging station."
                        + "\nIssue: " + initDeviceStatus);
                alertUserIssue.show();

                statsDeviceStatus.setText("Station Out of Order");
            }else{
                alertUserIssue = new AlertDialog.Builder(UserControlActivityPre.this, R.style.AlertDialogCustom);
                LayoutInflater factory = LayoutInflater.from(UserControlActivityPre.this);
                final View viewUK = factory.inflate(R.layout.control_alert_dialog, null);
                viewUK.setBackgroundResource(android.R.color.transparent);
                alertUserIssue.setView(viewUK);

                alertUserIssue.setTitle("Device Inoperational");
                alertUserIssue.setMessage("Kindly connect your vehicle to another charging station."
                        + "\nIssue: " + initDeviceStatus);
                alertUserIssue.show();

                statsDeviceStatus.setText("Station Out of Order");

                statsDeviceStatus.setText("Station Offline");
            }
        });

        rapidUpdateUI();
        periodicallyUpdateUI();
    }

    private void bindUI() {
        fabSwitch = findViewById(R.id.fabPower);
        awesomeSpeedometer= findViewById(R.id.awesomeSpeedometer);

        determinateTimeBar = findViewById(R.id.determinateBar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ImageView imhome = findViewById(R.id.homeButton);
        imhome.setOnClickListener(view -> {
            if(progressDialogStopping!=null){
                if(progressDialogStopping.isShowing())
                    progressDialogStopping.dismiss();
            }

            if(progressDialogStarting!=null){
                if(progressDialogStarting.isShowing())
                    progressDialogStarting.dismiss();
            }
            Intent homeint = new Intent(UserControlActivityPre.this, MultiStationsMapActivity.class);
            startActivity(homeint);
        });

        ImageView ivSearch = findViewById(R.id.searchButton);
        //ivSearch.setImageResource(R.drawable.ic_search_24px_disabled);
        ivSearch.setImageResource(R.drawable.ic_station_1_disabled);
        ivSearch.setOnClickListener(view -> {
            if(progressDialogStopping!=null){
                if(progressDialogStopping.isShowing())
                    progressDialogStopping.dismiss();
            }

            if(progressDialogStarting!=null){
                if(progressDialogStarting.isShowing())
                    progressDialogStarting.dismiss();
            }
            Intent homeint = new Intent(UserControlActivityPre.this, MultiStationsMapActivity.class);
            startActivity(homeint);
        });
        TextView tvSearchTitle = findViewById(R.id.searchText);
        tvSearchTitle.setTextColor(Color.parseColor("#9e9e9e"));

        ImageView ivUser = findViewById(R.id.personButton);
        //ivUser.setImageResource(R.drawable.ic_person_24px_disabled);
        ivUser.setImageResource(R.drawable.ic_person_alt1_disabled);
        ivUser.setOnClickListener(view -> {
            Intent homeint = new Intent(UserControlActivityPre.this, UserActivity.class);
            startActivity(homeint);
        });
        TextView tvUserTitle = findViewById(R.id.personText);
        tvUserTitle.setTextColor(Color.parseColor("#9e9e9e"));

        //TODO
        statsCCurrentStatus = findViewById(R.id.tvMileage);
        tvTimeTitle = findViewById(R.id.tvStatsElapsedTime);
        statsStartTime = findViewById(R.id.tvStatsStartTime);
        statsElapsedTime = findViewById(R.id.tvStatsElapsedTime);
        statsDeviceStatus = findViewById(R.id.tvStatsDeviceStatus);

        statsBatteryVoltage = findViewById(R.id.tvBatteryVoltage);
        statsPower = findViewById(R.id.tvStatsPower);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

            updateBool = false;
            if(progressDialogStopping!=null){
                if(progressDialogStopping.isShowing()){
                    progressDialogStopping.dismiss();
                }
            }
    }

    ////////////////////Repeating UI updates//////////////////////////
    private void periodicallyUpdateUI(){
        //STEP 3 - Station Status
        // TODO replace CountDownTimer to an Appropriate Background Service
        //new CountDownTimer(1200000, 21000) { //Two Hours Limit
        new CountDownTimer(1200000, 10000) { //Two Hours Limit
            public void onTick(long millisUntilFinished) {
                //if(repeatReq){
                if(updateBool){
                    Log.e("GET DEVICE", "Periodic Update");
                    Log.e("GET DEVICE",devid);
                    getDeviceStatus(devid);
                    if(initDeviceStatus.equalsIgnoreCase("CHARGE"))
                    {
                        stillCharging = 1;
                        millisUntilFinished = millisUntilFinished + 1200000;

                        inSessionID = new ChargingSessionID(devid,transID);
                        updateSessionStatus(inSessionID);
                        updateUI(chargingSessionResponse1);
                    }
                }else{
                    //onFinish();
                }
            }

            public void onFinish() {
                inSessionID = new ChargingSessionID(devid,transID);
                updateSessionStatus(inSessionID);
                stillCharging = 2;

                updateUI(chargingSessionResponse1);
                //TODO

                if(progressDialogStarting!=null){
                    if(progressDialogStarting.isShowing()){
                        progressDialogStarting.dismiss();
                    }
                }
            }
        }.start();
    }

    private void rapidUpdateUI(){
        //STEP 3 - Station Status
        new CountDownTimer(1200000, 2000) { //Two Hours Limit
            public void onTick(long millisUntilFinished) {
                //NetworkCheck();
                if(updateBool)
                {
                    Log.e("GET DEVICE", "Rapid Update");
                    Log.e("GET DEVICE",devid);
                    getDeviceStatus(devid);

                    //FAB Color
                    if (initDeviceStatus.equalsIgnoreCase("IDLE")) {
                        ViewCompat.setBackgroundTintList(fabSwitch,
                                ContextCompat.getColorStateList(UserControlActivityPre.this, R.color.colorAccent));
                        fabSwitch.setEnabled(true);
                    } else if (initDeviceStatus.equalsIgnoreCase("CHARGE")) {
                        ViewCompat.setBackgroundTintList(fabSwitch,
                                ContextCompat.getColorStateList(UserControlActivityPre.this, android.R.color.holo_red_light));
                        fabSwitch.setEnabled(true);
                    }else if (initDeviceStatus.equalsIgnoreCase("OFFLINE")) {
                        ViewCompat.setBackgroundTintList(fabSwitch,
                                ContextCompat.getColorStateList(UserControlActivityPre.this, R.color.colorDarkGrey));
                        fabSwitch.setEnabled(false);
                    }

                    //External Stop
                    Log.e("Status","Checking External Stop");
                    Log.e("transID",transID);
                    Log.e("ourStop",Boolean.toString(ourStop));
                    Log.e("initDeviceStatus",initDeviceStatus);
                    //Log.e("prevDeviceStatus",prevDeviceStatus);
                    if((!ourStop)&&(!transID.equals("0"))&&(initDeviceStatus.equalsIgnoreCase("IDLE"))
                            &&(prevDeviceStatus.equalsIgnoreCase("CHARGE"))){

                        Log.e("inStatus","Checking Succeeded");
                        Log.e("intransID",transID);
                        Log.e("inourStop",Boolean.toString(ourStop));
                        Log.e("ininitDeviceStatus",initDeviceStatus);
                        //Log.e("inprevDeviceStatus",prevDeviceStatus);

                        editor.remove("transID");
                        editor.remove("devid");
                        editor.putLong("loginStartSession",0);
                        editor.commit();

                        //Intent backIntent = new Intent(UserControlActivityPre.this, CachedMapActivity.class);

                        if(progressDialogStopping!=null){
                            if(progressDialogStopping.isShowing())
                                progressDialogStopping.dismiss();
                        }

                        if(progressDialogStarting!=null){
                            if(progressDialogStarting.isShowing())
                                progressDialogStarting.dismiss();
                        }

                        Intent backIntent = new Intent(UserControlActivityPre.this, MultiStationsMapActivity.class);
                        editor.remove("transID");
                        editor.apply();
                        if(initDeviceStatus.equalsIgnoreCase("offline")){
                            Toast.makeText(UserControlActivityPre.this,
                                    "Station Offline!!", Toast.LENGTH_SHORT).show();
                        }
                        finish();
                        startActivity(backIntent);
                    }

                    //ProgressDialogs
                    //TODO

                    if (progressDialogStarting != null) {
                        if ((progressDialogStarting.isShowing()) && (initDeviceStatus.equalsIgnoreCase("CHARGE")) && (stillCharging == 1)) {
                            progressDialogStarting.dismiss();
                        } else if ((progressDialogStarting.isShowing()) && (initDeviceStatus.equalsIgnoreCase("VNC"))) {
                            progressDialogStarting.setMessage("Kindly Connect Vehicle.");
                            //statsDeviceStatus.setText("Vehicle Not Connected");
                        }
                    }

                    if (progressDialogStarting != null) {
                        if ((progressDialogStarting.isShowing()) && (initDeviceStatus.equalsIgnoreCase("VNC")) && (stillCharging == 1)) {
                            //progressDialogStarting.setMessage("Kindly connect your vehicle to start charging.");
                        }
                    }


                    if (progressDialogStopping != null) {
                        if ((progressDialogStopping.isShowing()) && (initDeviceStatus.equalsIgnoreCase("IDLE")) && (stillCharging == 1)) {
                            stillCharging = 0;
                            progressDialogStopping.setMessage("Stopping.");
                            getSessionTransaction();
                        }

                        if ((progressDialogStopping.isShowing())) {
                            stillCharging = 0;
                            getSessionTransaction();
                        }
                    }

                    //TIME ELAPSED
                    if (initDeviceStatus.equalsIgnoreCase("CHARGE")) {
                        long epoch;
                        epoch = System.currentTimeMillis() / 1000;

                        if(pref.getLong("loginStartSession",0)==0) {
                            startTimeVal = System.currentTimeMillis() / 1000;
                            editor.putLong("loginStartSession", startTimeVal);
                        }else {
                            startTimeVal = pref.getLong("loginStartSession",0);
                        }

                        long unixDiffSeconds = epoch - startTimeVal;

                        Double vedants_time_thingy = 0.0;
                        if(chargingSessionResponse1!=null) {
                            if(chargingSessionResponse1.getChargingDuration()!=null) {
                                Log.e("VTIME", Double.toString(chargingSessionResponse1.getChargingDuration()));
                                vedants_time_thingy = (Double) (chargingSessionResponse1.getChargingDuration());
                                int mins = (vedants_time_thingy).intValue();
                                Log.e("vmins",Integer.toString(mins));
                                int secs = (int)((vedants_time_thingy-mins) * 100);
                                Log.e("vsec",Integer.toString(secs));
                                unixDiffSeconds = secs + (mins*60);
                                Log.e("ntime",Long.toString(unixDiffSeconds));
                            }
                        }

                        elapsed_hours = TimeUnit.SECONDS.toHours(unixDiffSeconds);
                        elapsed_minutes = TimeUnit.SECONDS.toMinutes(unixDiffSeconds) - (TimeUnit.SECONDS.toHours(unixDiffSeconds) * 60);
                        elapsed_seconds = TimeUnit.SECONDS.toSeconds(unixDiffSeconds) - (TimeUnit.SECONDS.toMinutes(unixDiffSeconds) * 60);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            if (elapsed_hours == 0 && elapsed_minutes == 0) {
                                statsElapsedTime.setText(elapsed_seconds + " s");
                            } else if (elapsed_hours == 0) {
                                statsElapsedTime.setText(elapsed_minutes + " m: " + elapsed_seconds + " s");
                            } else {
                                statsElapsedTime.setText(elapsed_hours + " h: " + elapsed_minutes + " m: " + elapsed_seconds + " s");
                            }
                        }

                        counter = counter + 1;
                        if ((counter / 5) > 99) {
                            counter = 0;
                            determinateTimeBar.setProgress(1);
                        } else {
                            determinateTimeBar.setProgress(counter / 5);
                        }

                    } else {
                        counter = 0;
                    }
                }
            }

            public void onFinish() {
                inSessionID = new ChargingSessionID(devid,transID);
                updateSessionStatus(inSessionID);
                stillCharging = 2;
                updateUI(chargingSessionResponse1);
            }
        }.start();
    }

    private void updateUI(ChargingSessionResponse chargingSessionResponse1)
    {
        if(chargingSessionResponse1!=null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //statsPower.setText(Integer.toString(chargingSessionResponse1.getSoc()));
                statsPower.setText(Double.toString(chargingSessionResponse1.getChargerPower())+" W");

                float energy = chargingSessionResponse1.getChargerEnergy().floatValue();

                if(energy>1000){
                    awesomeSpeedometer.setMaxSpeed(2000);
                    awesomeSpeedometer.setMinSpeed(1000);
                    awesomeSpeedometer.setTickNumber(11);
                }
                else if((energy<1000)&&(energy>100)){
                    awesomeSpeedometer.setMaxSpeed(1000);
                    awesomeSpeedometer.setMinSpeed(0);
                    awesomeSpeedometer.setTickNumber(11);
                }else if((energy<100)&&(energy>10)){
                    awesomeSpeedometer.setMaxSpeed(100);
                    awesomeSpeedometer.setMinSpeed(0);
                    awesomeSpeedometer.setTickNumber(11);
                }else if((energy<10)&&(energy>1)){
                    awesomeSpeedometer.setMaxSpeed(10);
                    awesomeSpeedometer.setMinSpeed(0);
                    awesomeSpeedometer.setTickNumber(11);
                }
                else{
                    awesomeSpeedometer.setMinMaxSpeed(0,10);
                    awesomeSpeedometer.setTickNumber(11);
                }

                awesomeSpeedometer.speedTo(energy);

                //BMS float bvolt = chargingSessionResponse1.getBatteryVoltage().floatValue();
                //BMS statsCCurrentStatus.setText(Float.toString(chargingSessionResponse1.getBatteryCurrent().floatValue()) + "A");

                float bvolt = chargingSessionResponse1.getChargerVoltage().floatValue();
                statsBatteryVoltage.setText(Float.toString(bvolt)+" V");
                statsCCurrentStatus.setText(Float.toString(chargingSessionResponse1.getChargerCurrent().floatValue()) + "A");
            }
            else
            {
                long unixSeconds = chargingSessionResponse1.getTimestamp();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                Date dt = null;
                try {
                    dt = sdf.parse(startTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long epoch;

                if (dt != null) {
                    epoch = dt.getTime();
                } else {
                    epoch = System.currentTimeMillis() / 1000;
                }

                if(pref.getLong("loginStartSession",0)==0) {
                    startTimeVal = System.currentTimeMillis() / 1000;
                    editor.putLong("loginStartSession", startTimeVal);
                }else {
                    startTimeVal = pref.getLong("loginStartSession",0);
                }

                long unixDiffSeconds = epoch - startTimeVal;

                //TODO
                Double vedants_time_thingy = 0.0;
                if(chargingSessionResponse1!=null) {
                    if(chargingSessionResponse1.getChargingDuration()!=null) {
                        Log.e("VTIME", Double.toString(chargingSessionResponse1.getChargingDuration()));
                        vedants_time_thingy = (Double) (chargingSessionResponse1.getChargingDuration());
                        int mins = (vedants_time_thingy).intValue();
                        Log.e("vmins",Integer.toString(mins));
                        int secs = (int)((vedants_time_thingy-mins) * 100);
                        Log.e("vsec",Integer.toString(secs));
                        unixDiffSeconds = secs + (mins*60);
                        Log.e("ntime",Long.toString(unixDiffSeconds));
                    }
                }

                elapsed_hours = TimeUnit.SECONDS.toHours(unixDiffSeconds);
                elapsed_minutes = TimeUnit.SECONDS.toMinutes(unixDiffSeconds) - (TimeUnit.SECONDS.toHours(unixDiffSeconds) * 60);
                elapsed_seconds = TimeUnit.SECONDS.toSeconds(unixDiffSeconds) - (TimeUnit.SECONDS.toMinutes(unixDiffSeconds) * 60);

                if (elapsed_hours == 0 && elapsed_minutes == 0) {
                    statsElapsedTime.setText(Html.fromHtml(elapsed_seconds + " s"));
                } else if (elapsed_hours == 0) {
                    statsElapsedTime.setText(Html.fromHtml(elapsed_minutes + " m: " + elapsed_seconds + " s"));
                } else {
                    statsElapsedTime.setText(Html.fromHtml(elapsed_hours + " h: " + elapsed_minutes + " m: " + elapsed_seconds + " s"));
                }

                //statsPower.setText(Integer.toString(chargingSessionResponse1.getSoc()));
                statsPower.setText(Double.toString(chargingSessionResponse1.getChargerPower())+" W");

                float energy = chargingSessionResponse1.getChargerEnergy().floatValue();

                if(energy>1000){
                    //awesomeSpeedometer.setUnit("kWh");
                    awesomeSpeedometer.setMaxSpeed(2000);
                    awesomeSpeedometer.setMinSpeed(1000);
                    awesomeSpeedometer.setTickNumber(11);
                }
                else if((energy<1000)&&(energy>100)){
                    awesomeSpeedometer.setMaxSpeed(1000);
                    awesomeSpeedometer.setMinSpeed(0);
                    awesomeSpeedometer.setTickNumber(11);

                    Log.e("Status","100<e<1000: 0-1000");
                }else if((energy<100)&&(energy>10)){
                    awesomeSpeedometer.setMaxSpeed(100);
                    awesomeSpeedometer.setMinSpeed(0);
                    awesomeSpeedometer.setTickNumber(11);

                    Log.e("Status","10<e<100: 0-100");
                }else if((energy<10)&&(energy>1)){
                    awesomeSpeedometer.setMaxSpeed(10);
                    awesomeSpeedometer.setMinSpeed(0);
                    awesomeSpeedometer.setTickNumber(11);

                    Log.e("Status","1<e<10: 0-10");
                }
                else{
                    awesomeSpeedometer.setMinMaxSpeed(0,10);
                    awesomeSpeedometer.setTickNumber(11);
                    unitAwe = "Wh";
                }

                awesomeSpeedometer.speedTo(energy);

                //BMS float bvolt = chargingSessionResponse1.getBatteryVoltage().floatValue();
                //BMS statsCCurrentStatus.setText(Float.toString(chargingSessionResponse1.getBatteryCurrent().floatValue()) + "A");

                float bvolt = chargingSessionResponse1.getChargerVoltage().floatValue();
                statsBatteryVoltage.setText(Float.toString(bvolt)+" V");
                statsCCurrentStatus.setText(Float.toString(chargingSessionResponse1.getChargerCurrent().floatValue()) + "A");
            }
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //statsBatteryVoltage.setText("0 kW");
                statsBatteryVoltage.setText("0 V");
                //statsPower.setText("\u20B9 0");
                statsPower.setText("0");
                statsCCurrentStatus.setText("0 A");
            }
        }
    }

    ////////////////////Start Stop//////////////////////////

    void startRepeatingTask(ChargingControl chargeControl) {
        repeatReq = true;
        startStation(chargeControl);
    }

    void stopRepeatingTask(ChargingControl chargeControl) {
        final AlertDialog.Builder aDiag = new AlertDialog.Builder(this);
            aDiag.setTitle("Do you want to stop charging?");
            aDiag.setCancelable(true);

            aDiag.setPositiveButton("Yes, Stop.", (dialog, which) -> {
                    stopStation(chargeControl);
                    stillCharging = 2;
                    dialog.dismiss();
                 });

            aDiag.setNegativeButton("Cancel", (dialog, whichButton) -> {
                    dialog.dismiss();
            });
        aDiag.show();
    }

    ////////////////////Network Calls//////////////////////////
    //Checks
    private boolean checkNetwork() {
        final ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        } else {
            Toast.makeText(this, "No Network Available. Kindly switch on Internet and Try again.", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    //Sets TransactionID
    private void startStation(ChargingControl chargeControl) {
        if(checkNetwork())
        {
            StationControlAPI chargeStationService = retrofit.create(StationControlAPI.class);
            Call<StartResponse> call = chargeStationService.instructDeviceStart(chargeControl);

            if((transID == null)||(transID.equalsIgnoreCase(""))){
                transID = "0";
            }

            call.enqueue(new Callback<StartResponse>() {
                @Override
                public void onResponse(Call<StartResponse> call, Response<StartResponse> response) {

                    //TODO
                    progressDialogStarting.show();
                    StartResponse startResponse = response.body();

                    //Log.e("Session Response",startResponse.toString());
                    //Log.e("Session Status", startResponse.getStatus());
                    //Log.e("Start Response", startResponse.getTransactionID());

                    transID = startResponse.getTransactionID();
                    //tvTimeTitle.setText(transID);
                    Log.e(">>> Start TransID",transID);
                    //Log.e("Saving session info",transID);

                    stillCharging = 1;

                    editor.putString("transID", transID);

                    Log.e(">>> Start DevID",devid);
                    editor.putString("devid", devid);
                    editor.commit();
                }

                @Override
                public void onFailure(Call<StartResponse> call, Throwable throwable) {
                    Log.e("Response for TransID","Failed");
                }
            });
        }
    }

    private void stopStation(ChargingControl chargeControl) {

        if(checkNetwork())
        {
            //TODO
            progressDialogStopping = ProgressDialog.show(UserControlActivityPre.this, "Stopping Charging Session", "Please wait..", false, false);

            StationControlAPI chargeStationService = retrofit.create(StationControlAPI.class);
            Call<StopResponse> call = chargeStationService.instructDeviceStop(chargeControl);

            Log.e("Status","Stop Station");
            ourStop = true;

            call.enqueue(new Callback<StopResponse>() {
                @Override
                public void onResponse(Call<StopResponse> call, Response<StopResponse> response) {
                    stillCharging = 2;
                    StopResponse stopResponse = response.body();
                    Log.e("Session Stop Response",stopResponse.toString());
                    Log.e("Session Stop Response Stat",stopResponse.getStatus());
                    prevDeviceStatus = "CHARGE";

                    initDeviceStatus = "STOPPED";
                    if(initDeviceStatus!="CHARGE")
                    {
                        if(initDeviceStatus.equalsIgnoreCase("offline")){
                            Toast.makeText(UserControlActivityPre.this,
                                    "Station Offline!!", Toast.LENGTH_SHORT).show();
                        }

                        if(progressDialogStopping!=null){
                            if(progressDialogStopping.isShowing())
                                progressDialogStopping.dismiss();
                        }

                        if(progressDialogStarting!=null){
                            if(progressDialogStarting.isShowing())
                                progressDialogStarting.dismiss();
                        }

                        //TODO
                        editor.remove("transID");
                        editor.apply();

                        Intent inty = new Intent(UserControlActivityPre.this, MultiStationsMapActivity.class);
                        startActivity(inty);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<StopResponse> call, Throwable throwable) {
                    Log.e("Session Response","Failed");
                }
            });
        }else{
            Toast.makeText(this, "If you're unable to connect to the internet, do not worry. Your session will stop on full charge.", Toast.LENGTH_LONG).show();
        }
    }

    private void getSessionTransaction(){
        Log.e("Transaction ID",transID);
        //TODO
        progressDialogStopping.dismiss();
        repeatReq = false;

        //TODO Working

        if(initDeviceStatus!="CHARGE")
        {
            if(initDeviceStatus.equalsIgnoreCase("offline")){
                Toast.makeText(UserControlActivityPre.this,
                        "Station Offline!!", Toast.LENGTH_SHORT).show();
            }

            if(progressDialogStopping!=null){
                if(progressDialogStopping.isShowing())
                    progressDialogStopping.dismiss();
            }

            if(progressDialogStarting!=null){
                if(progressDialogStarting.isShowing())
                    progressDialogStarting.dismiss();
            }

            editor.remove("transID");
            editor.apply();
            Intent inty = new Intent(UserControlActivityPre.this, MultiStationsMapActivity.class);
            startActivity(inty);
            finish();
        }

        /*
        TransactionRequest transactionRequest = new TransactionRequest(transID);
        TransactionAPI transactionService = retrofit.create(TransactionAPI.class);
        Call<TransactionResponse> call = transactionService.getTransactionResponse(transactionRequest);

        Log.e("Status","Fetching Transaction Details");

        if(initDeviceStatus!="CHARGE")
        {
            call.enqueue(new Callback<TransactionResponse>() {
                @Override
                public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {

                    TransactionResponse transactionResponse = response.body();

                    if(transactionResponse!=null)
                    {
                        Log.e("Transaction Status", transactionResponse.getTransactionId());
                        Log.e("Trasnsaction Response", transactionResponse.toString());

                        //Clear old Session
                        editor.remove("transID");
                        editor.remove("devid");
                        editor.putLong("loginStartSession",0);
                        editor.commit();

                        //Save to Local DB
                        TransactionEntity transactionE = new TransactionEntity(transactionResponse.getId(),
                                transactionResponse.getDeviceId(), transactionResponse.getChildId(),
                                epochToHumanDate(transactionResponse.getStartTime()),
                                transactionResponse.getStartTime(),
                                epochToMonthNumber(transactionResponse.getStartTime()),
                                transactionResponse.getTransactionId(),
                                transactionResponse.getEnergyConsumed(),
                                statsElapsedTime.getText().toString(),
                                transactionResponse.getSoc());

                        Log.e("StartTimeStamp",transactionResponse.getStartTime().toString());
                        Log.e("UserControl Transaction Saved:: ",transactionE.toString());

                        AddTransactionAsync addTask = new AddTransactionAsync(getApplicationContext(),transactionE);
                        addTask.execute();

                        //Intent to Map
                        //progressDialogStopping.dismiss();
                        repeatReq = false;
                        Intent inty = new Intent(UserControlActivityPre.this, MultiStationsMapActivity.class);
                        startActivity(inty);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<TransactionResponse> call, Throwable throwable) {
                    Log.e("Status","Retrofit session status HTTP Calling Failed");
                }
            });
        }*/

    }

    private void getDeviceStatus(String devidin) {

        if(devidin.length()<17){
            String childid = pref.getString("oldChildID","00");

            if(childid.length()<2){
                devidin = devidin+"-0"+childid;
            }else{
                devidin = devidin+"-"+childid;
            }
        }

        DeviceIDRequest devIDRequest = new DeviceIDRequest(devidin);
        StationControlAPI chargeStationService = retrofit.create(StationControlAPI.class);
        Call<DeviceStatusResponse> call = chargeStationService.getDeviceStatus(devIDRequest);

        //TODO
        Log.e("Device Status Request", devIDRequest.toString());

        String finalDevidin = devidin;
        call.enqueue(new Callback<DeviceStatusResponse>() {
            @Override
            public void onResponse(Call<DeviceStatusResponse> call, Response<DeviceStatusResponse> response) {

                if(initDeviceStatus.equalsIgnoreCase("CHARGE"))
                    prevDeviceStatus = "CHARGE";

                if(response.code()==200){
                    if(response.body().getDeviceStatus()!=null)
                    {
                        Log.e("Control:: Status",response.body().getDeviceStatus());
                        if(response.body().getDeviceStatus().equalsIgnoreCase("Device is offline"))
                        {
                            initDeviceStatus = "offline";
                        }
                    }
                    else{
                        DeviceStatusResponse deviceResponse = response.body();
                        Log.e("Device Status Request", devIDRequest.toString());
                        Log.e("Device Status Response",deviceResponse.toString());
                        String childid = finalDevidin.substring(finalDevidin.length()-2);
                        Log.e("ChildID",childid);
                        editor.putString("oldChildID",childid);
                        editor.apply();
                        Log.e("Device Status Request Post", devIDRequest.toString());
                        Log.e("ParentStatus", deviceResponse.getParentDeviceStatus());

                        if(deviceResponse.getParentDeviceStatus().equalsIgnoreCase("IDLE"))
                        {
                            if(childid.equalsIgnoreCase("01")){
                                initDeviceStatus = deviceResponse.getChild1DeviceStatus();
                                Log.e("Child1Status",deviceResponse.getChild1DeviceStatus());
                            }else if(childid.equalsIgnoreCase("02")){
                                initDeviceStatus = deviceResponse.getChild2DeviceStatus();
                                Log.e("Child2Status",deviceResponse.getChild2DeviceStatus());
                            }else if(childid.equalsIgnoreCase("03")){
                                initDeviceStatus = deviceResponse.getChild3DeviceStatus();
                                Log.e("Child3Status",deviceResponse.getChild3DeviceStatus());
                            }

                            if((prevDeviceStatus.equalsIgnoreCase("BNC"))&&
                                    (initDeviceStatus.equalsIgnoreCase("CHARGE"))){
                                if(progressDialogStarting!=null){
                                    if(progressDialogStarting.isShowing()){
                                        prevDeviceStatus = "CHARGE";
                                        progressDialogStarting.dismiss();
                                    }
                                }
                            }else if((prevDeviceStatus.equalsIgnoreCase("BNC"))&&
                                    (initDeviceStatus.equalsIgnoreCase("IDLE"))){
                                if(progressDialogStarting!=null){
                                    if(progressDialogStarting.isShowing()){
                                        prevDeviceStatus = "IDLE";
                                        progressDialogStarting.dismiss();

                                        final AlertDialog.Builder aDiag =
                                                new AlertDialog.Builder(UserControlActivityPre.this);
                                        aDiag.setTitle("No Battery Connected!");
                                        aDiag.setMessage("Connect a battery before starting charging.");
                                        aDiag.setCancelable(true);

                                        aDiag.setPositiveButton("Exit", (dialog, which) -> {
                                            stillCharging = 2;
                                            dialog.dismiss();
                                            editor.remove("transID");
                                            editor.apply();

                                            Intent exitIntent= new Intent(UserControlActivityPre.this,
                                                    MultiStationsMapActivity.class);
                                            startActivity(exitIntent);
                                            finish();
                                        });

                                        aDiag.setNegativeButton("Retry", (dialog, whichButton) -> {
                                            editor.remove("transID");
                                            editor.apply();
                                            dialog.dismiss();
                                        });
                                        aDiag.show();
                                    }
                                }
                            }

                            //Stop on FAULT
                            if((initDeviceStatus.equalsIgnoreCase("BNC"))){
                                if(progressDialogStarting!=null){
                                    if(progressDialogStarting.isShowing()){
                                        prevDeviceStatus = "BNC";
                                    }
                                }

                                statsDeviceStatus.setText("Battery Not Found");
                                createNotification("BSS Battery Disconnected!");
                            }
                            else if((initDeviceStatus.equalsIgnoreCase("EM"))||
                                    (initDeviceStatus.equalsIgnoreCase("OVP"))||
                                    (initDeviceStatus.equalsIgnoreCase("BMSF"))||
                                    (initDeviceStatus.equalsIgnoreCase("FAULT"))){
                                stillCharging = 1;
                                Toast.makeText(UserControlActivityPre.this,
                                        "Charger has "+ initDeviceStatus +".Stopping Session.",
                                        Toast.LENGTH_LONG).show();

                                ChargingControl chargeControl1 = new ChargingControl(devid,false, userid);
                                stillCharging = 2;
                                createNotification("BSS Charging Stopped on Port Fault:"+initDeviceStatus);
                                stopStation(chargeControl1);
                            }
                            else if((deviceResponse.getParentDeviceStatus().equalsIgnoreCase("EM"))||
                                    (deviceResponse.getParentDeviceStatus().equalsIgnoreCase("OVP"))||
                                    (deviceResponse.getParentDeviceStatus().equalsIgnoreCase("BMSF")))
                            {
                                stillCharging = 1;
                                Toast.makeText(UserControlActivityPre.this,
                                        "Fault. Stopping Session.", Toast.LENGTH_LONG).show();

                                ChargingControl chargeControl1 = new ChargingControl(devid,false, userid);
                                stillCharging = 2;
                                createNotification("BSS Charging Stopped on BSS Fault:"+initDeviceStatus);
                                stopStation(chargeControl1);
                            }
                        }else{
                            Toast.makeText(UserControlActivityPre.this,
                                    "Fault. Session Stopped.", Toast.LENGTH_LONG).show();

                            editor.remove("transID");
                            editor.apply();
                            Intent inty = new Intent(UserControlActivityPre.this, MultiStationsMapActivity.class);
                            startActivity(inty);
                            finish();
                        }
                    }
                }
                else
                    initDeviceStatus = "offline";

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    if(initDeviceStatus.equalsIgnoreCase("IDLE")) {
                        statsDeviceStatus.setText(Html.fromHtml("<font color='#007BB6'>Ready to Charge</font>", Html.FROM_HTML_MODE_COMPACT));

                        if(stillCharging == 2){
                            stillCharging = 1;
                            getSessionTransaction();
                        }

                    }
                    else if(initDeviceStatus.equalsIgnoreCase("CHARGE")){
                        statsDeviceStatus.setText(Html.fromHtml("<font color='#007BB6'>Charging</font>", Html.FROM_HTML_MODE_COMPACT));
                    }else if(initDeviceStatus.equalsIgnoreCase("OFFLINE")) {
                        statsDeviceStatus.setText(Html.fromHtml("<font color='#808080'>Station Offline</font>", Html.FROM_HTML_MODE_COMPACT));
                    }

                    if(initDeviceStatus.equalsIgnoreCase("CHARGE")){
                        stillCharging = 1;
                    }else if((stillCharging==2)&&(initDeviceStatus.equalsIgnoreCase("IDLE"))){
                        stillCharging = 0;
                    }
                }
            }

            @Override
            public void onFailure(Call<DeviceStatusResponse> call, Throwable throwable) {
                Log.e("initDeviceStatus","Offline");
                initDeviceStatus = "offline";
            }
        });

        //todo temp
        updateSessionStatus(inSessionID);
        updateUI(chargingSessionResponse1);
        //todo
    }

    private void updateSessionStatus(ChargingSessionID inSessionID){
        StationControlAPI chargeStationService = retrofit.create(StationControlAPI.class);
        Call<ChargingSessionResponse> call = chargeStationService.getChargingStatus(inSessionID);

        call.enqueue(new Callback<ChargingSessionResponse>() {
            @Override
            public void onResponse(Call<ChargingSessionResponse> call, Response<ChargingSessionResponse> response) {

                //Log.e("Status","Fetching Charging Session Status");

                ChargingSessionResponse sessionResponse = response.body();

                if(sessionResponse!=null) {
                    Log.e("Session Response", sessionResponse.toString());

                    chargingSessionResponse1 = sessionResponse;

                    Log.e("Session Response", chargeStationService.toString());

                    updateUI(chargingSessionResponse1);
                }
                else{
                    Log.e("Session Response", "NULL");
                }
            }

            @Override
            public void onFailure(Call<ChargingSessionResponse> call, Throwable throwable) {
                Log.e("Status","Charging Session Response Failed");
            }
        });
    }

    //TODO
    @Override
    protected void onPostResume() {
        super.onPostResume();

        Log.e("Status","Resuming User Control");

        //Printing details for initial start
        Log.e("Status","Initial");
        Log.e("TransID",transID);
        Log.e("DeviceStatus", initDeviceStatus);

        Log.e("OldTID",pref.getString("transID", "0"));

        if(!pref.getString("transID", "0").equals("0")) {
            Log.e("Control: Status","Found On Going Session.");

            transID = pref.getString("transID", "0");
            devid = pref.getString("devid", "0");
            inSessionID = new ChargingSessionID(devid,transID);
            stillCharging = 1;
            initDeviceStatus = "CHARGE";

            //Printing Details resumed
            Log.e("UCA Status","Resumed");
            Log.e("UCA TransID",transID);
            Log.e("UCA DeviceID",devid);
            Log.e("UCA inSessionID",inSessionID.toString());
            Log.e("UCA DeviceStatus", initDeviceStatus);

            updateSessionStatus(inSessionID);
            updateUI(chargingSessionResponse1);

            updateBool = true;

            rapidUpdateUI();
            periodicallyUpdateUI();
        }else{
            initDeviceStatus = "IDLE";
            Log.e("Control: Status","New Session");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        updateBool = false;
        if(progressDialogStopping!=null){
            if(progressDialogStopping.isShowing()){
                progressDialogStopping.dismiss();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(progressDialogStopping!=null){
            if(progressDialogStopping.isShowing())
                progressDialogStopping.dismiss();
        }
    }

    private String epochToHumanDate(long epochSeconds){
        long itemLong = epochSeconds;
        Date d = new Date(itemLong*1000L);
        String itemDateStr = new SimpleDateFormat("dd-MMM-YY | HH:mm a").format(d);
        return itemDateStr;
    }

    private int epochToMonthNumber(long epochSeconds){
        long itemLong = epochSeconds;
        Date d = new Date(itemLong*1000L);
        String itemDateStr = new SimpleDateFormat("MM").format(d);
        return Integer.valueOf(itemDateStr);
    }

    public void createNotification(String message) {
        // Prepare intent which is triggered if the
        // notification is selected
        Intent intent = new Intent(this, MultiStationsMapActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

        // Build notification
        Notification noti = new Notification.Builder(this)
                .setContentTitle("BSS Charging Alert")
                .setContentText(message)//.setSmallIcon(R.drawable.icon)
                .setContentIntent(pIntent).build();
                /*.addAction(R.drawable.icon, "Call", pIntent)
                .addAction(R.drawable.icon, "More", pIntent)
                .addAction(R.drawable.icon, "And more", pIntent).build();*/
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, noti);
    }
}
