package com.evqpoint.evqpointbatteryswappingstationapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;

public class AccountDetailsActivity extends AppCompatActivity {

    SharedPreferences pref;
    String email;
    String mobile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        TextView tvEmail = findViewById(R.id.tvEmail);
        TextView tvPhone = findViewById(R.id.tvMobile);
        Button btnChangePassword = findViewById(R.id.changePassButton);

        pref = this.getSharedPreferences("LoginPref", 0);
        if(pref.getString("loginEmail", null) != null){
            email = pref.getString("loginEmail", "");
            tvEmail.setText(email);
        }
        if(pref.getString("loginPhone", null) != null){
            mobile = pref.getString("loginPhone", "");
            tvPhone.setText(mobile);
        }

        ImageView ivEditEmail = findViewById(R.id.ivEditEmail);
        ivEditEmail.setOnClickListener(v -> {
            Intent updateEmailIntent = new Intent(AccountDetailsActivity.this, NewEmailActivity.class);
            startActivity(updateEmailIntent);
        });

        ImageView ivEditPhone = findViewById(R.id.ivEditPhone);
        ivEditPhone.setOnClickListener(v -> {
            Intent updatePhoneIntent = new Intent(AccountDetailsActivity.this, NewPhoneActivity.class);
            startActivity(updatePhoneIntent);
        });

        btnChangePassword.setOnClickListener(v -> {
            Intent updatePassIntent = new Intent(AccountDetailsActivity.this, NewPasswordActivity.class);
            startActivity(updatePassIntent);
        });
    }
}
