package com.evqpoint.evqpointbatteryswappingstationapp.activities.vehicles;

public class Vehicle {

    String vehicleNickName;
    String vehicleMake;
    String vehicleType;
    String vehicleModel;
    String portType;

    public String getVehicleNickName() {
        return vehicleNickName;
    }

    public void setVehicleNickName(String vehicleNickName) {
        this.vehicleNickName = vehicleNickName;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getPortType() {
        return portType;
    }

    public void setPortType(String portType) {
        this.portType = portType;
    }

    public Vehicle(String vehicleNickName, String vehicleMake, String vehicleType, String vehicleModel, String portType) {
        this.vehicleNickName = vehicleNickName;
        this.vehicleMake = vehicleMake;
        this.vehicleType = vehicleType;
        this.vehicleModel = vehicleModel;
        this.portType = portType;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "vehicleNickName='" + vehicleNickName + '\'' +
                ", vehicleMake='" + vehicleMake + '\'' +
                ", vehicleType='" + vehicleType + '\'' +
                ", vehicleModel='" + vehicleModel + '\'' +
                ", portType='" + portType + '\'' +
                '}';
    }
}
