package com.evqpoint.evqpointbatteryswappingstationapp.activities.mapactivity;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.ChargingStation;

public class StationMarker implements ClusterItem {

  private final ChargingStation cstation;
  private final LatLng latLng;

  public StationMarker(ChargingStation cStation, LatLng latLng) {
    this.cstation = cStation;
    this.latLng = latLng;
  }

  @Override
  public LatLng getPosition() {
    return latLng;
  }

  @Override
  public String getTitle() {
    return this.cstation.getName();
  }

  public ChargingStation getStation() {
    return this.cstation;
  }

  @Override
  public String getSnippet() {
    return this.cstation.getAddress();
  }

  @NonNull
  @Override
  public String toString() {
    return this.cstation.toString()+"\n"+this.latLng.toString();
  }
}
