package com.evqpoint.evqpointbatteryswappingstationapp.network.addstation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteChargingStation {

    @SerializedName("stationId")
    @Expose
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DeleteChargingStation(String id) {
        this.id = id;
    }
}
