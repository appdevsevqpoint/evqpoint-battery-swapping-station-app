package com.evqpoint.evqpointbatteryswappingstationapp.network.networktests;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TransactionTestActivity extends AppCompatActivity {

    //UI
    TextView tvRequest;
    TextView tvResponse;
    Button btnRequest;

    //Retrofit
    public static final String BASE_URL = "https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;
    TransactionResponse transactionResponse;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.test_ui_device_charging_status);
        tvRequest = findViewById(R.id.tvRequestParameters);
        tvResponse = findViewById(R.id.tvRestResponse);
        btnRequest = findViewById(R.id.btnRequest);

        String devid = "BNG-001-06";
        String transid = "b2942d34-fd96-4b91-9d1a-390b1579227d";

        tvRequest.setText("devid = "+devid+"\ntransid = "+transid);

        /* Request
        {
            "transactionID": "b2942d34-fd96-4b91-9d1a-390b1579227d"
        }*/

        /* Response
        * 400 Status
        * {
                "status": "No transaction has been made yet."
            }
        *
        * */

        TransactionRequest transactionRequest = new TransactionRequest(transid);

        btnRequest.setOnClickListener(v ->{
            updateTransactionResponse(transactionRequest);
            tvResponse.setText(transactionResponse.toString());
        });
    }

    private void updateTransactionResponse(TransactionRequest inTransactionRequest){
        TransactionAPI transactionService = retrofit.create(TransactionAPI.class);
        Call<TransactionResponse> call = transactionService.getTransactionResponse(inTransactionRequest);

        //Log.e("Status","Retrofit session status HTTP Call");

        call.enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                TransactionResponse transactionResponse = response.body();

                if(transactionResponse!=null) {
                    Log.e("Transaction Status", transactionResponse.getTransactionId());
                    Log.e("Trasnsaction Response", transactionResponse.toString());
                    transactionResponse = transactionResponse;
                }
            }

            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable throwable) {
                Log.e("Status","Retrofit session status HTTP Calling Failed");
            }
        });
    }
}
