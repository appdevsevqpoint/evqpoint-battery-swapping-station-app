package com.evqpoint.evqpointbatteryswappingstationapp.network.paymentgateway;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CFOrder {

  @SerializedName("orderAmount")
  @Expose
  private String orderAmount;

  public String getOrderAmount() {
    return orderAmount;
  }

  public void setOrderAmount(String orderAmount) {
    this.orderAmount = orderAmount;
  }

  public CFOrder(String orderAmount) {
    this.orderAmount = orderAmount;
  }

}