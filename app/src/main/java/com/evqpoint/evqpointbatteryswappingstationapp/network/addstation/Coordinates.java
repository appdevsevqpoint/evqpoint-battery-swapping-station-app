package com.evqpoint.evqpointbatteryswappingstationapp.network.addstation;

import java.util.ArrayList;
import java.util.List;

public class Coordinates {

    private double latitude;
    private double longitude;

    public Coordinates(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Coordinates(String inCoords) {

        inCoords = inCoords.replace("[", "");
        inCoords = inCoords.replace("]", "");
        inCoords = inCoords.replace("(", "");
        inCoords = inCoords.replace(")", "");

        this.latitude = Double.valueOf(inCoords.substring(0,inCoords.indexOf(",")));
        this.longitude = Double.valueOf(inCoords.substring(inCoords.indexOf(",")+1));
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public List<Double> getCoordinates(){
        ArrayList<Double> coord = new ArrayList<Double>();
        coord.add(getLatitude());
        coord.add(getLongitude());
        return coord;
    }

    public String toString(){
        return this.getLatitude() +","+ this.getLatitude();
    }

}
