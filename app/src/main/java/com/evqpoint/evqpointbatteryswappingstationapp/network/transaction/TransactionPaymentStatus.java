package com.evqpoint.evqpointbatteryswappingstationapp.network.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionPaymentStatus {

    @SerializedName("transactionID")
    @Expose
    private String transactionID;
    @SerializedName("paymentStatus")
    @Expose
    private String paymentStatus;

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public TransactionPaymentStatus(String transactionID, String paymentStatus) {
        this.transactionID = transactionID;
        this.paymentStatus = paymentStatus;
    }
}
