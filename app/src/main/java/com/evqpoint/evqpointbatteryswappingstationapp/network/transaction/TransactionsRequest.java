package com.evqpoint.evqpointbatteryswappingstationapp.network.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionsRequest {

    @SerializedName("userId")
    @Expose
    private String userId;

    /**
     * No args constructor for use in serialization
     *
     */
    public TransactionsRequest() {
    }

    /**
     *
     * @param userId
     */
    public TransactionsRequest(String userId) {
        super();
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "TransactionsRequest{" +
                "userId='" + userId + '\'' +
                '}';
    }
}