package com.evqpoint.evqpointbatteryswappingstationapp.network.vehicles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAllVehiclesRequest {

    @SerializedName("userid")
    @Expose
    String userid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public GetAllVehiclesRequest(String userid) {
        this.userid = userid;
    }
}
