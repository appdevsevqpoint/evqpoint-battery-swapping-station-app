package com.evqpoint.evqpointbatteryswappingstationapp.network.addstation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChargingStation {

  @SerializedName("_id")
  @Expose
  private String id;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("address")
  @Expose
  private String address;
  @SerializedName("location")
  @Expose
  private List<Double> location = null;
  @SerializedName("image")
  @Expose
  private String image;
  @SerializedName("isDeleted")
  @Expose
  private Boolean isDeleted;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public List<Double> getLocation() {
    return location;
  }

  public void setLocation(List<Double> location) {
    this.location = location;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public Boolean getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(Boolean isDeleted) {
    this.isDeleted = isDeleted;
  }

  public ChargingStation(String id, String name, String address, List<Double> location, String image, Boolean isDeleted) {
    this.id = id;
    this.name = name;
    this.address = address;
    this.location = location;
    this.image = image;
    this.isDeleted = isDeleted;
  }

  public ChargingStation(ChargingStation chargingStation) {
    this.id = chargingStation.getId();
    this.name = chargingStation.getName();
    this.address = chargingStation.getAddress();
    this.location = chargingStation.getLocation();
    this.image = chargingStation.getImage();
    this.isDeleted = chargingStation.getIsDeleted();
  }

  @Override
  public String toString() {
    return "{" +
            "id=\"" + id + '\"' +
            ", name=\"" + name + '\"' +
            ", address=\"" + address + '\"' +
            ", location=\"" + location + '\"' +
            ", image=\"" + image + '\"' +
            ", isDeleted=\"" + isDeleted + '\"' +
            '}';
  }
}