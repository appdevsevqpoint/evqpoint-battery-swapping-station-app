package com.evqpoint.evqpointbatteryswappingstationapp.network.transaction;

import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.TransactionEntity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.evqpoint.evqpointbatteryswappingstationapp.db.transactions.TransactionEntity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TransactionsResponse {

    @SerializedName("result")
    @Expose
    private List<Transaction> transactions = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public TransactionsResponse() {
    }

    /**
     *
     * @param transactions
     */
    public TransactionsResponse(List<Transaction> transactions) {
        super();
        this.transactions = transactions;
    }

    public List<TransactionEntity> getTransactionsAsEntity() {

        List<TransactionEntity> newTEList = new ArrayList<TransactionEntity>();

        for(int uu=0; uu<transactions.size(); uu++){
            TransactionEntity newTE = new TransactionEntity(transactions.get(uu).getId(),
                    transactions.get(uu).getDeviceId(),
                    transactions.get(uu).getChildId(),
                    Integer.toString(transactions.get(uu).getStartTime()),
                    transactions.get(uu).getStartTime(),
                    epochToMonthNumber(transactions.get(uu).getStartTime()),
                    transactions.get(uu).getTransactionId(),
                    transactions.get(uu).getEnergyConsumed(),
                    secondsToStandardTimeSpan(transactions.get(uu).getEndTime()-transactions.get(uu).getStartTime()),
                    transactions.get(uu).getSoc());

            newTEList.add(newTE);
        }

        return newTEList;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    private int epochToMonthNumber(long epochSeconds){
        long itemLong = epochSeconds;
        java.util.Date d = new java.util.Date(itemLong*1000L);
        String itemDateStr = new SimpleDateFormat("MM").format(d);
        return Integer.valueOf(itemDateStr);
    }

    private String secondsToStandardTimeSpan(long seconds){
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (day *24);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) *60);
        String timespan = Integer.toString(day)+" D "+ Long.toString(hours)+" H "+ Long.toString(minute)+" m "+ Long.toString(second)+ " s";
        return timespan;
    }
}