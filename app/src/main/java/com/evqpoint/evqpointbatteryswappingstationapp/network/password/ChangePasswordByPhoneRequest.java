package com.evqpoint.evqpointbatteryswappingstationapp.network.password;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangePasswordByPhoneRequest {

    @SerializedName("oldpassword")
    @Expose
    private String oldpassword;
    @SerializedName("newpassword")
    @Expose
    private String newpassword;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;

    /**
     * @param phoneNumber
     * @param oldpassword
     * @param newpassword
     */
    public ChangePasswordByPhoneRequest(String oldpassword, String newpassword, String phoneNumber) {
        super();
        this.oldpassword = oldpassword;
        this.newpassword = newpassword;
        this.phoneNumber = phoneNumber;
    }

    public String getOldpassword() {
        return oldpassword;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "ChangePasswordByPhoneRequest{" +
                "oldpassword='" + oldpassword + '\'' +
                ", newpassword='" + newpassword + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}