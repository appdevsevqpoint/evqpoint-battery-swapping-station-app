package com.evqpoint.evqpointbatteryswappingstationapp.network.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transaction {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("startTime")
    @Expose
    private Integer startTime;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    @SerializedName("energyConsumed")
    @Expose
    private Double energyConsumed;
    @SerializedName("endTime")
    @Expose
    private Integer endTime;
    @SerializedName("childId")
    @Expose
    private Integer childId;
    @SerializedName("soc")
    @Expose
    private Integer soc;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;

    /**
     * No args constructor for use in serialization
     *
     */
    public Transaction() {

    }

    /**
     *
     * @param energyConsumed
     * @param isDeleted
     * @param soc
     * @param startTime
     * @param id
     * @param endTime
     * @param childId
     * @param deviceId
     * @param transactionId
     */
    public Transaction(String id, String deviceId, Integer startTime, String transactionId,
                  Double energyConsumed, Integer endTime, Integer childId, Integer soc,
                  Boolean isDeleted) {
        super();
        this.id = id;
        this.deviceId = deviceId;
        this.startTime = startTime;
        this.transactionId = transactionId;
        this.energyConsumed = energyConsumed;
        this.endTime = endTime;
        this.childId = childId;
        this.soc = soc;
        this.isDeleted = isDeleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Double getEnergyConsumed() {
        return energyConsumed;
    }

    public void setEnergyConsumed(Double energyConsumed) {
        this.energyConsumed = energyConsumed;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public Integer getChildId() {
        return childId;
    }

    public void setChildId(Integer childId) {
        this.childId = childId;
    }

    public Integer getSoc() {
        return soc;
    }

    public void setSoc(Integer soc) {
        this.soc = soc;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", startTime=" + startTime +
                ", transactionId='" + transactionId + '\'' +
                ", energyConsumed=" + energyConsumed +
                ", endTime=" + endTime +
                ", childId=" + childId +
                ", soc=" + soc +
                ", isDeleted=" + isDeleted +
                '}';
    }
}