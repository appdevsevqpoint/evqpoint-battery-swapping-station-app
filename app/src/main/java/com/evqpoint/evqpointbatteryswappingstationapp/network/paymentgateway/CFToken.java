package com.evqpoint.evqpointbatteryswappingstationapp.network.paymentgateway;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CFToken {
  @SerializedName("orderId")
  @Expose
  private String orderId;
  @SerializedName("body")
  @Expose
  private CFTokenBody body;

  public String getOrderId() {
    return orderId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public CFTokenBody getBody() {
    return body;
  }

  public void setBody(CFTokenBody body) {
    this.body = body;
  }

  public CFToken(String orderId, CFTokenBody body) {
    this.orderId = orderId;
    this.body = body;
  }

}