package com.evqpoint.evqpointbatteryswappingstationapp.network.otp;

import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.DeviceIDRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.DeviceStatusResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface OtpApi {

    @GET
    Call<OtpResponse> sendEmailOTP(@Url String url);

}
