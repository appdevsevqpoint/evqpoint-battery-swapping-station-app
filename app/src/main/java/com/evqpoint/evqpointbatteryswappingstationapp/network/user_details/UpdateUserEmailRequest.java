package com.evqpoint.evqpointbatteryswappingstationapp.network.user_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public class UpdateUserEmailRequest {

    @NotNull
    @SerializedName("userid")
    @Expose
    private String userid;
    @NotNull
    @SerializedName("email")
    @Expose
    private String email;
    @NotNull
    @SerializedName("phone_number")
    @Expose
    private String phone;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UpdateUserEmailRequest(@NotNull String userid, @NotNull String email, @NotNull String phone) {
        this.userid = userid;
        this.email = email;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "UpdateUserRequest{" +
                "userid='" + userid + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
