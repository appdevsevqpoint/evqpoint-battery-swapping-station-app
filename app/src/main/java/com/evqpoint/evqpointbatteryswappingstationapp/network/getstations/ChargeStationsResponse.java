package com.evqpoint.evqpointbatteryswappingstationapp.network.getstations;

import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.ChargingStation;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.ChargingStation;

import java.util.List;

public class ChargeStationsResponse {
  @SerializedName("chargingStationsList")
  @Expose
  private List<ChargingStation> chargingStations = null;

  public List<ChargingStation> getChargingStations() {
    return chargingStations;
  }

  public void setChargingStations(List<ChargingStation> chargingStations) {
    this.chargingStations = chargingStations;
  }
}