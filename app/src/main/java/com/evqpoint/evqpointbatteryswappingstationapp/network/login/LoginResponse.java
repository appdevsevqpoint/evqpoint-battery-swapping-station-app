package com.evqpoint.evqpointbatteryswappingstationapp.network.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse {
  @SerializedName("_id")
  @Expose
  private String id;
  @SerializedName("password")
  @Expose
  private String password;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("email")
  @Expose
  private String email;
  @SerializedName("phone_number")
  @Expose
  private String phoneNumber;
  @SerializedName("isConfirmed")
  @Expose
  private Boolean isConfirmed;
  @SerializedName("isEnabled")
  @Expose
  private Boolean isEnabled;
  @SerializedName("isDeleted")
  @Expose
  private Boolean isDeleted;
  @SerializedName("childId")
  @Expose
  private Integer childId;
  @SerializedName("lastdevid")
  @Expose
  private String lastdevid;
  @SerializedName("transactionId")
  @Expose
  private List<String> transactionId = null;
  @SerializedName("address")
  @Expose
  private String address;
  @SerializedName("city")
  @Expose
  private String city;
  @SerializedName("country")
  @Expose
  private String country;
  @SerializedName("state")
  @Expose
  private String state;
  @SerializedName("token")
  @Expose
  private String token;

  /**
   * @param id
   * @param password
   * @param name
   * @param email
   * @param phoneNumber
   * @param isConfirmed
   * @param isEnabled
   * @param isDeleted
   * @param childId
   * @param lastdevid
   * @param transactionId
   * @param address
   * @param city
   * @param country
   * @param state
   * @param token
   */
  public LoginResponse(String id, String password, String name, String email, String phoneNumber, Boolean isConfirmed, Boolean isEnabled, Boolean isDeleted, Integer childId, String lastdevid, List<String> transactionId, String address, String city, String country, String state, String token) {
    super();
    this.id = id;
    this.password = password;
    this.name = name;
    this.email = email;
    this.phoneNumber = phoneNumber;
    this.isConfirmed = isConfirmed;
    this.isEnabled = isEnabled;
    this.isDeleted = isDeleted;
    this.childId = childId;
    this.lastdevid = lastdevid;
    this.transactionId = transactionId;
    this.address = address;
    this.city = city;
    this.country = country;
    this.state = state;
    this.token = token;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public Boolean getIsConfirmed() {
    return isConfirmed;
  }

  public void setIsConfirmed(Boolean isConfirmed) {
    this.isConfirmed = isConfirmed;
  }

  public Boolean getIsEnabled() {
    return isEnabled;
  }

  public void setIsEnabled(Boolean isEnabled) {
    this.isEnabled = isEnabled;
  }

  public Boolean getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(Boolean isDeleted) {
    this.isDeleted = isDeleted;
  }

  public Integer getChildId() {
    return childId;
  }

  public void setChildId(Integer childId) {
    this.childId = childId;
  }

  public String getLastdevid() {
    return lastdevid;
  }

  public void setLastdevid(String lastdevid) {
    this.lastdevid = lastdevid;
  }

  public List<String> getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(List<String> transactionId) {
    this.transactionId = transactionId;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  @Override
  public String toString() {
    return "LoginResponse{" +
            "id='" + id + '\'' +
            ", password='" + password + '\'' +
            ", name='" + name + '\'' +
            ", email='" + email + '\'' +
            ", phoneNumber='" + phoneNumber + '\'' +
            ", isConfirmed=" + isConfirmed +
            ", isEnabled=" + isEnabled +
            ", isDeleted=" + isDeleted +
            ", childId=" + childId +
            ", lastdevid='" + lastdevid + '\'' +
            ", transactionId=" + transactionId +
            ", address=" + address +
            ", city=" + city +
            ", country=" + country +
            ", state=" + state +
            ", token='" + token + '\'' +
            '}';
  }
}