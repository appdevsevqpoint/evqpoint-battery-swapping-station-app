package com.evqpoint.evqpointbatteryswappingstationapp.network.networktests;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.ChargingControl;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.StartResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.StationControlAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.StopResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionResponse;
import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
* Transaction Response: TransactionResponse{id='5dd77a16fc567d451112a051', deviceID='BNG-001-06', startTime=1574402556,
*  transactionID='d691f802-24cd-4a30-9ef2-b15d78d7f978', amount=0.0, tax=0.18, totalAmount=0.0, energyConsumed=0.0, endTime=1574402583}
*
* userid = "82d0a2e3-fb4f-4dff-aa1f-38f892e46d6b"
* password = XXBCArI
* email = Naseem@evqpoint.com
*/

public class InvoiceTestActivity extends AppCompatActivity {

    boolean status;
    String transID;
    String userid = "82d0a2e3-fb4f-4dff-aa1f-38f892e46d6b";
    String transactVal;

    //RETROFIT
    public static final String BASE_URL = "https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    TextView tvStartRequest;
    TextView tvStartResponse;

    TextView tvTransactionRequest;
    TextView tvTransactionResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_test);

        status = false;
        transactVal = "";

        //RETROFIT
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        //UI
        tvStartRequest = findViewById(R.id.tvStartStationParameters);
        tvStartResponse = findViewById(R.id.tvStartStationResponse);
        tvTransactionRequest = findViewById(R.id.tvTransactionRequestParameters);
        tvTransactionResponse = findViewById(R.id.tvTransactionRestResponse);

        //Control
        Button btnPower = findViewById(R.id.btnPower);
        btnPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(status){
                    status = false; //Stopping
                    ChargingControl cControl = new ChargingControl("BNG-001-06",status, userid);
                    stopStation(cControl);
                    btnPower.setText("Start Station");
                    Snackbar sb = Snackbar.make(view, "Stopping Station", Snackbar.LENGTH_LONG)
                            .setAction("Action", null);
                    sb.show();
                }else{
                    status = true; //Starting
                    ChargingControl cControl = new ChargingControl("BNG-001-06",status, userid);
                    tvStartRequest.setText("Start Station Parameters: "+"\nBNG-001-06"+" Status: "+status+"\nUserid: "+userid);
                    startStation(cControl);
                    btnPower.setText("Stop Station");
                    Snackbar sb = Snackbar.make(view, "Starting Station", Snackbar.LENGTH_LONG)
                            .setAction("Action", null);
                    sb.show();
                }
            }
        });

        Button btnTransaction = findViewById(R.id.btnTransaction);
        btnTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TransactionRequest tRequest = new TransactionRequest(transID);
                getTransaction(tRequest);
                Snackbar sb = Snackbar.make(view, "Fetched Transaction Details.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null);
                sb.show();
            }
        });
    }

    //Sets TransactionID
    private void startStation(ChargingControl chargeControl) {
        StationControlAPI chargeStationService = retrofit.create(StationControlAPI.class);
        Call<StartResponse> call = chargeStationService.instructDeviceStart(chargeControl);

        Log.e("Status","Start Station");

        call.enqueue(new Callback<StartResponse>() {
            @Override
            public void onResponse(Call<StartResponse> call, Response<StartResponse> response) {
                StartResponse startResponse = response.body();
                transID = startResponse.getTransactionID();
                tvTransactionRequest.setText("Transaction ID: " + transID);
                tvStartResponse.setText("Started\n"+startResponse.toString());
                Log.e("TransactionID",transID);
            }
            @Override
            public void onFailure(Call<StartResponse> call, Throwable throwable) {
                Log.e("Response for TransID","Failed");
            }
        });
    }

    private void stopStation(ChargingControl chargeControl) {
        StationControlAPI chargeStationService = retrofit.create(StationControlAPI.class);
        Call<StopResponse> call = chargeStationService.instructDeviceStop(chargeControl);

        call.enqueue(new Callback<StopResponse>() {
            @Override
            public void onResponse(Call<StopResponse> call, Response<StopResponse> response) {
                StopResponse stopResponse = response.body();
                tvStartResponse.setText("Stopped\n"+stopResponse.toString());
            }
            @Override
            public void onFailure(Call<StopResponse> call, Throwable throwable) {
                Log.e("Session Response","Failed");
            }
        });
    }

    private void getTransaction(TransactionRequest transactionRequest){
        TransactionAPI transactionService = retrofit.create(TransactionAPI.class);
        Call<TransactionResponse> call = transactionService.getTransactionResponse(transactionRequest);

        tvTransactionRequest.setText(transactionRequest.toString());

        call.enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                TransactionResponse transactionResponse = response.body();

                if(transactionResponse!=null) {
                    transactVal = transactionResponse.toString();
                }
                Log.e("Transaction Response", transactVal);

                tvTransactionResponse.setText(transactVal);
            }
            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable throwable) {
                Log.e("Transaction Response","Failed");
            }
        });
    }

}
