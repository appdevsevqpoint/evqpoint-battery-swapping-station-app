package com.evqpoint.evqpointbatteryswappingstationapp.network.transaction;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface TransactionAPI {

    @POST("getTransaction")
    Call<TransactionResponse> getTransactionResponse(@Body TransactionRequest body);

    @POST("updateTransaction")
    Call<TransactionResponse> updateTransaction(@Body TransactionPaymentStatus body);
    //Call<UpdateTransactionResponse> updateTransaction(@Body TransactionPaymentStatus body);

    @POST("getAllTransaction")
    Call<TransactionsResponse> getAllTransactions(@Body TransactionsRequest body);


}
