package com.evqpoint.evqpointbatteryswappingstationapp.network.getstations;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

//Alternative to Coordinates parameter for getChargningStationsList API
public class StationLocation {

  @SerializedName("location")
  @Expose
  private List<Double> location = null;

  public List<Double> getLocation() {
    return location;
  }

  public void setLocation(List<Double> location) {
    this.location = location;
  }

  public StationLocation(List<Double> location) {
    this.location = location;
  }
}

/**
 *
 * {
 *     "location":[0,0]
 * }
 *
 */
