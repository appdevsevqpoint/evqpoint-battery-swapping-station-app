package com.evqpoint.evqpointbatteryswappingstationapp.network.vehicles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.evqpoint.evqpointbatteryswappingstationapp.activities.vehicles.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class GetAllVehiclesResponse {

    @SerializedName("result")
    @Expose
    private List<VehicleResponse> vehicleResponse = null;

    public List<VehicleResponse> getVehicleResponse() {
        return vehicleResponse;
    }

    public void setVehicleResponse(List<VehicleResponse> vehicleResponse) {
        this.vehicleResponse = vehicleResponse;
    }

    public int getSize(){
        return this.vehicleResponse.size();
    }

    public List<Vehicle> fetchVehicles(){
        List<Vehicle> vehicleList = new ArrayList<>();

        for(int k=0; k<vehicleResponse.size(); k++){
            vehicleList.add(vehicleResponse.get(k).convertToVehicle());
        }

        return vehicleList;
    }
}
