package com.evqpoint.evqpointbatteryswappingstationapp.network.addstation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AddChargingStation {

  @SerializedName("name")
  @Expose
  private String name;

  @SerializedName("address")
  @Expose
  private String address;

  @SerializedName("location")
  @Expose
  private List<Double> location = null;

  @SerializedName("image")
  @Expose
  private String image;

  public String getName() {
    return name;
  }


  public void setName(String name) {
    this.name = name;
  }


  public List<Double> getLocation() {
    return location;
  }


  public void setLocation(List<Double> location) {
    this.location = location;
  }


  public String getAddress() {
    return address;
  }


  public void setAddress(String address) {
    this.address = address;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

    /*
    public AddChargingStation(String name, String address, List<Coordinates> location, String image) {
        this.name = name;
        this.address = address;
        this.location = location;
        this.image = image;
    }*/

  public AddChargingStation(String stationName, String stationAddress, List<Double> coordinates, String stationImge) {
    this.name = stationName;
    this.address = stationAddress;

    List<Double> locs = new ArrayList<>();
    locs.add(coordinates.get(0));
    locs.add(coordinates.get(1));
    this.location = locs;

    this.image = stationImge;
  }

  public AddChargingStation(ChargingStation inChargingStation) {
    this.name = inChargingStation.getName();

    Double lat = inChargingStation.getLocation().get(0);
    Double lon = inChargingStation.getLocation().get(1);
    List<Double> tempLocation = new ArrayList<Double>();
    tempLocation.add(lat);
    tempLocation.add(lon);

    this.location = tempLocation;
    this.address = inChargingStation.getAddress();
    this.image = inChargingStation.getImage();
  }

  @Override
  public String toString() {
    return "AddChargingStation{" +
            "name='" + name + '\'' +
            ", address='" + address + '\'' +
            ", location=" + location +
            ", image='" + image + '\'' +
            '}';
  }
}