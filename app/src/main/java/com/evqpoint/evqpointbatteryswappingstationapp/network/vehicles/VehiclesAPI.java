package com.evqpoint.evqpointbatteryswappingstationapp.network.vehicles;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface VehiclesAPI {

    @POST("addVehicle")
    Call<AddVehicleResponse> addVehicle(@Body AddVehicleRequest body);

    @POST("updateVehicle")
    Call<AddVehicleResponse> updateVehicle(@Body AddVehicleRequest body);

    @POST("getAllVehiclebyUser")
    Call<GetAllVehiclesResponse> getAllVehicles(@Body GetAllVehiclesRequest body);

}
