package com.evqpoint.evqpointbatteryswappingstationapp.network.addstation;

import com.evqpoint.evqpointbatteryswappingstationapp.network.getstations.ChargeStationsResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface NewChargePointService {

    @POST("user/addChargingStation")
    Call<ChargingStation> setNewChargePoint(@Body AddChargingStation body);

    @POST("chargingStationsList")
    Call<ChargeStationsResponse> getChargingStations();
}