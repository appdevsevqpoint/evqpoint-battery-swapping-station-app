package com.evqpoint.evqpointbatteryswappingstationapp.network.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddNewUser {

  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("email")
  @Expose
  private String email;
  @SerializedName("phone_number")
  @Expose
  private String phoneNumber;

  public AddNewUser(String username, String useremail, String userphone) {
    this.name = username;
    this.email = useremail;
    this.phoneNumber = userphone;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

}