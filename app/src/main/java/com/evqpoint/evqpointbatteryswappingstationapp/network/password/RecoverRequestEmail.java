package com.evqpoint.evqpointbatteryswappingstationapp.network.password;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecoverRequestEmail {

  @SerializedName("email")
  @Expose
  private String email;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public RecoverRequestEmail(String email) {
    this.email = email;
  }

  @Override
  public String toString() {
    return "RecoveryResponse{" +
            "email='" + email + "'}"+
            '}';
  }

}