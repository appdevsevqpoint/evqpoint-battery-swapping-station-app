package com.evqpoint.evqpointbatteryswappingstationapp.network.signup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterNewUser {

  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("email_phone")
  @Expose
  private String emailPhone;

  public RegisterNewUser(String fullname, String emailPhone) {
    this.name = fullname;
    this.emailPhone = emailPhone;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmailPhone() {
    return emailPhone;
  }

  public void setEmailPhone(String emailPhone) {
    this.emailPhone = emailPhone;
  }

  @Override
  public String toString() {
    return "RegisterNewUser{" +
            "name='" + name + '\'' +
            ", emailPhone='" + emailPhone + '\'' +
            '}';
  }
}