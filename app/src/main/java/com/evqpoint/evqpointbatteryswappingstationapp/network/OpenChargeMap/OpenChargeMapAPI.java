package com.evqpoint.evqpointbatteryswappingstationapp.network.OpenChargeMap;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenChargeMapAPI {

    @GET("poi/")
    Call<List<OpenChargeMapResponse>> getOpenStations(@Query("output") String output,
                                                      @Query("countrycode") String countrycode,
                                                      @Query("maxresults") int maxresults,
                                                      @Query("compact") String compact,
                                                      @Query("verbose") String verbose);

}
