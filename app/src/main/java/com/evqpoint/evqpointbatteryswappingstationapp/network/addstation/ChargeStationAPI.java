package com.evqpoint.evqpointbatteryswappingstationapp.network.addstation;

import com.evqpoint.evqpointbatteryswappingstationapp.network.getstations.ChargeStationsResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.getstations.StationLocation;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ChargeStationAPI {

  @POST("addChargingStation")
  Call<ChargingStation> setNewChargePoint(@Header("authorization") String authToken, @Body AddChargingStation body);

    /*@POST("chargingStationsList")
    Call<ChargeStationsResponse> getChargingStations();*/

  @POST("chargingStationsList")
  Call<ChargeStationsResponse> getChargingStations(@Header("authorization") String authToken, @Body StationLocation loc);

  @POST("deleteChargingStation")
  Call<String> removeChargingStation(@Header("authorization") String authToken, @Body DeleteChargingStation deleteStation);

}