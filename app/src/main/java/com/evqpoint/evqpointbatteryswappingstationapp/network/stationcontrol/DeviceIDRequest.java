package com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceIDRequest {

    @SerializedName("deviceId")
    @Expose
    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceID(String deviceID) {
        this.deviceId = deviceID;
    }

    public DeviceIDRequest(String deviceID) {
        this.deviceId = deviceID;
    }

    @Override
    public String toString() {
        return "DeviceIDRequest{" +
                "deviceId='" + deviceId + '\'' +
                '}';
    }
}
