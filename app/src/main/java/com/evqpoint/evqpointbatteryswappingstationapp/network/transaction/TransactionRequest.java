package com.evqpoint.evqpointbatteryswappingstationapp.network.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionRequest {

    //"transactionID\": \"ef981db8-e9ac-4e37-b71a-99c290ff604d\"

    @SerializedName("transactionId")
    @Expose
    private String transactionID;

    public TransactionRequest(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    @Override
    public String toString() {
        return "TransactionRequest{" +
                "transactionID='" + transactionID + '\'' +
                '}';
    }
}
