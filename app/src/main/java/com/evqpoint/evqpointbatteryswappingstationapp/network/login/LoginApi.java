package com.evqpoint.evqpointbatteryswappingstationapp.network.login;

import com.evqpoint.evqpointbatteryswappingstationapp.network.signup.RegisterNewUser;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginApi {

    @POST("register")
    Call<NewUserResponse> registerNewUser(@Body RegisterNewUser body);

    @POST("login")
    Call<LoginResponse> loginUserEmail(@Body LoginUserEmail body);

    @POST("login")
    Call<LoginResponse> loginUserPhone(@Body LoginUserPhone body);

    /*
    @POST("updateUser")
    Call<UpdatedUserResponse> updateUser(@Body UpdateUserRequest body);
    */
}
