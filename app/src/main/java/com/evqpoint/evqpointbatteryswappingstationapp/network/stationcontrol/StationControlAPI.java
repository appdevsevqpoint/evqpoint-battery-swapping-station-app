package com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface StationControlAPI {
    @POST("deviceStatus")
    Call<DeviceStatusResponse> getDeviceStatus(@Body DeviceIDRequest body);
    //Call<DeviceStatusResponse> getDeviceStatus(@Header("authorization") String authToken, @Body String body);

    @POST("chargingdevice")
    Call<StartResponse> instructDeviceStart(@Body ChargingControl body);
    //Call<StartResponse> instructDevice(@Header("authorization") String authToken, @Body AddChargingStation body);

    @POST("chargingdevice")
    Call<StopResponse> instructDeviceStop(@Body ChargingControl body);
    //Call<StartResponse> instructDevice(@Header("authorization") String authToken, @Body AddChargingStation body);


    @POST("chargingStatus")
    Call<ChargingSessionResponse> getChargingStatus(@Body ChargingSessionID body);
    //Call<ChargingSessionResponse> getChargingStatus(@Header("authorization") String authToken, @Body AddChargingStation body);
}