package com.evqpoint.evqpointbatteryswappingstationapp.network.password;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecoverRequestPhone {

  @SerializedName("phone_number")
  @Expose
  private String phone;

  public String getPhone() {
    return phone;
  }

  public void setEmail(String email) {
    this.phone = email;
  }

  public RecoverRequestPhone(String phone) {
    this.phone = phone;
  }

  @Override
  public String toString() {
    return "RecoveryResponse{" +
            "phone='" + phone + "'}"+
            '}';
  }

}