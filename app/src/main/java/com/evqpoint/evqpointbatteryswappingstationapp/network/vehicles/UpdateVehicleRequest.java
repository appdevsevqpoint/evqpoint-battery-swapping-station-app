package com.evqpoint.evqpointbatteryswappingstationapp.network.vehicles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateVehicleRequest {

    @SerializedName("vehicleid")
    @Expose
    private String vehicleid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("vehicle_make")
    @Expose
    private String vehicleMake;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("vehicle_model")
    @Expose
    private String vehicleModel;
    @SerializedName("port_type")
    @Expose
    private String portType;
    @SerializedName("max_power")
    @Expose
    private Integer maxPower;
    @SerializedName("battery_type")
    @Expose
    private String batteryType;
    @SerializedName("battery_capacity")
    @Expose
    private String batteryCapacity;
    @SerializedName("battery_amp")
    @Expose
    private Integer batteryAmp;
    @SerializedName("battery_watt")
    @Expose
    private Integer batteryWatt;
    @SerializedName("battery_voltage")
    @Expose
    private Integer batteryVoltage;

    public String getVehicleid() {
        return vehicleid;
    }

    public void setVehicleid(String vehicleid) {
        this.vehicleid = vehicleid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getPortType() {
        return portType;
    }

    public void setPortType(String portType) {
        this.portType = portType;
    }

    public Integer getMaxPower() {
        return maxPower;
    }

    public void setMaxPower(Integer maxPower) {
        this.maxPower = maxPower;
    }

    public String getBatteryType() {
        return batteryType;
    }

    public void setBatteryType(String batteryType) {
        this.batteryType = batteryType;
    }

    public String getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(String batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Integer getBatteryAmp() {
        return batteryAmp;
    }

    public void setBatteryAmp(Integer batteryAmp) {
        this.batteryAmp = batteryAmp;
    }

    public Integer getBatteryWatt() {
        return batteryWatt;
    }

    public void setBatteryWatt(Integer batteryWatt) {
        this.batteryWatt = batteryWatt;
    }

    public Integer getBatteryVoltage() {
        return batteryVoltage;
    }

    public void setBatteryVoltage(Integer batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }

    public UpdateVehicleRequest(String vehicleid, String name, String vehicleMake, String vehicleType,
                                String vehicleModel, String portType, Integer maxPower,
                                String batteryType, String batteryCapacity, Integer batteryAmp,
                                Integer batteryWatt, Integer batteryVoltage) {
        this.vehicleid = vehicleid;
        this.name = name;
        this.vehicleMake = vehicleMake;
        this.vehicleType = vehicleType;
        this.vehicleModel = vehicleModel;
        this.portType = portType;
        this.maxPower = maxPower;
        this.batteryType = batteryType;
        this.batteryCapacity = batteryCapacity;
        this.batteryAmp = batteryAmp;
        this.batteryWatt = batteryWatt;
        this.batteryVoltage = batteryVoltage;
    }

    @Override
    public String toString() {
        return "UpdateVehicleRequest{" +
                "vehicleid='" + vehicleid + '\'' +
                ", name='" + name + '\'' +
                ", vehicleMake='" + vehicleMake + '\'' +
                ", vehicleType='" + vehicleType + '\'' +
                ", vehicleModel='" + vehicleModel + '\'' +
                ", portType='" + portType + '\'' +
                ", maxPower=" + maxPower +
                ", batteryType='" + batteryType + '\'' +
                ", batteryCapacity='" + batteryCapacity + '\'' +
                ", batteryAmp=" + batteryAmp +
                ", batteryWatt=" + batteryWatt +
                ", batteryVoltage=" + batteryVoltage +
                '}';
    }
}
