package com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceStatusResponse {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("parentDeviceStatus")
    @Expose
    private String parentDeviceStatus;
    @SerializedName("child_1_DeviceStatus")
    @Expose
    private String child1DeviceStatus;
    @SerializedName("child_2_DeviceStatus")
    @Expose
    private String child2DeviceStatus;
    @SerializedName("child_3_DeviceStatus")
    @Expose
    private String child3DeviceStatus;
    @SerializedName("timestamp")
    @Expose
    private Integer timestamp;

    @SerializedName("deviceStatus")
    @Expose
    private String deviceStatus;

    /**
     *
     * @param deviceStatus
     */
    public DeviceStatusResponse(String deviceStatus) {
        super();
        this.deviceStatus = deviceStatus;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    /**
     * @param child2DeviceStatus
     * @param parentDeviceStatus
     * @param child1DeviceStatus
     * @param id
     * @param deviceId
     * @param child3DeviceStatus
     * @param timestamp
     */
    public DeviceStatusResponse(String id, String deviceId, String parentDeviceStatus, String child1DeviceStatus, String child2DeviceStatus, String child3DeviceStatus, Integer timestamp) {
        super();
        this.id = id;
        this.deviceId = deviceId;
        this.parentDeviceStatus = parentDeviceStatus;
        this.child1DeviceStatus = child1DeviceStatus;
        this.child2DeviceStatus = child2DeviceStatus;
        this.child3DeviceStatus = child3DeviceStatus;
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getParentDeviceStatus() {
        return parentDeviceStatus;
    }

    public void setParentDeviceStatus(String parentDeviceStatus) {
        this.parentDeviceStatus = parentDeviceStatus;
    }

    public String getChild1DeviceStatus() {
        return child1DeviceStatus;
    }

    public void setChild1DeviceStatus(String child1DeviceStatus) {
        this.child1DeviceStatus = child1DeviceStatus;
    }

    public String getChild2DeviceStatus() {
        return child2DeviceStatus;
    }

    public void setChild2DeviceStatus(String child2DeviceStatus) {
        this.child2DeviceStatus = child2DeviceStatus;
    }

    public String getChild3DeviceStatus() {
        return child3DeviceStatus;
    }

    public void setChild3DeviceStatus(String child3DeviceStatus) {
        this.child3DeviceStatus = child3DeviceStatus;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "DeviceStatusResponse{" +
                "id='" + id + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", parentDeviceStatus='" + parentDeviceStatus + '\'' +
                ", child1DeviceStatus='" + child1DeviceStatus + '\'' +
                ", child2DeviceStatus='" + child2DeviceStatus + '\'' +
                ", child3DeviceStatus='" + child3DeviceStatus + '\'' +
                ", timestamp=" + timestamp +
                ", deviceStatus=" + deviceStatus +
                '}';
    }
}