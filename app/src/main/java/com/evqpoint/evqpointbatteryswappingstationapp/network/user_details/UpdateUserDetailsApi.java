package com.evqpoint.evqpointbatteryswappingstationapp.network.user_details;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UpdateUserDetailsApi {

    @POST("updateUser")
    Call<UpdatedUserResponse> updateUserEmail(@Body UpdateUserEmailRequest body);

    @POST("updateUser")
    Call<UpdatedUserResponse> updateUserPhone(@Body UpdateUserPhoneRequest body);

    @POST("updateUser")
    Call<UpdatedUserResponse> updateUserAddress(@Body UpdateUserAddressRequest body);

}
