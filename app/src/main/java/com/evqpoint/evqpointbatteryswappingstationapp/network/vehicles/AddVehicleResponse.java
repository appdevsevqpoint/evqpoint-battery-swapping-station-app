package com.evqpoint.evqpointbatteryswappingstationapp.network.vehicles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddVehicleResponse {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("vehicle_make")
    @Expose
    private String vehicleMake;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("vehicle_model")
    @Expose
    private String vehicleModel;
    @SerializedName("port_type")
    @Expose
    private String portType;
    @SerializedName("max_power")
    @Expose
    private Integer maxPower;
    @SerializedName("battery_type")
    @Expose
    private String batteryType;
    @SerializedName("battery_voltage")
    @Expose
    private Integer batteryVoltage;
    @SerializedName("battery_capacity")
    @Expose
    private String batteryCapacity;
    @SerializedName("battery_amp")
    @Expose
    private Integer batteryAmp;
    @SerializedName("battery_watt")
    @Expose
    private Integer batteryWatt;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getPortType() {
        return portType;
    }

    public void setPortType(String portType) {
        this.portType = portType;
    }

    public Integer getMaxPower() {
        return maxPower;
    }

    public void setMaxPower(Integer maxPower) {
        this.maxPower = maxPower;
    }

    public String getBatteryType() {
        return batteryType;
    }

    public void setBatteryType(String batteryType) {
        this.batteryType = batteryType;
    }

    public Integer getBatteryVoltage() {
        return batteryVoltage;
    }

    public void setBatteryVoltage(Integer batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }

    public String getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(String batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Integer getBatteryAmp() {
        return batteryAmp;
    }

    public void setBatteryAmp(Integer batteryAmp) {
        this.batteryAmp = batteryAmp;
    }

    public Integer getBatteryWatt() {
        return batteryWatt;
    }

    public void setBatteryWatt(Integer batteryWatt) {
        this.batteryWatt = batteryWatt;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public AddVehicleResponse(String id, String userid, String name, String vehicleMake, String vehicleType,
                              String vehicleModel, String portType, Integer maxPower,
                              String batteryType, Integer batteryVoltage, String batteryCapacity,
                              Integer batteryAmp, Integer batteryWatt, Boolean isDeleted) {
        this.id = id;
        this.userid = userid;
        this.name = name;
        this.vehicleMake = vehicleMake;
        this.vehicleType = vehicleType;
        this.vehicleModel = vehicleModel;
        this.portType = portType;
        this.maxPower = maxPower;
        this.batteryType = batteryType;
        this.batteryVoltage = batteryVoltage;
        this.batteryCapacity = batteryCapacity;
        this.batteryAmp = batteryAmp;
        this.batteryWatt = batteryWatt;
        this.isDeleted = isDeleted;
    }

    @Override
    public String toString() {
        return "AddVehicleResponse{" +
                "id='" + id + '\'' +
                ", userid='" + userid + '\'' +
                ", name='" + name + '\'' +
                ", vehicleMake='" + vehicleMake + '\'' +
                ", vehicleType='" + vehicleType + '\'' +
                ", vehicleModel='" + vehicleModel + '\'' +
                ", portType='" + portType + '\'' +
                ", maxPower=" + maxPower +
                ", batteryType='" + batteryType + '\'' +
                ", batteryVoltage=" + batteryVoltage +
                ", batteryCapacity='" + batteryCapacity + '\'' +
                ", batteryAmp=" + batteryAmp +
                ", batteryWatt=" + batteryWatt +
                ", isDeleted=" + isDeleted +
                '}';
    }
}