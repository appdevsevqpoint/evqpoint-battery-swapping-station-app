package com.evqpoint.evqpointbatteryswappingstationapp.network.password;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangePasswordByEmailRequest {

    @SerializedName("oldpassword")
    @Expose
    private String oldpassword;
    @SerializedName("newpassword")
    @Expose
    private String newpassword;
    @SerializedName("email")
    @Expose
    private String email;

    /**
     * @param email
     * @param oldpassword
     * @param newpassword
     */
    public ChangePasswordByEmailRequest(String oldpassword, String newpassword, String email) {
        super();
        this.oldpassword = oldpassword;
        this.newpassword = newpassword;
        this.email = email;
    }

    public String getOldpassword() {
        return oldpassword;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "ChangePasswordByEmailRequest{" +
                "oldpassword='" + oldpassword + '\'' +
                ", newpassword='" + newpassword + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}