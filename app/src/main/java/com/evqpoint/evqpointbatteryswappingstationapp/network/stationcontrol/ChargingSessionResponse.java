package com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChargingSessionResponse {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("childId")
    @Expose
    private Integer childId;
    @SerializedName("soc")
    @Expose
    private Integer soc;
    @SerializedName("batteryCurrent")
    @Expose
    private Double batteryCurrent;
    @SerializedName("batteryVoltage")
    @Expose
    private Double batteryVoltage;
    @SerializedName("availableCapacity")
    @Expose
    private Double availableCapacity;
    @SerializedName("availableEnergy")
    @Expose
    private Double availableEnergy;
    @SerializedName("bmsTemp")
    @Expose
    private Integer bmsTemp;
    @SerializedName("chargerCurrent")
    @Expose
    private Double chargerCurrent;
    @SerializedName("chargerVoltage")
    @Expose
    private Double chargerVoltage;
    @SerializedName("chargerEnergy")
    @Expose
    private Double chargerEnergy;
    @SerializedName("chargerPower")
    @Expose
    private Double chargerPower;
    @SerializedName("binMSB")
    @Expose
    private Integer binMSB;
    @SerializedName("binLSB")
    @Expose
    private Integer binLSB;
    @SerializedName("chargingDuration")
    @Expose
    private Double chargingDuration;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    @SerializedName("timestamp")
    @Expose
    private Integer timestamp;

    /**
     * @param id
     * @param deviceId
     * @param childId
     * @param soc
     * @param batteryCurrent
     * @param batteryVoltage
     * @param availableCapacity
     * @param availableEnergy
     * @param bmsTemp
     * @param chargerCurrent
     * @param chargerVoltage
     * @param chargerEnergy
     * @param chargerPower
     * @param binMSB
     * @param binLSB
     * @param chargingDuration
     * @param transactionId
     * @param timestamp
     */
    public ChargingSessionResponse(String id, String deviceId, Integer childId, Integer soc,
                                   Double batteryCurrent, Double batteryVoltage,
                                   Double availableCapacity, Double availableEnergy,
                                   Integer bmsTemp, Double chargerCurrent,
                                   Double chargerVoltage, Double chargerEnergy,
                                   Double chargerPower, Integer binMSB, Integer binLSB,
                                   Double chargingDuration, String transactionId,
                                   Integer timestamp) {
    super();
    this.id = id;
    this.deviceId = deviceId;
    this.childId = childId;
    this.soc = soc;
    this.batteryCurrent = batteryCurrent;
    this.batteryVoltage = batteryVoltage;
    this.availableCapacity = availableCapacity;
    this.availableEnergy = availableEnergy;
    this.bmsTemp = bmsTemp;
    this.chargerCurrent = chargerCurrent;
    this.chargerVoltage = chargerVoltage;
    this.chargerEnergy = chargerEnergy;
    this.chargerPower = chargerPower;
    this.binMSB = binMSB;
    this.binLSB = binLSB;
    this.chargingDuration = chargingDuration;
    this.transactionId = transactionId;
    this.timestamp = timestamp;
    }

    public String getId() {
    return id;
    }

    public void setId(String id) {
    this.id = id;
    }

    public String getDeviceId() {
    return deviceId;
    }

    public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
    }

    public Integer getChildId() {
    return childId;
    }

    public void setChildId(Integer childId) {
    this.childId = childId;
    }

    public Integer getSoc() {
    return soc;
    }

    public void setSoc(Integer soc) {
    this.soc = soc;
    }

    public Double getBatteryVoltage() {
    return batteryVoltage;
    }

    public void setBatteryVoltage(Double batteryVoltage) {
    this.batteryVoltage = batteryVoltage;
    }

    public Double getAvailableCapacity() {
    return availableCapacity;
    }

    public void setAvailableCapacity(Double availableCapacity) {
    this.availableCapacity = availableCapacity;
    }

    public Double getAvailableEnergy() {
    return availableEnergy;
    }

    public void setAvailableEnergy(Double availableEnergy) {
    this.availableEnergy = availableEnergy;
    }

    public Integer getBmsTemp() {
    return bmsTemp;
    }

    public void setBmsTemp(Integer bmsTemp) {
    this.bmsTemp = bmsTemp;
    }

    public Double getChargerVoltage() {
    return chargerVoltage;
    }

    public void setChargerVoltage(Double chargerVoltage) {
    this.chargerVoltage = chargerVoltage;
    }

    public Double getChargerEnergy() {
    return chargerEnergy;
    }

    public void setChargerEnergy(Double chargerEnergy) {
    this.chargerEnergy = chargerEnergy;
    }

    public Integer getBinMSB() {
    return binMSB;
    }

    public void setBinMSB(Integer binMSB) {
    this.binMSB = binMSB;
    }

    public Integer getBinLSB() {
    return binLSB;
    }

    public void setBinLSB(Integer binLSB) {
    this.binLSB = binLSB;
    }

    public Double getChargingDuration() {
    return chargingDuration;
    }

    public void setChargingDuration(Double chargingDuration) {
    this.chargingDuration = chargingDuration;
    }

    public String getTransactionId() {
    return transactionId;
    }

    public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
    }

    public Integer getTimestamp() {
    return timestamp;
    }

    public Double getBatteryCurrent() {
        return batteryCurrent;
    }

    public void setBatteryCurrent(Double batteryCurrent) {
        this.batteryCurrent = batteryCurrent;
    }

    public Double getChargerCurrent() {
        return chargerCurrent;
    }

    public void setChargerCurrent(Double chargerCurrent) {
        this.chargerCurrent = chargerCurrent;
    }

    public Double getChargerPower() {
        return chargerPower;
    }

    public void setChargerPower(Double chargerPower) {
        this.chargerPower = chargerPower;
    }

    public void setTimestamp(Integer timestamp) {
    this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "ChargingSessionResponse{" +
                "id='" + id + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", childId=" + childId +
                ", soc=" + soc +
                ", batteryCurrent=" + batteryCurrent +
                ", batteryVoltage=" + batteryVoltage +
                ", availableCapacity=" + availableCapacity +
                ", availableEnergy=" + availableEnergy +
                ", bmsTemp=" + bmsTemp +
                ", chargerCurrent=" + chargerCurrent +
                ", chargerVoltage=" + chargerVoltage +
                ", chargerEnergy=" + chargerEnergy +
                ", chargerPower=" + chargerPower +
                ", binMSB=" + binMSB +
                ", binLSB=" + binLSB +
                ", chargingDuration=" + chargingDuration +
                ", transactionId='" + transactionId + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
