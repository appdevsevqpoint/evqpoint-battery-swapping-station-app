package com.evqpoint.evqpointbatteryswappingstationapp.network.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginUserPhone {

  @SerializedName("login_type")//"phone" or "email"
  @Expose
  private String loginType;
  @SerializedName("password")
  @Expose
  private String password;
  //TODO
  @SerializedName("phone_number")
  @Expose
  private String identifier;

  public LoginUserPhone(String type, String identifier, String userpass) {
    this.loginType = type;
    this.password = userpass;
    this.identifier = identifier;
  }

  public String getLoginType() {
    return loginType;
  }

  public void setLoginType(String loginType) {
    this.loginType = loginType;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String phoneNumber) {
    this.identifier = phoneNumber;
  }

  @Override
  public String toString() {
    return "LoginUserPhone{" +
            "loginType='" + loginType + '\'' +
            ", password='" + password + '\'' +
            ", identifier='" + identifier + '\'' +
            '}';
  }
}