package com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChargingControl {

  @SerializedName("devid")
  @Expose
  private String devid;
  @SerializedName("authStatus")
  @Expose
  private Boolean authStatus; //TRUE = ON; FALSE = OFF;
  @SerializedName("userid")
  @Expose
  private String userid;

  public String getDevid() {
    return devid;
  }

  public void setDevid(String devid) {
    this.devid = devid;
  }

  public Boolean getAuthStatus() {
    return authStatus;
  }

  public void setAuthStatus(Boolean authStatus) {
    this.authStatus = authStatus;
  }

  public String getUserid() {
    return userid;
  }

  public void setUserid(String userid) {
    this.userid = userid;
  }


  public ChargingControl(String devid, Boolean authStatus, String userid) {
    this.devid = devid;
    this.authStatus = authStatus;
    this.userid = userid;
  }
}
