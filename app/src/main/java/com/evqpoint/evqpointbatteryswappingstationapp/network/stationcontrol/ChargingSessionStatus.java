package com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChargingSessionStatus {

  @SerializedName("status")
  @Expose
  private String status;

  @SerializedName("transactionID")
  @Expose
  private String transactionID;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getTransactionID() {
    return transactionID;
  }

  public void setTransactionID(String status) {
    this.status = status;
  }

}
