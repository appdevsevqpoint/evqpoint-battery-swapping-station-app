package com.evqpoint.evqpointbatteryswappingstationapp.network.networktests;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.ChargingSessionID;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.ChargingSessionResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.StationControlAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class StatusTestActivity extends AppCompatActivity {

    //UI
    TextView tvRequest;
    TextView tvResponse;
    Button btnRequest;

    //Retrofit
    public static final String BASE_URL = "https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;
    ChargingSessionResponse chargingSessionResponse;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.test_ui_device_charging_status);
        tvRequest = findViewById(R.id.tvRequestParameters);
        tvResponse = findViewById(R.id.tvRestResponse);
        btnRequest = findViewById(R.id.btnRequest);

        String devid = "BNG-001-06";
        String transid = "b2942d34-fd96-4b91-9d1a-390b1579227d";

        tvRequest.setText("devid = "+devid+"\ntransid = "+transid);

        ChargingSessionID sessionID = new ChargingSessionID(devid,transid);


        btnRequest.setOnClickListener(v ->{
            updateSessionStatus(sessionID);
            tvResponse.setText(chargingSessionResponse.toString());
        });
    }

    private void updateSessionStatus(ChargingSessionID inSessionID){
        StationControlAPI chargeStationService = retrofit.create(StationControlAPI.class);
        Call<ChargingSessionResponse> call = chargeStationService.getChargingStatus(inSessionID);

        //Log.e("Status","Retrofit session status HTTP Call");

        call.enqueue(new Callback<ChargingSessionResponse>() {
            @Override
            public void onResponse(Call<ChargingSessionResponse> call, Response<ChargingSessionResponse> response) {

                //Log.e("Status","Retrofit session status HTTP Calling Responded");

                ChargingSessionResponse sessionResponse = response.body();

                if(sessionResponse!=null) {
                    //Log.e("Session Status Response", sessionResponse.getTransactionID());
                    Log.e("Session Response", sessionResponse.toString());
                    chargingSessionResponse = sessionResponse;
                }
            }

            @Override
            public void onFailure(Call<ChargingSessionResponse> call, Throwable throwable) {
                Log.e("Status","Retrofit session status HTTP Calling Failed");
            }
        });
    }
}
