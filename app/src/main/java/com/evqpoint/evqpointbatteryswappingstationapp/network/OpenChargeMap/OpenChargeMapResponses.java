package com.evqpoint.evqpointbatteryswappingstationapp.network.OpenChargeMap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OpenChargeMapResponses {

    List<OpenChargeMapResponse> openChargeMapResponses;

    public List<OpenChargeMapResponse> getOpenChargeMapResponses() {
        return openChargeMapResponses;
    }

    public void setOpenChargeMapResponses(List<OpenChargeMapResponse> openChargeMapResponses) {
        this.openChargeMapResponses = openChargeMapResponses;
    }

    public OpenChargeMapResponses(List<OpenChargeMapResponse> openChargeMapResponses) {
        this.openChargeMapResponses = openChargeMapResponses;
    }
}
