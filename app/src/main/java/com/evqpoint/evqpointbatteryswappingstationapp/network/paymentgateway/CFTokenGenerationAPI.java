package com.evqpoint.evqpointbatteryswappingstationapp.network.paymentgateway;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface CFTokenGenerationAPI {

  //BASE URL = https://api.evqpoint.com/user
  @POST("createOrder")
  Call<CFToken> setupOrder(@Body CFOrder body);

}
