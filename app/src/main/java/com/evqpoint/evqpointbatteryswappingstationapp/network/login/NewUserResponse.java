package com.evqpoint.evqpointbatteryswappingstationapp.network.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewUserResponse {

    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("isConfirmed")
    @Expose
    private Boolean isConfirmed;
    @SerializedName("isEnabled")
    @Expose
    private Boolean isEnabled;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("profile_completion")
    @Expose
    private String profileCompletion;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getIsConfirmed() {
        return isConfirmed;
    }

    public void setIsConfirmed(Boolean isConfirmed) {
        this.isConfirmed = isConfirmed;
    }

    public Boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getProfileCompletion() {
        return profileCompletion;
    }

    public void setProfileCompletion(String profileCompletion) {
        this.profileCompletion = profileCompletion;
    }

    @Override
    public String toString() {
        return "NewUserResponse{" +
                "password='" + password + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", isConfirmed=" + isConfirmed +
                ", isEnabled=" + isEnabled +
                ", isDeleted=" + isDeleted +
                ", profileCompletion='" + profileCompletion + '\'' +
                '}';
    }
}