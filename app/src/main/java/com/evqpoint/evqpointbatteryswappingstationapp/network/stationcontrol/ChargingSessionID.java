package com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChargingSessionID {

    @SerializedName("deviceId")
    @Expose
    private String deviceID;
    @SerializedName("transactionId")
    @Expose
    private String transactionID;

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public ChargingSessionID(String deviceID, String transactionID) {
        this.deviceID = deviceID;
        this.transactionID = transactionID;
    }

    @Override
    public String toString() {
        return "ChargingSessionID{" +
                "deviceID='" + deviceID + '\'' +
                ", transactionID='" + transactionID + '\'' +
                '}';
    }
}