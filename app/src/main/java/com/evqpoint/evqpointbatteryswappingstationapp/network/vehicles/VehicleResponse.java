package com.evqpoint.evqpointbatteryswappingstationapp.network.vehicles;

import com.evqpoint.evqpointbatteryswappingstationapp.activities.vehicles.Vehicle;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.evqpoint.evqpointbatteryswappingstationapp.activities.vehicles.Vehicle;

public class VehicleResponse {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("name")
    @Expose
    private String nickname;
    @SerializedName("vehicle_make")
    @Expose
    private String vehicleMake;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("vehicle_model")
    @Expose
    private String vehicleModel;
    @SerializedName("port_type")
    @Expose
    private String portType;
    @SerializedName("max_power")
    @Expose
    private Integer maxPower;
    @SerializedName("battery_type")
    @Expose
    private String batteryType;
    @SerializedName("battery_voltage")
    @Expose
    private Integer batteryVoltage;
    @SerializedName("battery_capacity")
    @Expose
    private String batteryCapacity;
    @SerializedName("battery_amp")
    @Expose
    private Integer batteryAmp;
    @SerializedName("battery_watt")
    @Expose
    private Integer batteryWatt;
    @SerializedName("isDeleted")
    @Expose
    private Boolean isDeleted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getPortType() {
        return portType;
    }

    public void setPortType(String portType) {
        this.portType = portType;
    }

    public Integer getMaxPower() {
        return maxPower;
    }

    public void setMaxPower(Integer maxPower) {
        this.maxPower = maxPower;
    }

    public String getBatteryType() {
        return batteryType;
    }

    public void setBatteryType(String batteryType) {
        this.batteryType = batteryType;
    }

    public Integer getBatteryVoltage() {
        return batteryVoltage;
    }

    public void setBatteryVoltage(Integer batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }

    public String getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(String batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Integer getBatteryAmp() {
        return batteryAmp;
    }

    public void setBatteryAmp(Integer batteryAmp) {
        this.batteryAmp = batteryAmp;
    }

    public Integer getBatteryWatt() {
        return batteryWatt;
    }

    public void setBatteryWatt(Integer batteryWatt) {
        this.batteryWatt = batteryWatt;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Vehicle convertToVehicle() {
        Vehicle vehi = new Vehicle(
            getNickname(), getVehicleMake(), getVehicleType(), getVehicleModel(), getPortType()
        );

        return vehi;
    }
}
