package com.evqpoint.evqpointbatteryswappingstationapp.network.password;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PasswordApi {

    @POST("forgotpassword")
    Call<String> requestPasswordByEmail(@Body RecoverRequestEmail body);

    @POST("forgotpassword")
    Call<String> requestPasswordByPhone(@Body RecoverRequestPhone body);

    @POST("changepassword")
    Call<String> requestPasswordChangeByPhone(@Body ChangePasswordByPhoneRequest body);

    @POST("changepassword")
    Call<String> requestPasswordChangeByEmail(@Body ChangePasswordByEmailRequest body);

}
