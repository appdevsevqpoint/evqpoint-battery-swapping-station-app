package com.evqpoint.evqpointbatteryswappingstationapp.network.OpenChargeMap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddressInfo {

@SerializedName("ID")
@Expose
private Integer iD;
@SerializedName("Title")
@Expose
private String title;
@SerializedName("AddressLine1")
@Expose
private String addressLine1;
@SerializedName("Town")
@Expose
private String town;
@SerializedName("StateOrProvince")
@Expose
private String stateOrProvince;
@SerializedName("Postcode")
@Expose
private String postcode;
@SerializedName("CountryID")
@Expose
private Integer countryID;
@SerializedName("Latitude")
@Expose
private Double latitude;
@SerializedName("Longitude")
@Expose
private Double longitude;
@SerializedName("ContactTelephone1")
@Expose
private String contactTelephone1;
@SerializedName("ContactEmail")
@Expose
private String contactEmail;
@SerializedName("RelatedURL")
@Expose
private String relatedURL;
@SerializedName("DistanceUnit")
@Expose
private Integer distanceUnit;

public Integer getID() {
return iD;
}

public void setID(Integer iD) {
this.iD = iD;
}

public String getTitle() {
return title;
}

public void setTitle(String title) {
this.title = title;
}

public String getAddressLine1() {
return addressLine1;
}

public void setAddressLine1(String addressLine1) {
this.addressLine1 = addressLine1;
}

public String getTown() {
return town;
}

public void setTown(String town) {
this.town = town;
}

public String getStateOrProvince() {
return stateOrProvince;
}

public void setStateOrProvince(String stateOrProvince) {
this.stateOrProvince = stateOrProvince;
}

public String getPostcode() {
return postcode;
}

public void setPostcode(String postcode) {
this.postcode = postcode;
}

public Integer getCountryID() {
return countryID;
}

public void setCountryID(Integer countryID) {
this.countryID = countryID;
}

public Double getLatitude() {
return latitude;
}

public void setLatitude(Double latitude) {
this.latitude = latitude;
}

public Double getLongitude() {
return longitude;
}

public void setLongitude(Double longitude) {
this.longitude = longitude;
}

public String getContactTelephone1() {
return contactTelephone1;
}

public void setContactTelephone1(String contactTelephone1) {
this.contactTelephone1 = contactTelephone1;
}

public String getContactEmail() {
return contactEmail;
}

public void setContactEmail(String contactEmail) {
this.contactEmail = contactEmail;
}

public String getRelatedURL() {
return relatedURL;
}

public void setRelatedURL(String relatedURL) {
this.relatedURL = relatedURL;
}

public Integer getDistanceUnit() {
return distanceUnit;
}

public void setDistanceUnit(Integer distanceUnit) {
this.distanceUnit = distanceUnit;
}

}

