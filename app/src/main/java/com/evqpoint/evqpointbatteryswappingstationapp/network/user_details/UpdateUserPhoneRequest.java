package com.evqpoint.evqpointbatteryswappingstationapp.network.user_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

public class UpdateUserPhoneRequest {

    @NotNull
    @SerializedName("userid")
    @Expose
    private String userid;
    @NotNull
    @SerializedName("email")
    @Expose
    private String email;
    @NotNull
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public UpdateUserPhoneRequest(@NotNull String userid, @NotNull String email, @NotNull String phone) {
        this.userid = userid;
        this.email = email;
        this.phoneNumber = phone;
    }

    @Override
    public String toString() {
        return "UpdateUserPhoneRequest{" +
                "userid='" + userid + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
