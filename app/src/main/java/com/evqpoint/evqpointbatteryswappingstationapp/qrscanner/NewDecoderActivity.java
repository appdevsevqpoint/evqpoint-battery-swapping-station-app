package com.evqpoint.evqpointbatteryswappingstationapp.qrscanner;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.control.UserControlActivityPre;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.mapactivity.MultiStationsMapActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionAPI;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.transaction.TransactionResponse;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class NewDecoderActivity extends AppCompatActivity
        implements ActivityCompat.OnRequestPermissionsResultCallback, QRCodeReaderView.OnQRCodeReadListener {

  //RETROFIT
  public static final String BASE_URL = "https://api.evqpoint.com/user/";
  private static Retrofit retrofit = null;

  //Permissions
  private static final int MY_PERMISSION_REQUEST_CAMERA = 0;

  //Old Preferences
  SharedPreferences pref;
  SharedPreferences.Editor editor;

  boolean lamp;
  private ViewGroup mainLayout;

  private TextView resultTextView;
  private QRCodeReaderView qrCodeReaderView;
  private PointsOverlayView pointsOverlayView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_decoder);

    mainLayout = findViewById(R.id.main_layout);
    Log.e("Status","New Decoder Activity");

    //TODO
    //On Going Session

    pref = this.getSharedPreferences("LoginPref", 0);
    Log.e("Status","Checking old session.");
    String transactionID ="0";
    transactionID = pref.getString("transID", "0");
    Log.e("Old TID",transactionID);

    if(pref.getString("transID", "0") != "0") {
      Log.e("Status","Found On Going Session.");

      transactionID = pref.getString("transID", "0");
      checkTransactionID(transactionID);

    }

    /*
    qrCodeReaderView = findViewById(R.id.qrdecoderview);
    qrCodeReaderView.setQRDecodingEnabled(true);
    */

    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            == PackageManager.PERMISSION_GRANTED) {
      initQRCodeReaderView();
    } else {
      requestCameraPermission();
    }

    lamp = false;

  }

  @Override protected void onResume() {
    super.onResume();

    if (qrCodeReaderView != null) {
      qrCodeReaderView.startCamera();
    }
  }

  @Override protected void onPause() {
    super.onPause();

    if (qrCodeReaderView != null) {
      qrCodeReaderView.stopCamera();
    }
  }

  @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                                   @NonNull int[] grantResults) {
    if (requestCode != MY_PERMISSION_REQUEST_CAMERA) {
      return;
    }

    if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
      //todo
      /*Snackbar sb = Snackbar.make(mainLayout,
              "Camera permission was granted.",
              Snackbar.LENGTH_SHORT);
              sb.show();
              */
      Toast.makeText(this, "Camera permission was granted.", Toast.LENGTH_SHORT).show();
      initQRCodeReaderView();
    } else {
      //Snackbar.make(mainLayout, "Camera permission request was denied.", Snackbar.LENGTH_SHORT).show();
      /*Snackbar sb = Snackbar.make(mainLayout, "Camera permission request was denied.", Snackbar.LENGTH_SHORT);
      sb.show();*/
      Toast.makeText(this, "Camera permission was denied.", Toast.LENGTH_SHORT).show();
    }
  }

  // Called when a QR is decoded
  // "text" : the text encoded in QR
  // "points" : points where QR control points are placed
  @Override public void onQRCodeRead(String text, PointF[] points) {
    resultTextView.setText(text);
    pointsOverlayView.setPoints(points);
    checkDeviceStatus(text);
  }

  private void requestCameraPermission() {
    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {

      Log.e("Status","Provided permission.");

      Intent intentX = new Intent(this, NewDecoderActivity.class);
      startActivity(intentX);

      Snackbar sb = Snackbar.make(mainLayout, "Camera access is required to display the camera preview.",
              Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
        @Override public void onClick(View view) {
          ActivityCompat.requestPermissions(NewDecoderActivity.this, new String[] {
                  Manifest.permission.CAMERA
          }, MY_PERMISSION_REQUEST_CAMERA);
        }
      });

      TextView tv = sb.getView().findViewById(com.google.android.material.R.id.snackbar_text);
      tv.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));

      sb.show();

    } else {
      Snackbar sb = Snackbar.make(mainLayout, "Permission is not available. Requesting camera permission.",
              Snackbar.LENGTH_SHORT);

      TextView tv = sb.getView().findViewById(com.google.android.material.R.id.snackbar_text);
      tv.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));

      sb.show();
      ActivityCompat.requestPermissions(this, new String[] {
              Manifest.permission.CAMERA
      }, MY_PERMISSION_REQUEST_CAMERA);
    }
  }

  private void initQRCodeReaderView() {
    View content = getLayoutInflater().inflate(R.layout.content_decoder, mainLayout, true);

    qrCodeReaderView = content.findViewById(R.id.qrdecoderview);
    resultTextView = content.findViewById(R.id.result_text_view);
    pointsOverlayView = content.findViewById(R.id.points_overlay_view);

    qrCodeReaderView.setAutofocusInterval(2000L);
    qrCodeReaderView.setOnQRCodeReadListener(this);
    qrCodeReaderView.setBackCamera();

    qrCodeReaderView.setQRDecodingEnabled(true);

    qrCodeReaderView.setTorchEnabled(false);
    qrCodeReaderView.startCamera();

    //My UI Widgets
    FloatingActionButton btnLight = content.findViewById(R.id.flashlight);

    btnLight.setOnClickListener(v -> {
      if(!lamp){
        qrCodeReaderView.setTorchEnabled(true); lamp = true;
        ViewCompat.setBackgroundTintList(btnLight,
                ContextCompat.getColorStateList(NewDecoderActivity.this,
                        R.color.colorDarkGrey));
        btnLight.setImageResource(R.drawable.ic_torch_light);
      }else{
        qrCodeReaderView.setTorchEnabled(false); lamp = false;
        ViewCompat.setBackgroundTintList(btnLight,
                ContextCompat.getColorStateList(NewDecoderActivity.this,
                        R.color.colorPrimary));
        btnLight.setImageResource(R.drawable.ic_torch_white);
      }
    });

    Button tvQuestion = content.findViewById(R.id.tvQuestion);
    tvQuestion.setOnClickListener(v -> {
      AlertDialog.Builder alertUserScan = new AlertDialog.Builder(NewDecoderActivity.this, R.style.CustomAlertDialog);
      LayoutInflater factory1 = LayoutInflater.from(NewDecoderActivity.this);
      final View view3 = factory1.inflate(R.layout.control_scan_dialog, null);
      view3.setBackgroundResource(android.R.color.transparent);
      alertUserScan.setView(view3);

      TextView title = new TextView(this);
      title.setText(R.string.authenticate);
      title.setPadding(10, 10, 10, 10);
      title.setGravity(Gravity.CENTER);
      title.setTextSize(20);
      alertUserScan.setCustomTitle(title);

      alertUserScan.setMessage("Kindly scan the QR code on the charging station.");
      alertUserScan.setNegativeButton("Ok.", (dialog, which) -> {
        qrCodeReaderView.setQRDecodingEnabled(true);
        dialog.dismiss();
      }).show();
    });

    Button tvInstruction = content.findViewById(R.id.tvInstruction);
    tvInstruction.setOnClickListener(v -> {
      AlertDialog.Builder alertUserScan = new AlertDialog.Builder(NewDecoderActivity.this, R.style.CustomAlertDialog);
      LayoutInflater factory1 = LayoutInflater.from(NewDecoderActivity.this);
      final View view3 = factory1.inflate(R.layout.control_scan_dialog, null);
      view3.setBackgroundResource(android.R.color.transparent);
      alertUserScan.setView(view3);

      TextView title = new TextView(this);
      title.setText(R.string.authenticate);
      title.setPadding(10, 10, 10, 10);
      title.setGravity(Gravity.CENTER);
      title.setTextSize(20);
      alertUserScan.setCustomTitle(title);

      alertUserScan.setMessage("Kindly scan the QR code on the charging station.");
      alertUserScan.setNegativeButton("Ok.", (dialog, which) -> {
        qrCodeReaderView.setQRDecodingEnabled(true);
        dialog.dismiss();
      }).show();
    });
  }

  private void checkDeviceStatus(String devid){
    if((devid.toLowerCase().contains("bng"))&&(devid.toLowerCase().contains("hyb")))
    {
      /*
      Intent homeint = new Intent(NewDecoderActivity.this, UserControlActivityPre.class);
      homeint.putExtra("devid",devid);
      finish();
      startActivity(homeint);
      */
      //TODO
      Intent homeint = new Intent(NewDecoderActivity.this, DeviceStatusActivity.class);
      homeint.putExtra("devid",devid);
      finish();
      startActivity(homeint);

    }else{
      Toast.makeText(this, "Kindly scan the correct QR code.", Toast.LENGTH_LONG).show();

      //Intent homeint = new Intent(NewDecoderActivity.this, CachedMapActivity.class);
      Intent homeint = new Intent(NewDecoderActivity.this, MultiStationsMapActivity.class);
      finish();
      startActivity(homeint);
    }
  }

  private void checkTransactionID(String tid){
    Log.e("Transaction ID",tid);
    TransactionRequest transactionRequest = new TransactionRequest(tid);

    TransactionAPI transactionService = retrofit.create(TransactionAPI.class);
    Call<TransactionResponse> call = transactionService.getTransactionResponse(transactionRequest);

    Log.e("Status","Fetching Transaction Details");

    call.enqueue(new Callback<TransactionResponse>() {
      @Override
      public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {

        TransactionResponse transactionResponse = response.body();

        Log.e("Status","Got response from Transaction API.");
        if(response.code()==400){
          Log.e("Decoder Activity: Status","Session is on going!!");
          Intent homeint = new Intent(NewDecoderActivity.this, UserControlActivityPre.class);
          String devid1 = pref.getString("devid","0");
          homeint.putExtra("devid",devid1);
          startActivity(homeint);
          finish();
        }
        else if(response.isSuccessful())
        {
          Log.e("Status","Session Stopped.");
          Log.e("Transaction Status", transactionResponse.getTransactionId());
          Log.e("Trasnsaction Response", transactionResponse.toString());
        }
      }

      @Override
      public void onFailure(Call<TransactionResponse> call, Throwable throwable) {
        Log.e("Status","Retrofit session status HTTP Calling Failed");
      }
    });
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }
}