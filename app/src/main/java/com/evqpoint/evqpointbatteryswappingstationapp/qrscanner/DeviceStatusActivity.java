package com.evqpoint.evqpointbatteryswappingstationapp.qrscanner;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.evqpoint.evqpointbatteryswappingstationapp.R;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.control.UserControlActivityPre;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.mapactivity.MultiStationsMapActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.activities.user.UserActivity;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.DeviceIDRequest;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.DeviceStatusResponse;
import com.evqpoint.evqpointbatteryswappingstationapp.network.stationcontrol.StationControlAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DeviceStatusActivity extends AppCompatActivity {

    AlertDialog.Builder alertUserOffline;
    AlertDialog alertUserOfflineDial;
    String devid; String initDeviceStatus; String initParentStatus;
    //RETROFIT
    public static final String BASE_URL = "https://client.evqpoint.com/api/user/";//"https://client.evqpoint.com/user/";;//"https://api.evqpoint.com/user/";
    private static Retrofit retrofit = null;

    ProgressDialog progressDialogDeviceCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_status);

        //Retrofit Initialization
        //RETROFIT
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        devid = getIntent().getStringExtra("devid");
        checkDeviceStatus(devid);
    }

    private void alertDeviceStatus() {
        if(initDeviceStatus.contains("Fault")){
            /*
             * ["IDLE", "ES", "OC", "GCFI", "EP", "P", "OV", "UV", "P"]
             *  ES - Emergency Switch
             *  OC - Over Current
             *  GCFI - Groud current leakage fault
             *  EP - Earth Presence
             *  P - No power
             *  OV - Over voltage
             *  UV - Under Voltage
             **/
            if(initParentStatus.equalsIgnoreCase("ES")){
                if(progressDialogDeviceCheck.isShowing()){
                    progressDialogDeviceCheck.dismiss();
                }

                alertUserOffline = new AlertDialog.Builder(DeviceStatusActivity.this, R.style.CustomAlertDialog);
                LayoutInflater factory2 = LayoutInflater.from(DeviceStatusActivity.this);
                final View view2 = factory2.inflate(R.layout.control_alert_dialog_offline, null);
                view2.setBackgroundResource(android.R.color.transparent);
                alertUserOffline.setView(view2);
                alertUserOffline.setTitle("Fault: Emergency Stop");
                alertUserOffline.setMessage("Kindly use another Charging Station.");

                alertUserOffline.setPositiveButton("Ok.", (dialog, which) -> {
                    alertUserOfflineDial.dismiss();
                    dialog.dismiss();
                    Intent intent = new Intent(this, MultiStationsMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }).setCancelable(false);

                alertUserOfflineDial = alertUserOffline.create();
                alertUserOfflineDial.show();

            }else if(initParentStatus.equalsIgnoreCase("OC")){
                if(progressDialogDeviceCheck.isShowing()){
                    progressDialogDeviceCheck.dismiss();
                }

                alertUserOffline = new AlertDialog.Builder(DeviceStatusActivity.this, R.style.CustomAlertDialog);
                LayoutInflater factory2 = LayoutInflater.from(DeviceStatusActivity.this);
                final View view2 = factory2.inflate(R.layout.control_alert_dialog_offline, null);
                view2.setBackgroundResource(android.R.color.transparent);
                alertUserOffline.setView(view2);
                alertUserOffline.setTitle("Fault: Over Current");
                alertUserOffline.setMessage("Kindly use another Charging Station.");

                alertUserOffline.setPositiveButton("Ok.", (dialog, which) -> {
                    alertUserOfflineDial.dismiss();
                    dialog.dismiss();
                    Intent intent = new Intent(this, MultiStationsMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }).setCancelable(false);

                alertUserOfflineDial = alertUserOffline.create();
                alertUserOfflineDial.show();

            }else if(initParentStatus.equalsIgnoreCase("GCFI")){
                if(progressDialogDeviceCheck.isShowing()){
                    progressDialogDeviceCheck.dismiss();
                }

                alertUserOffline = new AlertDialog.Builder(DeviceStatusActivity.this, R.style.CustomAlertDialog);
                LayoutInflater factory2 = LayoutInflater.from(DeviceStatusActivity.this);
                final View view2 = factory2.inflate(R.layout.control_alert_dialog_offline, null);
                view2.setBackgroundResource(android.R.color.transparent);
                alertUserOffline.setView(view2);
                alertUserOffline.setTitle("Fault: Ground Current Leakage Fault");
                alertUserOffline.setMessage("Kindly use another Charging Station.");

                alertUserOffline.setPositiveButton("Ok.", (dialog, which) -> {
                    alertUserOfflineDial.dismiss();
                    dialog.dismiss();
                    Intent intent = new Intent(this, MultiStationsMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }).setCancelable(false);

                alertUserOfflineDial = alertUserOffline.create();
                alertUserOfflineDial.show();

            }else if(initParentStatus.equalsIgnoreCase("EP")){
                if(progressDialogDeviceCheck.isShowing()){
                    progressDialogDeviceCheck.dismiss();
                }

                alertUserOffline = new AlertDialog.Builder(DeviceStatusActivity.this, R.style.CustomAlertDialog);
                LayoutInflater factory2 = LayoutInflater.from(DeviceStatusActivity.this);
                final View view2 = factory2.inflate(R.layout.control_alert_dialog_offline, null);
                view2.setBackgroundResource(android.R.color.transparent);
                alertUserOffline.setView(view2);
                alertUserOffline.setTitle("Fault: Earth Presence");
                alertUserOffline.setMessage("Kindly use another Charging Station.");

                alertUserOffline.setPositiveButton("Ok.", (dialog, which) -> {
                    alertUserOfflineDial.dismiss();
                    dialog.dismiss();
                    Intent intent = new Intent(this, MultiStationsMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }).setCancelable(false);

                alertUserOfflineDial = alertUserOffline.create();
                alertUserOfflineDial.show();

            }else if(initParentStatus.equalsIgnoreCase("P")){
                if(progressDialogDeviceCheck.isShowing()){
                    progressDialogDeviceCheck.dismiss();
                }

                alertUserOffline = new AlertDialog.Builder(DeviceStatusActivity.this, R.style.CustomAlertDialog);
                LayoutInflater factory2 = LayoutInflater.from(DeviceStatusActivity.this);
                final View view2 = factory2.inflate(R.layout.control_alert_dialog_offline, null);
                view2.setBackgroundResource(android.R.color.transparent);
                alertUserOffline.setView(view2);
                alertUserOffline.setTitle("Fault: No Power Available");
                alertUserOffline.setMessage("Kindly use another Charging Station.");

                alertUserOffline.setPositiveButton("Ok.", (dialog, which) -> {
                    alertUserOfflineDial.dismiss();
                    dialog.dismiss();
                    Intent intent = new Intent(this, MultiStationsMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }).setCancelable(false);

                alertUserOfflineDial = alertUserOffline.create();
                alertUserOfflineDial.show();

            }else if(initParentStatus.equalsIgnoreCase("OV")){
                if(progressDialogDeviceCheck.isShowing()){
                    progressDialogDeviceCheck.dismiss();
                }

                alertUserOffline = new AlertDialog.Builder(DeviceStatusActivity.this, R.style.CustomAlertDialog);
                LayoutInflater factory2 = LayoutInflater.from(DeviceStatusActivity.this);
                final View view2 = factory2.inflate(R.layout.control_alert_dialog_offline, null);
                view2.setBackgroundResource(android.R.color.transparent);
                alertUserOffline.setView(view2);
                alertUserOffline.setTitle("Fault: Over Voltage");
                alertUserOffline.setMessage("Kindly use another Charging Station.");

                alertUserOffline.setPositiveButton("Ok.", (dialog, which) -> {
                    alertUserOfflineDial.dismiss();
                    dialog.dismiss();
                    Intent intent = new Intent(this, MultiStationsMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }).setCancelable(false);

                alertUserOfflineDial = alertUserOffline.create();
                alertUserOfflineDial.show();

            }else if(initParentStatus.equalsIgnoreCase("UV")){
                if(progressDialogDeviceCheck.isShowing()){
                    progressDialogDeviceCheck.dismiss();
                }

                alertUserOffline = new AlertDialog.Builder(DeviceStatusActivity.this, R.style.CustomAlertDialog);
                LayoutInflater factory2 = LayoutInflater.from(DeviceStatusActivity.this);
                final View view2 = factory2.inflate(R.layout.control_alert_dialog_offline, null);
                view2.setBackgroundResource(android.R.color.transparent);
                alertUserOffline.setView(view2);
                alertUserOffline.setTitle("Fault: Under Voltage");
                alertUserOffline.setMessage("Kindly use another Charging Station.");

                alertUserOffline.setPositiveButton("Ok.", (dialog, which) -> {
                    alertUserOfflineDial.dismiss();
                    dialog.dismiss();
                    Intent intent = new Intent(this, MultiStationsMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }).setCancelable(false);

                alertUserOfflineDial = alertUserOffline.create();
                alertUserOfflineDial.show();
            }
        }
        else if(initDeviceStatus.equalsIgnoreCase("busy")){
            if(progressDialogDeviceCheck.isShowing()){
                progressDialogDeviceCheck.dismiss();
            }

            alertUserOffline = new AlertDialog.Builder(DeviceStatusActivity.this, R.style.CustomAlertDialog);
            LayoutInflater factory2 = LayoutInflater.from(DeviceStatusActivity.this);
            final View view2 = factory2.inflate(R.layout.control_alert_dialog_offline, null);
            view2.setBackgroundResource(android.R.color.transparent);
            alertUserOffline.setView(view2);
            alertUserOffline.setTitle("Charger Busy");
            alertUserOffline.setMessage("Kindly use another Charging Station.");

            alertUserOffline.setPositiveButton("Ok.", (dialog, which) -> {
                alertUserOfflineDial.dismiss();
                dialog.dismiss();
                Intent intent = new Intent(this, MultiStationsMapActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }).setCancelable(false);

            alertUserOfflineDial = alertUserOffline.create();
            alertUserOfflineDial.show();
        }
        else if(initParentStatus!=null)
        {
            Log.e("StatusActivity: DEVID", devid);

            if(initDeviceStatus.equalsIgnoreCase("IDLE")){
                //Intent intent = new Intent(this, ChargeOptionActivity.class);
                Intent intent = new Intent(this, UserControlActivityPre.class);
                intent.putExtra("devid",devid);

                if(progressDialogDeviceCheck.isShowing()){
                    progressDialogDeviceCheck.dismiss();
                }

                startActivity(intent);
                finish();
            }
            else if((initDeviceStatus.equalsIgnoreCase("EM"))||
                    (initDeviceStatus.equalsIgnoreCase("OVP"))||
                    (initDeviceStatus.equalsIgnoreCase("BMSF")))
            {
                if(progressDialogDeviceCheck.isShowing()){
                    progressDialogDeviceCheck.dismiss();
                }

                alertUserOffline = new AlertDialog.Builder(DeviceStatusActivity.this, R.style.CustomAlertDialog);
                LayoutInflater factory2 = LayoutInflater.from(DeviceStatusActivity.this);
                final View view2 = factory2.inflate(R.layout.control_alert_dialog_offline, null);
                view2.setBackgroundResource(android.R.color.transparent);
                alertUserOffline.setView(view2);
                alertUserOffline.setTitle("Device Offline");
                alertUserOffline.setMessage("Kindly use another Charging Station.");
                alertUserOffline.setPositiveButton("Ok.", (dialog, which) -> {

                    if(alertUserOfflineDial != null)
                        alertUserOfflineDial.dismiss();

                    dialog.dismiss();

                    Intent intent = new Intent(this, UserActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }).setCancelable(false);

                alertUserOfflineDial = alertUserOffline.create();
                alertUserOfflineDial.show();
            }
            else if(initDeviceStatus.equalsIgnoreCase("CHARGE"))
            {
                if(progressDialogDeviceCheck.isShowing()){
                    progressDialogDeviceCheck.dismiss();
                }

                alertUserOffline = new AlertDialog.Builder(DeviceStatusActivity.this, R.style.CustomAlertDialog);
                LayoutInflater factory2 = LayoutInflater.from(DeviceStatusActivity.this);
                final View view2 = factory2.inflate(R.layout.control_alert_dialog_offline, null);
                view2.setBackgroundResource(android.R.color.transparent);
                alertUserOffline.setView(view2);
                alertUserOffline.setTitle("Station in Use");
                alertUserOffline.setMessage("Kindly use another Charging Station.");

                alertUserOffline.setPositiveButton("Ok.", (dialog, which) -> {
                    //("DeviceStatusActivity: Dialog Status","Positive Click + Dismissing Dialog");
                    alertUserOfflineDial.dismiss();
                    dialog.dismiss();
                    //Log.e("DeviceStatusActivity: Dialog Status","Invoking Return Intent");
                    Intent intent = new Intent(this, MultiStationsMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }).setCancelable(false);

                //Log.e("DeviceStatusActivity: Dialog Status","Creating Dialog");
                alertUserOfflineDial = alertUserOffline.create();
                //Log.e("DeviceStatusActivity: Dialog Status","Invoking Dialog");
                alertUserOfflineDial.show();
            }
            else if(initDeviceStatus.equalsIgnoreCase("Offline"))
            {
                if(progressDialogDeviceCheck.isShowing()){
                    progressDialogDeviceCheck.dismiss();
                }

                Log.e("Status Activity:: Offline Alert","Showing Offline Alert");
                alertUserOffline = new AlertDialog.Builder(DeviceStatusActivity.this, R.style.CustomAlertDialog);
                LayoutInflater factory2 = LayoutInflater.from(DeviceStatusActivity.this);
                final View view2 = factory2.inflate(R.layout.control_alert_dialog_offline, null);
                view2.setBackgroundResource(android.R.color.transparent);
                alertUserOffline.setView(view2);
                alertUserOffline.setTitle("Device Offline");
                alertUserOffline.setMessage("Kindly use another Charging Station.");

                alertUserOffline.setPositiveButton("Ok.", (dialog, which) -> {
                    //Log.e("DeviceStatusActivity: Dialog Status","Positive Click + Dismissing Dialog");
                    alertUserOfflineDial.dismiss();
                    dialog.dismiss();
                    //Log.e("DeviceStatusActivity: Dialog Status","Invoking Return Intent");
                    Intent intent = new Intent(this, MultiStationsMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }).setCancelable(false);

                //Log.e("DeviceStatusActivity: Dialog Status","Creating Dialog");
                alertUserOfflineDial = alertUserOffline.create();
                //Log.e("DeviceStatusActivity: Dialog Status","Invoking Dialog");
                alertUserOfflineDial.show();
            }
            else if(initDeviceStatus.equalsIgnoreCase("FAULT")){

                if(progressDialogDeviceCheck.isShowing()){
                    progressDialogDeviceCheck.dismiss();
                }

                alertUserOffline = new AlertDialog.Builder(DeviceStatusActivity.this, R.style.CustomAlertDialog);
                LayoutInflater factory2 = LayoutInflater.from(DeviceStatusActivity.this);
                final View view2 = factory2.inflate(R.layout.control_alert_dialog_offline, null);
                view2.setBackgroundResource(android.R.color.transparent);
                alertUserOffline.setView(view2);
                alertUserOffline.setTitle("Device Fault");
                alertUserOffline.setMessage("Kindly use another Charging Station.");

                alertUserOffline.setPositiveButton("Ok.", (dialog, which) -> {
                    //Log.e("DeviceStatusActivity: Dialog Status","Positive Click + Dismissing Dialog");
                    alertUserOfflineDial.dismiss();
                    dialog.dismiss();
                    //Log.e("DeviceStatusActivity: Dialog Status","Invoking Return Intent");
                    Intent intent = new Intent(this, MultiStationsMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }).setCancelable(false);

                //Log.e("DeviceStatusActivity: Dialog Status","Creating Dialog");
                alertUserOfflineDial = alertUserOffline.create();
                //Log.e("DeviceStatusActivity: Dialog Status","Invoking Dialog");
                alertUserOfflineDial.show();

                /*Toast.makeText(DeviceStatusActivity.this,
                        "System in Fault!", Toast.LENGTH_SHORT).show();
                Intent backIntent = new Intent(DeviceStatusActivity.this, MultiStationsMapActivity.class);
                finish();
                startActivity(backIntent);*/
            }
        }
    }


    /*
    @Override
    public void onPause(){
        super.onPause();
        if (alertUserOfflineDial.isShowing()){
            alertUserOfflineDial.dismiss();
        }
    }
    */

    @Override
    public void onStop() {
        super.onStop();
    /*
        if (alertUserOfflineDial.isShowing()){
            alertUserOfflineDial.dismiss();
        }
    */

        if (progressDialogDeviceCheck!=null){
            if(progressDialogDeviceCheck.isShowing())
            {
                progressDialogDeviceCheck.dismiss();
            }
        }

        if(alertUserOfflineDial != null)
            if(alertUserOfflineDial.isShowing())
                alertUserOfflineDial.dismiss();
    }

    private void checkDeviceStatus(String devidin) {
        initDeviceStatus = "Unknown";

        //String childid = devidin.substring(devidin.lastIndexOf("-")+1,2);
        String childid = devidin.substring(devidin.length()-2);
        Log.e("DeviceStatusActivity: ChildID",childid);

        if(checkNetwork())
        {
            progressDialogDeviceCheck = ProgressDialog.show(DeviceStatusActivity.this, "Checking Station Status.",
                    "Please wait..", false, false);

            /*
            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .build();
            */

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    //.client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            DeviceIDRequest devIDRequest = new DeviceIDRequest(devidin);
            StationControlAPI chargeStationService = retrofit.create(StationControlAPI.class);
            Call<DeviceStatusResponse> call = chargeStationService.getDeviceStatus(devIDRequest);

            Log.e("DeviceStatusActivity: Status","Getting device status.");
            Log.e("DeviceStatusActivity: Request", devIDRequest.toString());

            call.enqueue(new Callback<DeviceStatusResponse>() {
                @Override
                public void onResponse(Call<DeviceStatusResponse> call, Response<DeviceStatusResponse> response) {

                    if(response.isSuccessful()){
                        DeviceStatusResponse deviceResponse = response.body();
                        Log.e("DeviceStatusActivity: DeviceStatusActivity: Device Status", deviceResponse.toString());

                        if(deviceResponse.getDeviceStatus()!=null)
                        {
                            Log.e("DeviceStatusActivity: Resp initDeviceStatus","Offline");
                            initDeviceStatus = "offline";
                            alertDeviceStatus();
                            //TODO
                            if(progressDialogDeviceCheck.isShowing()){
                                progressDialogDeviceCheck.dismiss();
                            }

                            alertUserOffline = new AlertDialog.Builder(DeviceStatusActivity.this, R.style.CustomAlertDialog);
                            LayoutInflater factory2 = LayoutInflater.from(DeviceStatusActivity.this);
                            final View view2 = factory2.inflate(R.layout.control_alert_dialog_offline, null);
                            view2.setBackgroundResource(android.R.color.transparent);
                            alertUserOffline.setView(view2);
                            alertUserOffline.setTitle("Station Offline");
                            alertUserOffline.setMessage("Kindly use another Charging Station.");

                            alertUserOffline.setPositiveButton("Ok.", (dialog, which) -> {
                                //Log.e("DeviceStatusActivity: Dialog Status","Positive Click + Dismissing Dialog");
                                alertUserOfflineDial.dismiss();
                                dialog.dismiss();
                                //Log.e("DeviceStatusActivity: Dialog Status","Invoking Return Intent");
                                Intent intent = new Intent(DeviceStatusActivity.this, MultiStationsMapActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }).setCancelable(false);

                            alertUserOfflineDial = alertUserOffline.create();
                            alertUserOfflineDial.show();

                        }
                        else if(deviceResponse.getParentDeviceStatus()!=null){
                            initParentStatus = deviceResponse.getParentDeviceStatus();
                            if(!initParentStatus.equalsIgnoreCase("IDLE")){
                                initDeviceStatus = "Fault";
                                alertDeviceStatus();
                            }
                            else{
                                if(response.code()==200){

                                    boolean busy = false;
                                    if(deviceResponse.getChild1DeviceStatus().equalsIgnoreCase("CHARGE")){
                                        busy = true;
                                    }
                                    else if(deviceResponse.getChild2DeviceStatus().equalsIgnoreCase("CHARGE")){
                                        busy = true;
                                    }else if(deviceResponse.getChild3DeviceStatus().equalsIgnoreCase("CHARGE")){
                                        busy = true;
                                    }

                                    //Allow ONLY ONE CHILD to charge
                                    if(childid.equalsIgnoreCase("01")){
                                        initDeviceStatus = deviceResponse.getChild1DeviceStatus();
                                    }else if(childid.equalsIgnoreCase("02")){
                                        initDeviceStatus = deviceResponse.getChild2DeviceStatus();
                                    }else if(childid.equalsIgnoreCase("03")){
                                        initDeviceStatus = deviceResponse.getChild3DeviceStatus();
                                    }

                                    Log.e("DeviceStatusActivity: STATUS DEV",initDeviceStatus);

                                    /*SINGLE CHILD LOCK
                                    if((initDeviceStatus.equalsIgnoreCase("IDLE"))&&
                                            (busy)){
                                        Log.e("DeviceStatusActivity: initDeviceStatus","Busy Charging");
                                        initDeviceStatus = "busy";
                                        alertDeviceStatus();
                                    }
                                    else if((initDeviceStatus.equalsIgnoreCase("IDLE"))&&(!busy)){*/
                                        Intent intent = new Intent(DeviceStatusActivity.this,
                                                UserControlActivityPre.class);
                                        intent.putExtra("devid",devid);

                                        if(progressDialogDeviceCheck.isShowing()){
                                            progressDialogDeviceCheck.dismiss();
                                        }

                                        startActivity(intent);
                                        finish();
                                    //}
                                }
                                else {
                                    Log.e("DeviceStatusActivity: ERRORED",initDeviceStatus);
                                    initDeviceStatus = "offline";
                                    alertDeviceStatus();
                                }

                                Log.e("Device Status", initDeviceStatus);

                                if(initDeviceStatus.equalsIgnoreCase("CHARGE")){
                                    Log.e("DeviceStatusActivity: initDeviceStatus","Busy Charging");
                                    initDeviceStatus = "busy";
                                    alertDeviceStatus();
                                }
                            }
                        }
                    }else{
                        Log.e("DeviceStatusActivity: initDeviceStatus1","Offline");
                        initDeviceStatus = "offline";
                        alertDeviceStatus();
                    }
                }

                @Override
                public void onFailure(Call<DeviceStatusResponse> call, Throwable throwable) {
                    Log.e("DeviceStatusActivity: initDeviceStatus2","Offline");
                    initDeviceStatus = "offline";
                    alertDeviceStatus();
                }
            });
        }
    }

    private boolean checkNetwork() {
        final ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        //TODO TEST & BUILD
        /*Toast.makeText(this, "No internet. Kindly connect internet and try again.", Toast.LENGTH_LONG).show();
            Intent inten = getParentActivityIntent();
            startActivity(inten);
            finish();*/
        return activeNetwork != null && activeNetwork.isConnected();
    }
}
