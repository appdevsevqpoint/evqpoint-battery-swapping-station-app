package com.evqpoint.evqpointbatteryswappingstationapp.db.transactions;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

public class FetchLastWeekTransactionsSumAsync extends AsyncTask<Void, Void, Double> {

    private Context context;

    public FetchLastWeekTransactionsSumAsync(Context context) {
        this.context = context;
    }

    @Override
    protected Double doInBackground(Void... params) {
        TransactionDB db = TransactionDB.getInstance(context);

        long currenttimestamp = System.currentTimeMillis()/1000;
        Log.e("Current Time Stamp",Long.toString(currenttimestamp));

        long limittimestamp = currenttimestamp - 604800;
        Log.e("Limit Time",Long.toString(limittimestamp));

        Double sums = db.transactionDao().getLastWeekSum(limittimestamp);

        /*
        List<TransactionEntity> transactionListdb = db.transactionDao().getLastWeek(limittimestamp);

        Double sums = 0.0;
        for(int p=0; p<transactionListdb.size(); p++){

            Log.e("Transactions Weekly", transactionListdb.get(p).toString());

            sums = sums + transactionListdb.get(p).getTotalAmount();
        }

        Log.e("FetchWeekSum DB",sums.toString());
        */
        return sums;
    }
}