package com.evqpoint.evqpointbatteryswappingstationapp.db.stations;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.ChargingStation;

import java.util.List;

public class AddAllStationsAsync extends AsyncTask<Void, Void, Void> {

    private List<StationEntity> stationsListdb;
    private StationDB db;

    public AddAllStationsAsync(Context inContext, List<StationEntity> inTransactionList) {
        this.stationsListdb = inTransactionList;
        db = StationDB.getInstance(inContext);

        //logSavedStationList();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        db.stationDao().insertAllStations(stationsListdb);
        return null;
    }

    public void logSavedStationList(){
        Log.e("DB Station List", String.valueOf(this.stationsListdb.size()));

        for(int i=0; i<this.stationsListdb.size(); i++){
            Log.e("DB Station Item", this.stationsListdb.get(i).toString());
        }
    }
}