package com.evqpoint.evqpointbatteryswappingstationapp.db.transactions;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

public class FetchLast5TransactionsAsync extends AsyncTask<Void, Void, List<TransactionEntity>> {

    private Context context;

    public FetchLast5TransactionsAsync(Context context) {
        this.context = context;
    }

    @Override
    protected List<TransactionEntity> doInBackground(Void... params) {
        TransactionDB db = TransactionDB.getInstance(context);
        List<TransactionEntity> transactionListdb = db.transactionDao().getLastFive();

        return transactionListdb;
    }
}