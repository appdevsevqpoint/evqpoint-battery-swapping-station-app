package com.evqpoint.evqpointbatteryswappingstationapp.db.transactions;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

public class FetchLastWeekTransactionsAsync extends AsyncTask<Void, Void, List<TransactionEntity>> {

    private Context context;

    public FetchLastWeekTransactionsAsync(Context context) {
        this.context = context;
    }

    @Override
    protected List<TransactionEntity> doInBackground(Void... params) {
        TransactionDB db = TransactionDB.getInstance(context);

        long currenttimestamp = System.currentTimeMillis()/1000;
        long limittimestamp = currenttimestamp - 604800;
        List<TransactionEntity> transactionListdb = db.transactionDao().getLastWeek(limittimestamp);

        return transactionListdb;
    }
}