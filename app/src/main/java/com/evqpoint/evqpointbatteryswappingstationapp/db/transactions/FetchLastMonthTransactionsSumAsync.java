package com.evqpoint.evqpointbatteryswappingstationapp.db.transactions;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.text.SimpleDateFormat;

public class FetchLastMonthTransactionsSumAsync extends AsyncTask<Void, Void, Double> {

    private Context context;

    public FetchLastMonthTransactionsSumAsync(Context context) {
        this.context = context;
    }

    @Override
    protected Double doInBackground(Void... params) {
        TransactionDB db = TransactionDB.getInstance(context);

        long currenttimestamp = System.currentTimeMillis()/1000;
        Log.e("Current Time Stamp",Long.toString(currenttimestamp));

        Double sums = db.transactionDao().getThisMonthSum(epochToMonthNumber(currenttimestamp));
        //Log.e("Sums",sums.toString());
        return sums;
    }

    private int epochToMonthNumber(long epochSeconds){
        long itemLong = epochSeconds;
        java.util.Date d = new java.util.Date(itemLong*1000L);
        String itemDateStr = new SimpleDateFormat("MM").format(d);
        return Integer.valueOf(itemDateStr);
    }
}