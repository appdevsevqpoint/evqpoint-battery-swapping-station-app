package com.evqpoint.evqpointbatteryswappingstationapp.db.transactions;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "transactions_table")
public class TransactionEntity {

    @PrimaryKey
    @NonNull
    private String id;
    @ColumnInfo(name = "deviceId")
    private String deviceID;
    @ColumnInfo(name = "childId")
    private int childID;
    @ColumnInfo(name = "startTime")
    private String startTime;
    @ColumnInfo(name = "starttimestamp")
    private int startTimeStamp;
    @ColumnInfo(name = "startmonth")
    private int startMonth;
    @ColumnInfo(name = "transactionId")
    private String transactionID;
    @ColumnInfo(name = "energyConsumed")
    private Double energyConsumed;
    @ColumnInfo(name = "duration")
    private String duration;
    @ColumnInfo(name = "soc")
    private Integer soc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getStartTimeStamp() {
        return startTimeStamp;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartTimeStamp(int startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public Double getEnergyConsumed() {
        return energyConsumed;
    }

    public void setEnergyConsumed(Double energyConsumed) {
        this.energyConsumed = energyConsumed;
    }

    public String getDuration() {
        return this.duration;
    }

    public void setDuration(String inDuration) {
        this.duration = inDuration;
    }

    public int getChildID() {
        return childID;
    }

    public void setChildID(int childID) {
        this.childID = childID;
    }

    public Integer getSoc() {
        return soc;
    }

    public void setSoc(Integer soc) {
        this.soc = soc;
    }

    /**
     * @param id
     * @param deviceID
     * @param childID
     * @param startTime
     * @param id
     * @param startTimeStamp
     * @param startMonth
     * @param transactionID
     * @param energyConsumed
     * @param duration
     * @param soc
     */
    public TransactionEntity(@NonNull String id, String deviceID, int childID, String startTime,
                             int startTimeStamp, int startMonth, String transactionID,
                             Double energyConsumed, String duration, Integer soc) {
        this.id = id;
        this.deviceID = deviceID;
        this.childID = childID;
        this.startTime = startTime;
        this.startTimeStamp = startTimeStamp;
        this.startMonth = startMonth;
        this.transactionID = transactionID;
        this.energyConsumed = energyConsumed;
        this.duration = duration;
        this.soc = soc;
    }

    @Override
    public String toString() {
        return "TransactionEntity{" +
                "id='" + id + '\'' +
                ", deviceID='" + deviceID + '\'' +
                ", childID=" + childID +
                ", startTime='" + startTime + '\'' +
                ", startTimeStamp=" + startTimeStamp +
                ", startMonth=" + startMonth +
                ", transactionID='" + transactionID + '\'' +
                ", energyConsumed=" + energyConsumed +
                ", duration='" + duration + '\'' +
                ", soc=" + soc +
                '}';
    }
}
