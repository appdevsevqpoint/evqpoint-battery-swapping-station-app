package com.evqpoint.evqpointbatteryswappingstationapp.db.stations;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(version = 1, entities = {StationEntity.class})
public abstract class StationDB extends RoomDatabase {

    private static StationDB stationsDB;

    public static StationDB getInstance(Context context){

        if(stationsDB==null){
            stationsDB = Room.databaseBuilder(context.getApplicationContext(),
                    StationDB.class, "stations_table").build();
        }
        return stationsDB;
    }

    public abstract StationDao stationDao();
}
