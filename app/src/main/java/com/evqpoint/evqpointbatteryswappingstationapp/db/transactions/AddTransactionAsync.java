package com.evqpoint.evqpointbatteryswappingstationapp.db.transactions;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class AddTransactionAsync extends AsyncTask<Void, Void, Void> {

    private TransactionEntity transaction;
    private TransactionDB db;

    public AddTransactionAsync(Context inContext, TransactionEntity inTransaction) {
        this.transaction = inTransaction;
        db = TransactionDB.getInstance(inContext);

        logSavedTransactionList();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        db.transactionDao().insertTransaction(transaction);
        return null;
    }

    public void logSavedTransactionList(){
        Log.e("DB Transaction to Save", this.transaction.toString());
    }
}