package com.evqpoint.evqpointbatteryswappingstationapp.db.stations;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface StationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllStations(List<StationEntity> stations);

    @Query("SELECT * FROM stations_table")
    List<StationEntity> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertStation(StationEntity transaction);

}
