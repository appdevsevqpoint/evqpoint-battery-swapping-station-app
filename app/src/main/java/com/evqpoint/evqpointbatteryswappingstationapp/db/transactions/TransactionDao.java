package com.evqpoint.evqpointbatteryswappingstationapp.db.transactions;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface TransactionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllTransactions(List<TransactionEntity> transactions);

    @Query("SELECT * FROM transactions_table ORDER BY starttimestamp DESC")
    List<TransactionEntity> getAll();

    @Query("SELECT * FROM transactions_table ORDER BY starttimestamp DESC LIMIT 5")
    List<TransactionEntity> getLastFive();

    @Query("SELECT * FROM transactions_table WHERE starttimestamp > :limittimestamp ORDER BY starttimestamp DESC")
    List<TransactionEntity> getLastWeek(long limittimestamp);

    @Query("SELECT SUM(energyConsumed) FROM transactions_table WHERE starttimestamp > :limittimestamp")
    Double getLastWeekSum(long limittimestamp);

    /*
    @Query("SELECT * FROM transactions_table ORDER BY starttimestamp DESC WHERE starttimestamp > :limittimestamp")
    List<TransactionEntity> getThisMonth(long limittimestamp);

    @Query("SELECT * FROM transactions_table ORDER BY starttimestamp DESC WHERE starttimestamp > :limittimestamp")
    List<TransactionEntity> getThisQuarter(long limittimestamp);
    */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTransaction(TransactionEntity transaction);

    @Update
    void updateTransaction(TransactionEntity transaction);

    @Query("SELECT SUM(energyConsumed) FROM transactions_table WHERE startmonth == :monthNumber")
    Double getThisMonthSum(int monthNumber);
}
