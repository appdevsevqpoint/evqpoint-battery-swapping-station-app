package com.evqpoint.evqpointbatteryswappingstationapp.db.transactions;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

public class InitTransactionsAsync extends AsyncTask<Void, Void, Void> {

    private List<TransactionEntity> transactionsListdb;
    private Context context;

    public InitTransactionsAsync(Context inContext, List<TransactionEntity> inTransactionList) {
        this.transactionsListdb = inTransactionList;
        this.context = inContext;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        TransactionDB db = TransactionDB.getInstance(context);

        if(db.transactionDao().getAll().isEmpty()) {
            db.transactionDao().insertAllTransactions(transactionsListdb);
        }
        return null;
    }
}