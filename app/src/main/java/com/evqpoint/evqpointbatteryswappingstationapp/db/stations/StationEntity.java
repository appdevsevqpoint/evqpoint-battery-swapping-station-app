package com.evqpoint.evqpointbatteryswappingstationapp.db.stations;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.evqpoint.evqpointbatteryswappingstationapp.network.addstation.ChargingStation;

import java.util.ArrayList;
import java.util.List;

@Entity(tableName = "stations_table")
public class StationEntity {

    @PrimaryKey
    @NonNull
    private String id;
    private String name;
    private String address;
    private Double lat;
    private Double lon;
    private String image;
    private Boolean isDeleted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public StationEntity(@NonNull String id, String name, String address, Double lat, Double lon, String image, Boolean isDeleted) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.lat = lat;
        this.lon = lon;
        this.image = image;
        this.isDeleted = isDeleted;
    }

    public StationEntity(ChargingStation chargingStation) {
        this.id = chargingStation.getId();
        this.name = chargingStation.getName();
        this.address = chargingStation.getAddress();
        this.lat = chargingStation.getLocation().get(0);
        this.lon = chargingStation.getLocation().get(1);
        this.image = chargingStation.getImage();
        this.isDeleted = chargingStation.getIsDeleted();
    }

    @Override
    public String toString() {
        return "{" +
                "id=\"" + id + '\"' +
                ", name=\"" + name + '\"' +
                ", address=\"" + address + '\"' +
                ", location=\"" + lat + '\"' +
                ", location=\"" + lon + '\"' +
                ", image=\"" + image + '\"' +
                ", isDeleted=\"" + isDeleted + '\"' +
                '}';
    }

    public ChargingStation convertToChargingStation() {

        List<Double> locationX = new ArrayList<Double>();

        if(this.lat!=null){
            locationX.add(this.lat);
            locationX.add(this.lon);
        }else{
            locationX.add(0.0);
            locationX.add(0.0);
        }

        ChargingStation newChargingStation = new ChargingStation(
                this.id,
                this.name,
                this.address,
                locationX,
                this.image,
                this.isDeleted
        );

        return newChargingStation;
    }
}
