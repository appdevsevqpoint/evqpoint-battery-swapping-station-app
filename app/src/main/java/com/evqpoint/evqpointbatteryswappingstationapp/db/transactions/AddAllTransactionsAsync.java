package com.evqpoint.evqpointbatteryswappingstationapp.db.transactions;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

public class AddAllTransactionsAsync extends AsyncTask<Void, Void, Void> {

    private List<TransactionEntity> transactionsListdb;
    private TransactionDB db;

    public AddAllTransactionsAsync(Context inContext, List<TransactionEntity> inTransactionList) {
        this.transactionsListdb = inTransactionList;
        db = TransactionDB.getInstance(inContext);

        logSavedTransactionList();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        db.transactionDao().insertAllTransactions(transactionsListdb);
        return null;
    }

    public void logSavedTransactionList(){
        Log.e("DB Task List", String.valueOf(this.transactionsListdb.size()));

        for(int i=0; i<this.transactionsListdb.size(); i++){
            Log.e("DB Transaction Item", this.transactionsListdb.get(i).toString());
        }
    }
}