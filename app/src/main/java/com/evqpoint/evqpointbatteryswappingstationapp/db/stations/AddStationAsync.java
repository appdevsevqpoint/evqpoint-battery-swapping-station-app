package com.evqpoint.evqpointbatteryswappingstationapp.db.stations;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class AddStationAsync extends AsyncTask<Void, Void, Void> {

    private StationEntity stationEntity;
    private StationDB db;

    public AddStationAsync(Context inContext, StationEntity inTransaction) {
        this.stationEntity = inTransaction;
        db = StationDB.getInstance(inContext);

        logSavedTransactionList();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        db.stationDao().insertStation(stationEntity);
        return null;
    }

    public void logSavedTransactionList(){
        Log.e("DB Station to Save", this.stationEntity.toString());
    }
}