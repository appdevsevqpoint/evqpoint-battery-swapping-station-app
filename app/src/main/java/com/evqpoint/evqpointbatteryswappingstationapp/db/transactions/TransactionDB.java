package com.evqpoint.evqpointbatteryswappingstationapp.db.transactions;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(version = 3, entities = {TransactionEntity.class})
public abstract class TransactionDB extends RoomDatabase {

    private static TransactionDB transactionDB;

    public static TransactionDB getInstance(Context context){

        if(transactionDB == null){
            transactionDB = Room.databaseBuilder(context.getApplicationContext(),
                    TransactionDB.class, "transactions_table").fallbackToDestructiveMigration().build();
        }
        return transactionDB;
    }

    public abstract TransactionDao transactionDao();
}
