package com.evqpoint.evqpointbatteryswappingstationapp.db.transactions;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

public class FetchTransactionsAsync extends AsyncTask<Void, Void, List<TransactionEntity>> {

    private Context context;

    public FetchTransactionsAsync(Context context) {
        this.context = context;
    }

    @Override
    protected List<TransactionEntity> doInBackground(Void... params) {
        TransactionDB db = TransactionDB.getInstance(context);
        List<TransactionEntity> transactionListdb = db.transactionDao().getAll();

        return transactionListdb;
    }
}