package com.evqpoint.evqpointbatteryswappingstationapp.db.stations;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

public class InitStationsAsync extends AsyncTask<Void, Void, Void> {

    private List<StationEntity> stationListdb;
    private Context context;

    public InitStationsAsync(Context inContext, List<StationEntity> inStationList) {
        this.stationListdb = inStationList;
        this.context = inContext;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        StationDB db = StationDB.getInstance(context);

        if(db.stationDao().getAll().isEmpty()) {
            db.stationDao().insertAllStations(stationListdb);
        }
        return null;
    }
}