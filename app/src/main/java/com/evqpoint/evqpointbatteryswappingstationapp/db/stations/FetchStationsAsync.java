package com.evqpoint.evqpointbatteryswappingstationapp.db.stations;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

public class FetchStationsAsync extends AsyncTask<Void, Void, List<StationEntity>> {

    private Context context;

    public FetchStationsAsync(Context context) {
        this.context = context;
    }

    @Override
    protected List<StationEntity> doInBackground(Void... params) {
        StationDB db = StationDB.getInstance(context);
        List<StationEntity> stationListdb = db.stationDao().getAll();

        return stationListdb;
    }
}